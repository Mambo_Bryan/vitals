package com.collabmedhealthcare.vitals.data.models.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContactTraceRequest {
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("long")
    @Expose
    private String lng;
    @SerializedName("location_name")
    @Expose
    private String locationName;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("patient_id")
    @Expose
    private Integer patientId;
    @SerializedName("secondary_email")
    @Expose
    private String secondaryEmail;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    @SerializedName("trackees")
    @Expose
    private List<TrackerRequestBody> trackees = null;

    public void setAddress(String address) {
        this.address = address;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void set_long(String lng) {
        this.lng = lng;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public void setSecondaryEmail(String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setTrackees(List<TrackerRequestBody> trackees) {
        this.trackees = trackees;
    }
}
