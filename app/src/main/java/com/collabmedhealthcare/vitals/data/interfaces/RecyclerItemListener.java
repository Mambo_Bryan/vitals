package com.collabmedhealthcare.vitals.data.interfaces;

public interface RecyclerItemListener {
    void onItemSelected(int position);
}
