package com.collabmedhealthcare.vitals.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactData implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("patient")
    @Expose
    private String patient;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("secondary_email")
    @Expose
    private String secondaryEmail;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("trackees")
    @Expose
    private Integer trackees;
    @SerializedName("traced_contacts")
    @Expose
    private Integer tracedContacts;

    protected ContactData(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        patient = in.readString();
        mobile = in.readString();
        telephone = in.readString();
        contact = in.readString();
        location = in.readString();
        email = in.readString();
        secondaryEmail = in.readString();
        address = in.readString();
        comments = in.readString();
        if (in.readByte() == 0) {
            trackees = null;
        } else {
            trackees = in.readInt();
        }
        if (in.readByte() == 0) {
            tracedContacts = null;
        } else {
            tracedContacts = in.readInt();
        }
    }

    public static final Creator<ContactData> CREATOR = new Creator<ContactData>() {
        @Override
        public ContactData createFromParcel(Parcel in) {
            return new ContactData(in);
        }

        @Override
        public ContactData[] newArray(int size) {
            return new ContactData[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public String getPatient() {
        return patient;
    }

    public String getMobile() {
        return mobile;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getContact() {
        return contact;
    }

    public String getLocation() {
        return location;
    }

    public String getEmail() {
        return email;
    }

    public String getSecondaryEmail() {
        return secondaryEmail;
    }

    public String getAddress() {
        return address;
    }

    public String getComments() {
        return comments;
    }

    public Integer getTrackers() {
        return trackees;
    }

    public Integer getTracedContacts() {
        return tracedContacts;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(patient);
        dest.writeString(mobile);
        dest.writeString(telephone);
        dest.writeString(contact);
        dest.writeString(location);
        dest.writeString(email);
        dest.writeString(secondaryEmail);
        dest.writeString(address);
        dest.writeString(comments);
        if (trackees == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(trackees);
        }
        if (tracedContacts == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(tracedContacts);
        }
    }
}
