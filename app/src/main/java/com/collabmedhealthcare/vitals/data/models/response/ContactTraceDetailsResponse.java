package com.collabmedhealthcare.vitals.data.models.response;

import com.collabmedhealthcare.vitals.data.models.ContactTracingDetailsData;
import com.collabmedhealthcare.vitals.data.models.ResponseLinks;
import com.collabmedhealthcare.vitals.data.models.ResponseMeta;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContactTraceDetailsResponse {

    @SerializedName("data")
    @Expose
    private List<ContactTracingDetailsData> data = null;
    @SerializedName("links")
    @Expose
    private transient ResponseLinks links;
    @SerializedName("meta")
    @Expose
    private ResponseMeta meta;

    public List<ContactTracingDetailsData> getData() {
        return data;
    }

    public ResponseLinks getLinks() {
        return links;
    }

    public ResponseMeta getMeta() {
        return meta;
    }
}
