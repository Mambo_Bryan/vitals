package com.collabmedhealthcare.vitals.data.models.response;

import com.collabmedhealthcare.vitals.data.models.PatientRegion;
import com.collabmedhealthcare.vitals.data.models.RegionClinics;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegionDetails {

    @SerializedName("clinics")
    @Expose
    private List<RegionClinics> clinics;

    @SerializedName("regions")
    @Expose
    private List<PatientRegion> regions;

    public List<RegionClinics> getClinics() {
        return clinics;
    }

    public List<PatientRegion> getRegions() {
        return regions;
    }
}
