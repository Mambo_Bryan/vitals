package com.collabmedhealthcare.vitals.data.models.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdatePatientRequest {

    @SerializedName("age")
    @Expose
    public String age;
    @SerializedName("age_in")
    @Expose
    public String ageIn = "Years";
    @SerializedName("patient_id")
    @Expose
    public Integer patientId;
    @SerializedName("hasInsurance")
    @Expose
    public Boolean hasInsurance = false;
    @SerializedName("profile")
    @Expose
    public PatientProfileRequest profile;
    @SerializedName("noks")
    @Expose
    public List<NextOfKinRequest> noks = null;
    @SerializedName("schemes")
    @Expose
    public List<Object> schemes = null;
    @SerializedName("table")
    @Expose
    public String table = "reception_patients";
    @SerializedName("patient")
    @Expose
    public Object patient;
    @SerializedName("populated")
    @Expose
    public Boolean populated = true;

    public void setAge(String age) {
        this.age = age;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public void setProfile(PatientProfileRequest profile) {
        this.profile = profile;
    }

    public void setNoks(List<NextOfKinRequest> noks) {
        this.noks = noks;
    }

    public void setPatient(Object patient) {
        this.patient = patient;
    }

}
