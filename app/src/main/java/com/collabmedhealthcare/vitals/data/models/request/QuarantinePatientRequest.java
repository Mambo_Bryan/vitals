package com.collabmedhealthcare.vitals.data.models.request;

import com.collabmedhealthcare.vitals.data.models.PatientLocationDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuarantinePatientRequest {

    @SerializedName("patient_id")
    @Expose
    public Integer patientId;
    @SerializedName("starting_on")
    @Expose
    public String startingOn;
    @SerializedName("days")
    @Expose
    public String days;
    @SerializedName("place")
    @Expose
    public PatientLocationDetails place;

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public void setStartingOn(String startingOn) {
        this.startingOn = startingOn;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public void setPlace(PatientLocationDetails place) {
        this.place = place;
    }
}
