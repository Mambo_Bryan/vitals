package com.collabmedhealthcare.vitals.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseLinks {
    @SerializedName("first")
    @Expose
    private String first;
    @SerializedName("last")
    @Expose
    private String last;
    @SerializedName("prev")
    @Expose
    private Object prev;
    @SerializedName("next")
    @Expose
    private Object next;

    public String getFirst() {
        return first;
    }

    public String getLast() {
        return last;
    }

    public Object getPrev() {
        return prev;
    }

    public Object getNext() {
        return next;
    }
}
