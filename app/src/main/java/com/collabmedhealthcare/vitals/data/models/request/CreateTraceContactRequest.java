package com.collabmedhealthcare.vitals.data.models.request;

import com.collabmedhealthcare.vitals.data.models.TraceProfile;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateTraceContactRequest {

    @SerializedName("profile")
    @Expose
    private TraceProfile profile;
    @SerializedName("contact_id")
    @Expose
    private String contactId;

    public void setProfile(TraceProfile profile) {
        this.profile = profile;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }
}
