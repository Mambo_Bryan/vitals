package com.collabmedhealthcare.vitals.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SampleRecordRequestBody {

    @SerializedName("other_details")
    @Expose
    private String otherDetails;

    @SerializedName("collection_point_name")
    @Expose
    private String collectionPointName;

    @SerializedName("collection_point")
    @Expose
    private PatientLocationDetails collectionPoint;

    public void setCollectionPointName(String collectionPointName) {
        this.collectionPointName = collectionPointName;
    }

    public void setCollectionPoint(PatientLocationDetails collectionPoint) {
        this.collectionPoint = collectionPoint;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

}
