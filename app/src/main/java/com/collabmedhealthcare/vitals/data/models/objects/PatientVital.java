package com.collabmedhealthcare.vitals.data.models.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PatientVital {

    @SerializedName("visit")
    @Expose
    private Integer visit;
    @SerializedName("bp_systolic")
    @Expose
    private Integer bpSystolic;
    @SerializedName("pulse")
    @Expose
    private Integer pulse;
    @SerializedName("weight")
    @Expose
    private Integer weight;
    @SerializedName("height")
    @Expose
    private Double height;
    @SerializedName("user")
    @Expose
    private Integer user;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public Integer getVisit() {
        return visit;
    }

    public Integer getBpSystolic() {
        return bpSystolic;
    }

    public Integer getPulse() {
        return pulse;
    }

    public Integer getWeight() {
        return weight;
    }

    public Double getHeight() {
        return height;
    }

    public Integer getUser() {
        return user;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}
