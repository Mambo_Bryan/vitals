package com.collabmedhealthcare.vitals.data.models.response;

import com.collabmedhealthcare.vitals.data.models.objects.PatientVital;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VitalResponseBody {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("alert")
    @Expose
    private String alert;

    @SerializedName("vital")
    @Expose
    private PatientVital vital;

    public String getMessage() {
        return message;
    }

    public String getAlert() {
        return alert;
    }

    public PatientVital getVital() {
        return vital;
    }
}
