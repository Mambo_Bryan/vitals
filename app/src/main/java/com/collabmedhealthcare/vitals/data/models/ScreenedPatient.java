package com.collabmedhealthcare.vitals.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.collabmedhealthcare.vitals.data.models.objects.NextOfKin;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ScreenedPatient implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("roll_no")
    @Expose
    private Object rollNo;
    @SerializedName("employee_id")
    @Expose
    private Object employeeId;
    @SerializedName("dependant_id")
    @Expose
    private Object dependantId;
    @SerializedName("patient_no")
    @Expose
    private Integer patientNo;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("inpatient_no")
    @Expose
    private Object inpatientNo;
    @SerializedName("tsc_number")
    @Expose
    private Object tscNumber;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("middle_name")
    @Expose
    private String middleName;
    @SerializedName("alt_number")
    @Expose
    private String altNumber;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("dob_object")
    @Expose
    private DateOfBirthObject dobObject;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("age_number")
    @Expose
    private Object ageNumber;
    @SerializedName("age_in")
    @Expose
    private Object ageIn;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("contacts")
    @Expose
    private List<Contact> contacts = null;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("id_no")
    @Expose
    private String idNo;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    @SerializedName("town")
    @Expose
    private Object town;
    @SerializedName("age_friendly")
    @Expose
    private String ageFriendly;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("joined_on")
    @Expose
    private String joinedOn;
    @SerializedName("pending_invoice_balance")
    @Expose
    private Integer pendingInvoiceBalance;
    @SerializedName("results")
    @Expose
    private List<PatientTestResult> patientTestResults = null;
    @SerializedName("temperature")
    @Expose
    private String temperature;
    @SerializedName("last_collection_point")
    @Expose
    private Object lastCollectionPoint;
    @SerializedName("home_location")
    @Expose
    private PatientLocationDetails homeLocation;
    @SerializedName("work_location")
    @Expose
    private PatientLocationDetails workLocation;
    @SerializedName("vitals")
    @Expose
    private List<Vitals> vitals = null;
    @SerializedName("schemes")
    @Expose
    private List<Object> schemes = null;
    @SerializedName("nok")
    @Expose
    private List<NextOfKin> nok = null;

    protected ScreenedPatient(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            patientNo = null;
        } else {
            patientNo = in.readInt();
        }
        number = in.readString();
        fullName = in.readString();
        firstName = in.readString();
        middleName = in.readString();
        altNumber = in.readString();
        lastName = in.readString();
        dob = in.readString();
        dateOfBirth = in.readString();
        sex = in.readString();
        mobile = in.readString();
        idNo = in.readString();
        email = in.readString();
        telephone = in.readString();
        ageFriendly = in.readString();
        image = in.readString();
        joinedOn = in.readString();
        if (in.readByte() == 0) {
            pendingInvoiceBalance = null;
        } else {
            pendingInvoiceBalance = in.readInt();
        }
        temperature = in.readString();
    }

    public static final Creator<ScreenedPatient> CREATOR = new Creator<ScreenedPatient>() {
        @Override
        public ScreenedPatient createFromParcel(Parcel in) {
            return new ScreenedPatient(in);
        }

        @Override
        public ScreenedPatient[] newArray(int size) {
            return new ScreenedPatient[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public Object getRollNo() {
        return rollNo;
    }

    public Object getEmployeeId() {
        return employeeId;
    }

    public Object getDependantId() {
        return dependantId;
    }

    public Integer getPatientNo() {
        return patientNo;
    }

    public String getNumber() {
        return number;
    }

    public Object getInpatientNo() {
        return inpatientNo;
    }

    public Object getTscNumber() {
        return tscNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getAltNumber() {
        return altNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDob() {
        return dob;
    }

    public DateOfBirthObject getDobObject() {
        return dobObject;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public Object getAgeNumber() {
        return ageNumber;
    }

    public Object getAgeIn() {
        return ageIn;
    }

    public String getSex() {
        return sex;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public String getMobile() {
        return mobile;
    }

    public String getIdNo() {
        return idNo;
    }

    public String getEmail() {
        return email;
    }

    public String getTelephone() {
        return telephone;
    }

    public Object getTown() {
        return town;
    }

    public String getAgeFriendly() {
        return ageFriendly;
    }

    public String getImage() {
        return image;
    }

    public String getJoinedOn() {
        return joinedOn;
    }

    public Integer getPendingInvoiceBalance() {
        return pendingInvoiceBalance;
    }

    public List<PatientTestResult> getPatientTestResults() {
        return patientTestResults;
    }

    public String getTemperature() {
        return temperature;
    }

    public Object getLastCollectionPoint() {
        return lastCollectionPoint;
    }

    public PatientLocationDetails getHomeLocation() {
        return homeLocation;
    }

    public PatientLocationDetails getWorkLocation() {
        return workLocation;
    }

    public List<Vitals> getVitals() {
        return vitals;
    }

    public List<Object> getSchemes() {
        return schemes;
    }

    public List<NextOfKin> getNok() {
        return nok;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (patientNo == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(patientNo);
        }
        dest.writeString(number);
        dest.writeString(fullName);
        dest.writeString(firstName);
        dest.writeString(middleName);
        dest.writeString(altNumber);
        dest.writeString(lastName);
        dest.writeString(dob);
        dest.writeString(dateOfBirth);
        dest.writeString(sex);
        dest.writeString(mobile);
        dest.writeString(idNo);
        dest.writeString(email);
        dest.writeString(telephone);
        dest.writeString(ageFriendly);
        dest.writeString(image);
        dest.writeString(joinedOn);
        if (pendingInvoiceBalance == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(pendingInvoiceBalance);
        }
        dest.writeString(temperature);
    }
}
