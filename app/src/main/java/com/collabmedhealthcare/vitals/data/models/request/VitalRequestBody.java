package com.collabmedhealthcare.vitals.data.models.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VitalRequestBody {

    @SerializedName("covid")
    @Expose
    private Boolean covid = true;
    @SerializedName("patient_id")
    @Expose
    private Integer patientId;
    @SerializedName("temprature")
    @Expose
    private Double temprature;
    @SerializedName("bp_systolic")
    @Expose
    private Integer bpSystolic = 78;
    @SerializedName("bp_diatolic")
    @Expose
    private Integer bpDiatolic = 100;
    @SerializedName("pulse")
    @Expose
    private Integer pulse = 72;
    @SerializedName("weight")
    @Expose
    private Integer weight = 58;
    @SerializedName("height")
    @Expose
    private Double height = 1.28;

    @SerializedName("symptoms")
    @Expose
    private transient List<String> symptoms = null;

    public void setSymptoms(List<String> symptoms) {
        this.symptoms = symptoms;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public void setTemprature(Double temprature) {
        this.temprature = temprature;
    }
}
