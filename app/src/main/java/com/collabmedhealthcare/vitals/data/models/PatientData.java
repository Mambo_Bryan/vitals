package com.collabmedhealthcare.vitals.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PatientData {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("patient")
    @Expose
    private ScreenedPatient patient;
    @SerializedName("visit")
    @Expose
    private Integer visit;

    public String getMessage() {
        return message;
    }

    public ScreenedPatient getPatient() {
        return patient;
    }

    public Integer getVisit() {
        return visit;
    }
}
