package com.collabmedhealthcare.vitals.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SampleRequest {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("patient")
    @Expose
    private String patient;
    @SerializedName("requested_by")
    @Expose
    private String requestedBy;
    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("sample_type")
    @Expose
    private String sampleType;

    @SerializedName("sample_type_id")
    @Expose
    private int sampleTypeId;

    @SerializedName("procedures")
    @Expose
    private Object procedures;
    @SerializedName("status")
    @Expose
    private String status;

    public Integer getId() {
        return id;
    }

    public String getPatient() {
        return patient;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public String getDate() {
        return date;
    }

    public String getSampleType() {
        return sampleType;
    }

    public int getSampleTypeId() {
        return sampleTypeId;
    }

    public Object getProcedures() {
        return procedures;
    }

    public String getStatus() {
        return status;
    }
}
