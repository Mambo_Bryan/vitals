package com.collabmedhealthcare.vitals.data.api;

import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

    //Dynamic method for generating the service
    public static <S> S createService(Class<S> serviceClass) {
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .build();
        // add your other interceptors …
        // add logging as last interceptor
        httpClient.addInterceptor(logging);
        /**
         * Generates a service for the clients that can be used once
         */
        //create retrofit instance
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstantsUtils.BASE_API_URL)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(serviceClass);
    }
}
