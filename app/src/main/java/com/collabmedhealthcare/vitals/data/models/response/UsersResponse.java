package com.collabmedhealthcare.vitals.data.models.response;

import com.collabmedhealthcare.vitals.data.models.ResponseMeta;
import com.collabmedhealthcare.vitals.data.models.objects.SystemUser;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UsersResponse {
    @SerializedName("data")
    @Expose
    public List<SystemUser> systemUsers = null;
    @SerializedName("links")
    @Expose
    public transient String links;
    @SerializedName("meta")
    @Expose
    public ResponseMeta meta;

    public List<SystemUser> getSystemUsers() {
        return systemUsers;
    }

    public ResponseMeta getMeta() {
        return meta;
    }
}
