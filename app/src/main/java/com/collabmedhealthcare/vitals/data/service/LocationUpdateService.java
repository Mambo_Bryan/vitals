package com.collabmedhealthcare.vitals.data.service;

import android.Manifest;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.SampleRequest;
import com.collabmedhealthcare.vitals.data.models.PatientRequestBody;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.response.PatientResponse;
import com.collabmedhealthcare.vitals.utils.LocationUtils;
import com.collabmedhealthcare.vitals.utils.NetworkUtils;
import com.collabmedhealthcare.vitals.utils.NotificationUtils;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class LocationUpdateService extends JobService {

    private SampleRequest request;

    private static String TAG = "LocationUpdate";

    private boolean jobCancelled = false;
    private PatientRequestBody myPatientRequestBody;

    @Override
    public boolean onStartJob(JobParameters params) {

        updatePatientLocation(params);

        return true;
    }

    private boolean checkPermissions() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED;
    }


    private void updatePatientLocation(JobParameters params) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());

                if (checkPermissions()) {

                    if (!LocationUtils.isLocationEnabled(getApplicationContext())) {

                        notificationUtils.makeImportantNotificationWithFunctionAndButton("Location Services",
                                "Please turn on your location for updates");

                        jobFinished(params, true);

                    } else if (!NetworkUtils.isNetworkEnabled(getApplicationContext())) {

                        notificationUtils.makeImportantNotificationWithFunctionAndButton("Location Services",
                                "Please turn on your location for updates");

                        jobFinished(params, true);

                    } else {
                        myPatientRequestBody = new PatientRequestBody();

                        createNewPatient();

                        SharedPrefsUtil prefsUtil = SharedPrefsUtil.getInstance(getApplicationContext());

                        ApiService apiService = ApiClient.createService(ApiService.class);
                        Call<PatientResponse> call = apiService.screenNewPatient(prefsUtil.getUserAccessToken(), myPatientRequestBody);
                        call.enqueue(new Callback<PatientResponse>() {
                            @Override
                            public void onResponse(Call<PatientResponse> call, Response<PatientResponse> response) {
                                if (response.isSuccessful() && response.body() != null) {
                                    jobFinished(params, false);
                                    return;
                                }

                                jobFinished(params, true);

                            }

                            @Override
                            public void onFailure(Call<PatientResponse> call, Throwable t) {
                                jobFinished(params, true);
                            }
                        });
                    }
                } else {

                    notificationUtils.makeImportantNotificationWithFunction("App Permissions",
                            "Open app to ensure all permissions are granted");

                    jobFinished(params, true);
                }
            }
        }).start();
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        jobCancelled = true;
        return true;
    }

    private void createNewPatient() {

        Random random = new Random();
        int number = random.nextInt(100);

        ArrayList<String> symptoms = new ArrayList<>();
        symptoms.add("Test Fever");
        symptoms.add("Test Cough");

        myPatientRequestBody = new PatientRequestBody();

//        myPatientRequestBody.setName("Jambo Test " + number);
//        myPatientRequestBody.setIdNo("0001" + number);
//        myPatientRequestBody.setAge(number);
//        myPatientRequestBody.setMobile("0001" + number);
//        myPatientRequestBody.setHome(getPlace());
//        myPatientRequestBody.setHomeName("Collabmed");
//        myPatientRequestBody.setWork(getPlace());
//        myPatientRequestBody.setWorkName("Embu");
//        myPatientRequestBody.setTemperature((double) number);
//        myPatientRequestBody.setVisitedPlaces("Canary, Arctic");
//        myPatientRequestBody.setGender("male");
//        myPatientRequestBody.setSymptoms(symptoms);
//        myPatientRequestBody.setTested(false);
//        myPatientRequestBody.setResults(false);
//        myPatientRequestBody.setCollectionPoint(getPlace());

    }

    public String getPlace() {
        // Add a marker in Sydney and move the camera
        double latitude = -1.279275;
        double longitude = 36.770232;

        LatLng lavington = new LatLng(latitude, longitude);
        return lavington.latitude + " , " + lavington.longitude;
    }

}
