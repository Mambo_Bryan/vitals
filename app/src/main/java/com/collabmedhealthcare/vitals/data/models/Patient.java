package com.collabmedhealthcare.vitals.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Patient {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("roll_no")
    @Expose
    public Object rollNo;
    @SerializedName("employee_id")
    @Expose
    public Object employeeId;
    @SerializedName("dependant_id")
    @Expose
    public Object dependantId;
    @SerializedName("patient_no")
    @Expose
    public Integer patientNo;
    @SerializedName("number")
    @Expose
    public String number;
    @SerializedName("inpatient_no")
    @Expose
    public Object inpatientNo;
    @SerializedName("tsc_number")
    @Expose
    public Object tscNumber;
    @SerializedName("full_name")
    @Expose
    public String fullName;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("middle_name")
    @Expose
    public String middleName;
    @SerializedName("alt_number")
    @Expose
    public Object altNumber;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("dob")
    @Expose
    public String dob;
    @SerializedName("dob_object")
    @Expose
    public transient DateOfBirthObject dobObject;
    @SerializedName("date_of_birth")
    @Expose
    public String dateOfBirth;
    @SerializedName("age_number")
    @Expose
    public Object ageNumber;
    @SerializedName("age_in")
    @Expose
    public Object ageIn;
    @SerializedName("home")
    @Expose
    public String home;
    @SerializedName("work")
    @Expose
    public String work;
    @SerializedName("sex")
    @Expose
    public String sex;
    @SerializedName("contacts")
    @Expose
    public List<Object> contacts = null;
    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("id_no")
    @Expose
    public String idNo;
    @SerializedName("email")
    @Expose
    public Object email;
    @SerializedName("telephone")
    @Expose
    public Object telephone;
    @SerializedName("town")
    @Expose
    public Object town;
    @SerializedName("age_friendly")
    @Expose
    public String ageFriendly;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("joined_on")
    @Expose
    public String joinedOn;
    @SerializedName("pending_invoice_balance")
    @Expose
    public Integer pendingInvoiceBalance;
    @SerializedName("results")
    @Expose
    public List<Object> results = null;
    @SerializedName("home_name")
    @Expose
    public Object homeName;
    @SerializedName("work_name")
    @Expose
    public Object workName;
    @SerializedName("temperature")
    @Expose
    public String temperature;
    @SerializedName("last_collection_point")
    @Expose
    public Object lastCollectionPoint;
    @SerializedName("home_lat")
    @Expose
    public Object homeLat;
    @SerializedName("home_long")
    @Expose
    public Object homeLong;
    @SerializedName("work_lat")
    @Expose
    public Object workLat;
    @SerializedName("work_long")
    @Expose
    public Object workLong;
    @SerializedName("vitals")
    @Expose
    public List<Vitals> vitals = null;
    @SerializedName("schemes")
    @Expose
    public transient List<Object> schemes = null;
    @SerializedName("nok")
    @Expose
    public transient List<String> nok = null;

    public Integer getId() {
        return id;
    }

    public Object getRollNo() {
        return rollNo;
    }

    public Object getEmployeeId() {
        return employeeId;
    }

    public Object getDependantId() {
        return dependantId;
    }

    public Integer getPatientNo() {
        return patientNo;
    }

    public String getNumber() {
        return number;
    }

    public Object getInpatientNo() {
        return inpatientNo;
    }

    public Object getTscNumber() {
        return tscNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Object getAltNumber() {
        return altNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDob() {
        return dob;
    }

    public DateOfBirthObject getDobObject() {
        return dobObject;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public Object getAgeNumber() {
        return ageNumber;
    }

    public Object getAgeIn() {
        return ageIn;
    }

    public String getSex() {
        return sex;
    }

    public String getMobile() {
        return mobile;
    }

    public String getIdNo() {
        return idNo;
    }

    public Object getEmail() {
        return email;
    }

    public Object getTelephone() {
        return telephone;
    }

    public Object getTown() {
        return town;
    }

    public String getAgeFriendly() {
        return ageFriendly;
    }

    public String getImage() {
        return image;
    }

    public String getJoinedOn() {
        return joinedOn;
    }

    public Integer getPendingInvoiceBalance() {
        return pendingInvoiceBalance;
    }

    public List<Object> getSchemes() {
        return schemes;
    }

}
