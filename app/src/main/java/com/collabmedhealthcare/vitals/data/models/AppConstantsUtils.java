package com.collabmedhealthcare.vitals.data.models;

public class AppConstantsUtils {

    public static final String BASE_API_URL = "https://hmis.collabmed.net/api/";
    public static String LOCATION_API_KEY = "AIzaSyBOj4Cu-TyqCOStDvas3n4ZckEEjxnlljQ";

    public static String PATIENT_NAME_STRING = "patient_name";
    public static String PATIENT_ID_STRING = "patient_ID";
    public static String[] accountTypes = {"Patient", "Responder"};

    public static final String NOTIFICATION_CHANNEL_1_NAME = "channel1";
    public static final int NOTIFICATION_CHANNEL_1_ID = 800;
    public static final String NOTIFICATION_CHANNEL_2_NAME = "channel2";
    public static final int NOTIFICATION_CHANNEL_2_ID = 801;

    //prefs strings
    public static String PREFS_STRING = "sharedPrefs";
    public static String USERNAME_STRING = "username";
    public static String ACCESS_TOKEN_STRING = "accessToken";
    public static String IS_USER_LOGGED_IN = "isUserLoggedIn";
    public static String IS_ALREADY_SET_UP = "isSettingUp";
    public static String IS_FIRST_TIME_OPENING = "isFirstTimeOpening";
    public static String USER_REGION_STRING = "userRegion";
    public static String URLS_STRING = "userUrls";

    public static int COVID_19_CODE = 6370;
    public static int CODE_TRACING_ACTIVITY = 201;
    public static int CODE_SCREENED_ACTIVITY = 202;

    //Intent Extras
    public static final String GOOGLE_MAPS_LOCATION_EXTRA = "maps_location";
    public static final String SCAN_BARCODE_EXTRA = "barcode_data";
    public static final String GOOGLE_MAPS_BUNDLE_EXTRA = "bundle";
    public static final String ON_BOARD_ACCOUNT_TYPE_EXTRA = "account_type";
    public static final String GENERAL_TRACE_CONTACT_EXTRA = "trace_contact_extra";
    public static final String SPECIFIC_TRACE_CONTACT_EXTRA = "specific_trace_contact_extra";

    public static final String SCREENED_PATIENT_ACTIVITY_EXTRA = "screened_patient_extra";
    public static final String SCREENED_PATIENT_ACTIVITY_CODE_EXTRA = "screened_patient_code_extra";

    public static final String SAMPLE_COLLECTION_ACTIVITY_EXTRA = "sample_collection_extra";
    public static final String SAMPLE_COLLECTION_ACTIVITY_CODE_EXTRA = "sample_collection_code_extra";

    public static final int CODE_SAMPLE_LIST_ACTIVITY = 901;
    public static final int CODE_ADD_PATIENT_ACTIVITY = 902;

    public static final int PATIENT_ADD = 700;
    public static final int PATIENT_EDIT = 701;
    public static final int PATIENT_ADD_FROM_CONTACT = 702;

    public static final int REQUEST_CODE_MAPS_LOCATION_HOME = 100;
    public static final int REQUEST_CODE_MAPS_LOCATION_WORk = 101;
    public static final int REQUEST_CODE_MAPS_LOCATION_COLLECTION_POINT = 102;
    public static final int REQUEST_CODE_BARCODE_SCAN = 103;
    public static final int REQUEST_CODE_ADD_QUARANTINE_PATIENT = 105;

    public static final String CONTACT_TRACE_STRING_EXTRA = "contact_trace";
    public static final int LEVEL_1_CONTACT_TRACE_INT = 501;
    public static final int LEVEL_1_CONTACT_TRACE_UPDATE_INT = 502;
    public static final int LEVEL_2_CONTACT_TRACE_INT = 503;
    public static final int LEVEL_2_CONTACT_TRACE_UPDATE_INT = 504;

    public static final int ERROR = -3;

    //Request codes
    public static final int REQUEST_CODE_TAKE_PHOTO = 0;
    public static final int REQUEST_CODE_CHOOSE_FROM_GALLERY = 1;

    //permission code
    public static final int ZXING_CAMERA_PERMISSION = 900;

}
