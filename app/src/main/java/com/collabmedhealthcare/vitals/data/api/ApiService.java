package com.collabmedhealthcare.vitals.data.api;

import com.collabmedhealthcare.vitals.data.models.LoginRequest;
import com.collabmedhealthcare.vitals.data.models.PatientRequestBody;
import com.collabmedhealthcare.vitals.data.models.SampleRecordRequestBody;
import com.collabmedhealthcare.vitals.data.models.request.ContactTraceRequest;
import com.collabmedhealthcare.vitals.data.models.request.CreateTraceContactRequest;
import com.collabmedhealthcare.vitals.data.models.request.QuarantinePatientRequest;
import com.collabmedhealthcare.vitals.data.models.request.RequestSampleBody;
import com.collabmedhealthcare.vitals.data.models.request.UpdatePatientRequest;
import com.collabmedhealthcare.vitals.data.models.request.VitalRequestBody;
import com.collabmedhealthcare.vitals.data.models.response.CollectionResponse;
import com.collabmedhealthcare.vitals.data.models.response.ContactTraceDetailsResponse;
import com.collabmedhealthcare.vitals.data.models.response.ContactsTracingResponse;
import com.collabmedhealthcare.vitals.data.models.response.DefaultMessageResponse;
import com.collabmedhealthcare.vitals.data.models.response.DetailsResponse;
import com.collabmedhealthcare.vitals.data.models.response.LoginResponse;
import com.collabmedhealthcare.vitals.data.models.response.PatientResponse;
import com.collabmedhealthcare.vitals.data.models.response.QuarantinedPatientsResponse;
import com.collabmedhealthcare.vitals.data.models.response.SampleRequestsResponse;
import com.collabmedhealthcare.vitals.data.models.response.ScreenedPatientsResponse;
import com.collabmedhealthcare.vitals.data.models.response.UpdateResponse;
import com.collabmedhealthcare.vitals.data.models.response.UsersResponse;
import com.collabmedhealthcare.vitals.data.models.response.VitalResponseBody;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    /**
     * TODO add method description and body
     *
     * @return
     */
    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @POST("users/authenticate")
    Call<LoginResponse> loginUser(
            @Body LoginRequest loginRequest);

    /**
     * TODO add method description and body
     *
     * @return
     */
    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @GET("reception/patients")
    Call<DetailsResponse> getPatientDetails(
            @Header("Authorization") String token,
            @Query("system_id") String digitalId);

    /**
     * TODO add method description and body
     *
     * @return
     */
    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @POST("reception/patients")
    Call<PatientResponse> screenNewPatient(
            @Header("Authorization") String token,
            @Body PatientRequestBody patientRequestBody);

    /**
     * TODO add method description and body
     *
     * @return
     */
    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @PATCH("reception/patients/{id}/")
    Call<UpdateResponse> updateScreenedPatient(
            @Header("Authorization") String token,
            @Path("id") int patientId,
            @Body UpdatePatientRequest updatePatientRequest);

    /**
     * TODO add method description and body
     *
     * @return
     */
    @Headers("Content-Type:application/json")
    @GET("evaluation/sampleCollectionRequests")
    Call<SampleRequestsResponse> getSampleRequests(
            @Header("Authorization") String token,
            @Query("page") int page);

    /**
     * TODO add method description and body
     *
     * @return
     */
    @Headers("Content-Type:application/json")
    @GET("evaluation/sampleCollectionRequests")
    Call<SampleRequestsResponse> getSearchedSampleRequests(
            @Header("Authorization") String token,
            @Query("search") String query,
            @Query("page") int page);

    /**
     * TODO add method description and body
     *
     * @return
     */
    @Headers("Content-Type:application/json")
    @GET("evaluation/sampleCollectionRequests")
    Call<SampleRequestsResponse> getPatientTestHistory(
            @Header("Authorization") String token,
            @Query("search") String query,
            @Query("page") int page);

    /**
     * TODO add method description and body
     *
     * @return
     */
    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @GET("reception/patients")
    Call<ScreenedPatientsResponse> getScreenedPatients(
            @Header("Authorization") String token,
            @Query("page") int page,
            @Query("screened") boolean screened
    );

    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @POST("evaluation/vitals")
    Call<VitalResponseBody> sendVitalsToServer(
            @Header("Authorization") String token,
            @Body VitalRequestBody request);

    /**
     * TODO add method description and body
     *
     * @return
     */
    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @GET("reception/patients")
    Call<ScreenedPatientsResponse> getSearchedScreenedPatient(
            @Header("Authorization") String token,
            @Query("search") String patientName,
            @Query("screened") boolean screened,
            @Query("page") int page);

    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @GET("reception/patients")
    Call<ScreenedPatientsResponse> getSearchedPatient(
            @Header("Authorization") String token,
            @Query("search") String patientName,
            @Query("shallow") boolean screened);

    /**
     * TODO add method description and body
     *
     * @return
     */
    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @GET("reception/contactTracing")
    Call<ContactsTracingResponse> getGeneralContactTracingList(
            @Header("Authorization") String token,
            @Query("page") int page);


    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @GET("reception/contactTracing")
    Call<ContactsTracingResponse> getSearchedGeneralContactTracingList(
            @Header("Authorization") String token,
            @Query("search") String contactName,
            @Query("page") int page
    );

    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @POST("reception/contact-tracing")
    Call<DefaultMessageResponse> createNewContactTrace(
            @Header("Authorization") String token,
            @Body ContactTraceRequest request);

    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @GET("reception/contactTracingDetails")
    Call<ContactTraceDetailsResponse> getSpecificContactTracingDetails(
            @Header("Authorization") String token,
            @Query("contact_id") int id,
            @Query("page") int page);

    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @GET("reception/contactTracingDetails")
    Call<ContactTraceDetailsResponse> getSearchedSpecificContactTracingDetails(
            @Header("Authorization") String token,
            @Query("contact_id") int id,
            @Query("search") String query,
            @Query("page") int page);

    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @POST("reception/contact-tracing/save_details")
    Call<DefaultMessageResponse> createNewContactTracePerson(
            @Header("Authorization") String token,
            @Body CreateTraceContactRequest request
    );

    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @POST("evaluation/sample-collection/request-sample/{patient_id}")
    Call<DefaultMessageResponse> requestSampleCollection(
            @Header("Authorization") String token,
            @Path("patient_id") int id,
            @Body RequestSampleBody body
    );

    /**
     * TODO add method description and body
     *
     * @return
     */
    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @POST("evaluation/sample-collection/{id}/from-request")
    Call<CollectionResponse> recordSampleCollection(
            @Header("Authorization") String token,
            @Path("id") int patientId,
            @Body SampleRecordRequestBody collection);

    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @GET("evaluation/selfQuarantinePatients")
    Call<QuarantinedPatientsResponse> getQuarantinedPatients(
            @Header("Authorization") String token,
            @Query("page") int page);

    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @GET("evaluation/selfQuarantinePatients")
    Call<QuarantinedPatientsResponse> getSearchedQuarantinedPatients(
            @Header("Authorization") String token,
            @Query("search") String query,
            @Query("page") int page);

    @Headers({"Content-Type:application/json", "X-Requested-With:XMLHttpRequest"})
    @POST("evaluation/selfQuarantinePatients/store")
    Call<DefaultMessageResponse> addQuarantinePatient(
            @Header("Authorization") String token,
            @Body QuarantinePatientRequest body
    );

    @Headers({"Accept:application/json", "X-Requested-With:XMLHttpRequest"})
    @GET("users")
    Call<UsersResponse> getUsers(
            @Header("Authorization") String token,
            @Query("users_list") boolean isUsersList,
            @Query("page") int page);

    @Headers({"Accept:application/json", "X-Requested-With:XMLHttpRequest"})
    @GET("users")
    Call<UsersResponse> getSearchedUsers(
            @Header("Authorization") String token,
            @Query("users_list") boolean isUsersList,
            @Query("search") String query,
            @Query("page") int page);

}
