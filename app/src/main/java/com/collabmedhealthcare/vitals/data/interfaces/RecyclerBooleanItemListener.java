package com.collabmedhealthcare.vitals.data.interfaces;

public interface RecyclerBooleanItemListener {
    void onBooleanItemSelected(boolean isSelected, int position);
}
