package com.collabmedhealthcare.vitals.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DummyPatient {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("visit")
    @Expose
    private Integer visit;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("temprature")
    @Expose
    private Double temprature;
    @SerializedName("tested")
    @Expose
    private Boolean tested;
    @SerializedName("tested_result")
    @Expose
    private String testedResult;
    @SerializedName("Home")
    @Expose
    private String home;
    @SerializedName("sample_collected")
    @Expose
    private Boolean sampleCollected;
    @SerializedName("traced")
    @Expose
    private Boolean traced;
    @SerializedName("traced_status")
    @Expose
    private String tracedStatus;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setVisit(Integer visit) {
        this.visit = visit;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTemprature(Double temprature) {
        this.temprature = temprature;
    }

    public void setTested(Boolean tested) {
        this.tested = tested;
    }

    public void setTestedResult(String testedResult) {
        this.testedResult = testedResult;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public void setSampleCollected(Boolean sampleCollected) {
        this.sampleCollected = sampleCollected;
    }

    public void setTraced(Boolean traced) {
        this.traced = traced;
    }

    public void setTracedStatus(String tracedStatus) {
        this.tracedStatus = tracedStatus;
    }

    public Integer getId() {
        return id;
    }

    public Integer getVisit() {
        return visit;
    }

    public String getName() {
        return name;
    }

    public Double getTemprature() {
        return temprature;
    }

    public Boolean getTested() {
        return tested;
    }

    public String getTestedResult() {
        return testedResult;
    }

    public String getHome() {
        return home;
    }

    public Boolean getSampleCollected() {
        return sampleCollected;
    }

    public Boolean getTraced() {
        return traced;
    }

    public String getTracedStatus() {
        return tracedStatus;
    }
}
