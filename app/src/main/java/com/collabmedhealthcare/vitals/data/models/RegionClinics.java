package com.collabmedhealthcare.vitals.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegionClinics {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("practice")
    @Expose
    private Integer practice;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    @SerializedName("fax")
    @Expose
    private Object fax;
    @SerializedName("mobile")
    @Expose
    private Object mobile;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("town")
    @Expose
    private String town;
    @SerializedName("location")
    @Expose
    private Object location;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("building")
    @Expose
    private String building;
    @SerializedName("office")
    @Expose
    private String office;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("logo")
    @Expose
    private Object logo;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("region_id")
    @Expose
    private Integer regionId;
    @SerializedName("pivot")
    @Expose
    private transient String  pivot;

    public Integer getId() {
        return id;
    }

    public Integer getPractice() {
        return practice;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getTelephone() {
        return telephone;
    }

    public Object getFax() {
        return fax;
    }

    public Object getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }

    public String getTown() {
        return town;
    }

    public Object getLocation() {
        return location;
    }

    public String getStreet() {
        return street;
    }

    public String getBuilding() {
        return building;
    }

    public String getOffice() {
        return office;
    }

    public String getStatus() {
        return status;
    }

    public String getType() {
        return type;
    }

    public Object getLogo() {
        return logo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public Object getDescription() {
        return description;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public String getPivot() {
        return pivot;
    }
}
