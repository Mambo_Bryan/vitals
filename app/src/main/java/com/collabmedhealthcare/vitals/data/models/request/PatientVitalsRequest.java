package com.collabmedhealthcare.vitals.data.models.request;

import com.collabmedhealthcare.vitals.data.models.PatientLocationDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PatientVitalsRequest {

    @SerializedName("temperature")
    @Expose
    private Double temperature;
    @SerializedName("visited_places")
    @Expose
    private String visitedPlaces;
    @SerializedName("symptoms")
    @Expose
    private List<String> symptoms = null;
    @SerializedName("collection_point")
    @Expose
    private PatientLocationDetails collectionPoint;

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public void setVisitedPlaces(String visitedPlaces) {
        this.visitedPlaces = visitedPlaces;
    }

    public void setSymptoms(List<String> symptoms) {
        this.symptoms = symptoms;
    }

    public void setCollectionPoint(PatientLocationDetails collectionPoint) {
        this.collectionPoint = collectionPoint;
    }
}
