package com.collabmedhealthcare.vitals.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TracePatient {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("roll_no")
    @Expose
    private Object rollNo;
    @SerializedName("employee_id")
    @Expose
    private Object employeeId;
    @SerializedName("dependant_id")
    @Expose
    private Object dependantId;
    @SerializedName("patient_no")
    @Expose
    private Integer patientNo;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("inpatient_no")
    @Expose
    private Object inpatientNo;
    @SerializedName("tsc_number")
    @Expose
    private Object tscNumber;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("middle_name")
    @Expose
    private String middleName;
    @SerializedName("alt_number")
    @Expose
    private Object altNumber;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("dob_object")
    @Expose
    private transient Object dobObject;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("age_number")
    @Expose
    private Object ageNumber;
    @SerializedName("age_in")
    @Expose
    private Object ageIn;
    @SerializedName("home")
    @Expose
    private String home;
    @SerializedName("work")
    @Expose
    private String work;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("contacts")
    @Expose
    private List<Object> contacts = null;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("id_no")
    @Expose
    private String idNo;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("telephone")
    @Expose
    private Object telephone;
    @SerializedName("town")
    @Expose
    private Object town;
    @SerializedName("age_friendly")
    @Expose
    private String ageFriendly;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("joined_on")
    @Expose
    private String joinedOn;
    @SerializedName("pending_invoice_balance")
    @Expose
    private Integer pendingInvoiceBalance;
    @SerializedName("results")
    @Expose
    private List<Object> results = null;
    @SerializedName("home_name")
    @Expose
    private Object homeName;
    @SerializedName("work_name")
    @Expose
    private Object workName;
    @SerializedName("temperature")
    @Expose
    private Object temperature;
    @SerializedName("last_collection_point")
    @Expose
    private Object lastCollectionPoint;
    @SerializedName("home_lat")
    @Expose
    private Object homeLat;
    @SerializedName("home_long")
    @Expose
    private Object homeLong;
    @SerializedName("work_lat")
    @Expose
    private Object workLat;
    @SerializedName("work_long")
    @Expose
    private Object workLong;
    @SerializedName("vitals")
    @Expose
    private List<Object> vitals = null;
    @SerializedName("schemes")
    @Expose
    private List<Object> schemes = null;
    @SerializedName("nok")
    @Expose
    private List<Object> nok = null;
}
