package com.collabmedhealthcare.vitals.data.models.response;

import com.collabmedhealthcare.vitals.data.models.QuarantinedPatient;
import com.collabmedhealthcare.vitals.data.models.ResponseLinks;
import com.collabmedhealthcare.vitals.data.models.ResponseMeta;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuarantinedPatientsResponse {

    @SerializedName("data")
    @Expose
    public List<QuarantinedPatient> quarantinedPatientList = null;
    @SerializedName("links")
    @Expose
    public ResponseLinks links;
    @SerializedName("meta")
    @Expose
    public ResponseMeta meta;

    public List<QuarantinedPatient> getQuarantinedPatientList() {
        return quarantinedPatientList;
    }

    public ResponseMeta getMeta() {
        return meta;
    }
}
