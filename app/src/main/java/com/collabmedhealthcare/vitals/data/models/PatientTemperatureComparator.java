package com.collabmedhealthcare.vitals.data.models;

import java.util.Comparator;

public class PatientTemperatureComparator implements Comparator<DummyPatient> {

    @Override
    public int compare(DummyPatient o1, DummyPatient o2) {
        return o2.getTemprature().compareTo(o1.getTemprature());
    }

}
