package com.collabmedhealthcare.vitals.data.models.response;

import com.collabmedhealthcare.vitals.data.models.PatientData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PatientResponse {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("alert")
    @Expose
    private String alert;

    @SerializedName("patient_id")
    @Expose
    private PatientData patientId;

    public String getMessage() {
        return message;
    }

    public PatientData getPatientData() {
        return patientId;
    }

    public String getAlert() {
        return alert;
    }
}
