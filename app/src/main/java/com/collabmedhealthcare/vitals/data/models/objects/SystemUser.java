package com.collabmedhealthcare.vitals.data.models.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SystemUser {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("full_name")
    @Expose
    public String fullName;
    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("active")
    @Expose
    public Integer active;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("roles")
    @Expose
    public String roles;
    @SerializedName("username")
    @Expose
    public String username;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public String getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }

    public Integer getActive() {
        return active;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getRoles() {
        return roles;
    }

    public String getUsername() {
        return username;
    }
}
