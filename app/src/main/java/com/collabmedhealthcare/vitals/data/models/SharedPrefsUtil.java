package com.collabmedhealthcare.vitals.data.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SharedPrefsUtil {

    private static SharedPrefsUtil prefsUtil;
    private static SharedPreferences sharedPreferences;

    private Context context;

    private SharedPrefsUtil(Context context) {
        sharedPreferences = context.getSharedPreferences(AppConstantsUtils.PREFS_STRING,
                Context.MODE_PRIVATE);

        this.context = context;

    }

    // singleton pattern for instance of the class
    public static SharedPrefsUtil getInstance(Context context) {
        if (prefsUtil == null) {
            prefsUtil = new SharedPrefsUtil(context);
        }
        return prefsUtil;
    }

    /**
     * Completely clears all user details in the app
     */
    public void clearAllPreferencesData() {
        sharedPreferences.edit().clear().apply();
    }

    /**
     * TODO enter method purpose
     *
     * @return
     */
    public boolean isUserLoggedIn() {
        return sharedPreferences.getBoolean(AppConstantsUtils.IS_USER_LOGGED_IN, false);
    }

    /**
     * Stores user has logged in to shared preferences
     */
    public void setUserHasLoggedIn() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(AppConstantsUtils.IS_USER_LOGGED_IN, true);
        editor.apply();

    }

    /**
     * Stores user has logged in to shared preferences
     */
    public void setUserHasSetUp() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(AppConstantsUtils.IS_ALREADY_SET_UP, true);
        editor.apply();
    }

    public boolean getIsUserSettingUp() {
        return sharedPreferences.getBoolean(AppConstantsUtils.IS_ALREADY_SET_UP, false);
    }

    /**
     * Stores user has logged in to shared preferences
     */
    public void setOpenedApp() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(AppConstantsUtils.IS_FIRST_TIME_OPENING, true);
        editor.apply();
    }

    public boolean getIsFirstTimeOpening() {
        return sharedPreferences.getBoolean(AppConstantsUtils.IS_FIRST_TIME_OPENING, false);
    }

    /**
     * Stores user has logged in to shared preferences
     */
    public void storeAccessToken(String token) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(AppConstantsUtils.ACCESS_TOKEN_STRING, token);
        editor.apply();
    }

    /**
     * Stores user has logged in to shared preferences
     */
    public void storeUsername(String username) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(AppConstantsUtils.USERNAME_STRING, username);
        editor.apply();
    }

    /**
     * TODO enter method purpose
     *
     * @return
     */
    public String getUsername() {
        return sharedPreferences.getString(AppConstantsUtils.USERNAME_STRING,
                null);
    }


    /**
     * TODO enter method purpose
     *
     * @return
     */
    public String getUserAccessToken() {
        return "Bearer " + sharedPreferences.getString(AppConstantsUtils.ACCESS_TOKEN_STRING,
                "");
    }

    public void storeUserRegion(UserRegion region) {

        SharedPreferences.Editor editor = sharedPreferences.edit();

        Gson gson = new Gson();

        String userRegionString = gson.toJson(region, UserRegion.class);

        editor.putString(AppConstantsUtils.USER_REGION_STRING, userRegionString);

        editor.apply();

    }

    public UserRegion getResponderRegion() {

        String userRegionString = sharedPreferences.getString(AppConstantsUtils.USER_REGION_STRING,
                null);

        Gson gson = new Gson();
        UserRegion region = gson.fromJson(userRegionString, UserRegion.class);

        return region;

    }

    public void storeUrls(ArrayList<BaseUrl> urls) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();

        String json = gson.toJson(urls);

        editor.putString(AppConstantsUtils.URLS_STRING, json);
        editor.apply();

    }

    public List<BaseUrl> getUrls() {

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = sharedPrefs.getString(AppConstantsUtils.URLS_STRING, null);

        if (json != null) {
            Type type = new TypeToken<List<BaseUrl>>() {
            }.getType();
            return gson.fromJson(json, type);
        } else {
            return null;
        }

    }


}
