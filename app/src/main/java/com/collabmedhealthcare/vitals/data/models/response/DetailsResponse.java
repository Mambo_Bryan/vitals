package com.collabmedhealthcare.vitals.data.models.response;

import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailsResponse {

    @SerializedName("data")
    @Expose
    public ScreenedPatient patient;

    public ScreenedPatient getPatient() {
        return patient;
    }
}
