package com.collabmedhealthcare.vitals.data.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Header {

    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("sortable")
    @Expose
    private Boolean sortable;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("align")
    @Expose
    private String align;

    public String getText() {
        return text;
    }

    public Boolean getSortable() {
        return sortable;
    }

    public String getValue() {
        return value;
    }

    public String getAlign() {
        return align;
    }
}
