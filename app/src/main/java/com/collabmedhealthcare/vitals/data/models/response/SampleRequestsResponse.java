package com.collabmedhealthcare.vitals.data.models.response;

import com.collabmedhealthcare.vitals.data.models.ResponseLinks;
import com.collabmedhealthcare.vitals.data.models.ResponseMeta;
import com.collabmedhealthcare.vitals.data.models.SampleRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SampleRequestsResponse {

    @SerializedName("data")
    @Expose
    private List<SampleRequest> sampleRequests = null;
    @SerializedName("links")
    @Expose
    private ResponseLinks responseLinks;
    @SerializedName("meta")
    @Expose
    private ResponseMeta meta;

    public List<SampleRequest> getSampleRequests() {
        return sampleRequests;
    }

    public ResponseLinks getResponseLinks() {
        return responseLinks;
    }

    public ResponseMeta getMeta() {
        return meta;
    }
}
