package com.collabmedhealthcare.vitals.data.models.response;

import com.collabmedhealthcare.vitals.data.models.ResponseLinks;
import com.collabmedhealthcare.vitals.data.models.ResponseMeta;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ScreenedPatientsResponse {
    @SerializedName("data")
    @Expose
    private List<ScreenedPatient> screenedPatients = null;
    @SerializedName("headers")
    @Expose
    private List<Header> headers = null;
    @SerializedName("links")
    @Expose
    private ResponseLinks responseLinks;
    @SerializedName("meta")
    @Expose
    private ResponseMeta meta;

    public List<ScreenedPatient> getScreenedPatients() {
        return screenedPatients;
    }

    public List<Header> getHeaders() {
        return headers;
    }

    public ResponseLinks getResponseLinks() {
        return responseLinks;
    }

    public ResponseMeta getMeta() {
        return meta;
    }
}
