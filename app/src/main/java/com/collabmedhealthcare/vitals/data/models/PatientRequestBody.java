package com.collabmedhealthcare.vitals.data.models;

import com.collabmedhealthcare.vitals.data.models.request.NextOfKinRequest;
import com.collabmedhealthcare.vitals.data.models.request.PatientProfileRequest;
import com.collabmedhealthcare.vitals.data.models.request.PatientVitalsRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PatientRequestBody {

    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("age_in")
    @Expose
    private String ageIn = "years";
    @SerializedName("profile")
    @Expose
    private PatientProfileRequest profile;
    @SerializedName("vitals")
    @Expose
    private PatientVitalsRequest vitals;
    @SerializedName("noks")
    @Expose
    private List<NextOfKinRequest> kins = null;
    @SerializedName("hasInsurance")
    @Expose
    private Boolean hasInsurance = false;
    @SerializedName("screening")
    @Expose
    private Boolean screening = true;
    @SerializedName("schemes")
    @Expose
    private List<String> schemes = new ArrayList<>();

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setProfile(PatientProfileRequest profile) {
        this.profile = profile;
    }

    public void setVitals(PatientVitalsRequest vitals) {
        this.vitals = vitals;
    }

    public void setKins(List<NextOfKinRequest> kins) {
        this.kins = kins;
    }
}
