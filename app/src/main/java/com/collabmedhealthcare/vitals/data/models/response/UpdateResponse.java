package com.collabmedhealthcare.vitals.data.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateResponse {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("alert")
    @Expose
    private String alert;

    @SerializedName("patient_id")
    @Expose
    private int patientId;

    public String getMessage() {
        return message;
    }

    public int getPatientData() {
        return patientId;
    }

    public String getAlert() {
        return alert;
    }
}
