package com.collabmedhealthcare.vitals.data.models.objects;

public class SampleType {

    int sampleId;
    String sampleName;

    public SampleType(int sampleId, String sampleName) {
        this.sampleId = sampleId;
        this.sampleName = sampleName;
    }

    public int getSampleId() {
        return sampleId;
    }

    public String getSampleName() {
        return sampleName;
    }
}
