package com.collabmedhealthcare.vitals.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactTracingDetailsData {

    @SerializedName("contact_id")
    @Expose
    private Integer contactId;
    @SerializedName("patient_id")
    @Expose
    private Integer patientId;
    @SerializedName("patient_name")
    @Expose
    private String patientName;
    @SerializedName("patient")
    @Expose
    private ScreenedPatient patient;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("tracked_by")
    @Expose
    private Object trackedBy;
    @SerializedName("comments")
    @Expose
    private Object comments;

    public Integer getContactId() {
        return contactId;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public ScreenedPatient getPatient() {
        return patient;
    }

    public String getStatus() {
        return status;
    }

    public Object getTrackedBy() {
        return trackedBy;
    }

    public Object getComments() {
        return comments;
    }
}
