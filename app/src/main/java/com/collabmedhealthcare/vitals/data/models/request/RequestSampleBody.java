package com.collabmedhealthcare.vitals.data.models.request;

import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestSampleBody {

    @SerializedName("procedure")
    @Expose
    int procedure = AppConstantsUtils.COVID_19_CODE;

    @SerializedName("sample_type")
    @Expose
    private int sampleType;

    @SerializedName("other_details")
    @Expose
    private String otherDetails;

    public void setSampleType(int sampleType) {
        this.sampleType = sampleType;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }
}
