package com.collabmedhealthcare.vitals.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contact {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("tracking_status")
    @Expose
    private String trackingStatus;
    @SerializedName("trackee")
    @Expose
    private String trackee;

    public String getName() {
        return name;
    }

    public String getMobile() {
        return mobile;
    }

    public String getLocation() {
        return location;
    }

    public String getTrackingStatus() {
        return trackingStatus;
    }

    public String getTrackee() {
        return trackee;
    }
}
