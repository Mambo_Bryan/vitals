package com.collabmedhealthcare.vitals.data.models;

import com.collabmedhealthcare.vitals.R;

public class ProfilePhotoUtils {

    public static int getProfileResourceByName(String name) {

        String[] nameArray = name.toLowerCase().trim().split("");
        int resourceId = AppConstantsUtils.ERROR;

        switch (nameArray[1]) {
            case "a":
                resourceId = R.drawable.a_contact;
                break;

            case "b":
                resourceId = R.drawable.b;
                break;
            case "c":
                resourceId = R.drawable.c;
                break;
            case "d":
                resourceId = R.drawable.d;
                break;
            case "e":
                resourceId = R.drawable.e;
                break;
            case "f":
                resourceId = R.drawable.f;
                break;
            case "g":
                resourceId = R.drawable.g;
                break;
            case "h":
                resourceId = R.drawable.h;
                break;
            case "i":
                resourceId = R.drawable.i;
                break;
            case "j":
                resourceId = R.drawable.j;
                break;
            case "k":
                resourceId = R.drawable.k;
                break;

            case "l":
                resourceId = R.drawable.l;
                break;
            case "m":
                resourceId = R.drawable.m;
                break;
            case "n":
                resourceId = R.drawable.n;
                break;
            case "o":
                resourceId = R.drawable.o;
                break;
            case "p":
                resourceId = R.drawable.p;
                break;
            case "q":
                resourceId = R.drawable.q;
                break;
            case "r":
                resourceId = R.drawable.r;
                break;
            case "s":
                resourceId = R.drawable.s;
                break;
            case "t":
                resourceId = R.drawable.t;
                break;
            case "u":
                resourceId = R.drawable.u;
                break;
            case "v":
                resourceId = R.drawable.v;
                break;
            case "w":
                resourceId = R.drawable.w;
                break;
            case "x":
                resourceId = R.drawable.x;
                break;

            case "y":
                resourceId = R.drawable.y;
                break;

            case "z":
                resourceId = R.drawable.z;
                break;

            default:
                break;
        }

        return resourceId;
    }
}
