package com.collabmedhealthcare.vitals.data.models.response;

import com.collabmedhealthcare.vitals.data.models.ResponseLinks;
import com.collabmedhealthcare.vitals.data.models.ResponseMeta;
import com.collabmedhealthcare.vitals.data.models.ContactData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContactsTracingResponse {

    @SerializedName("data")
    @Expose
    private List<ContactData> data = null;
    @SerializedName("links")
    @Expose
    private ResponseLinks responseLinks;
    @SerializedName("meta")
    @Expose
    private ResponseMeta meta;

    public List<ContactData> getData() {
        return data;
    }

    public ResponseLinks getResponseLinks() {
        return responseLinks;
    }

    public ResponseMeta getMeta() {
        return meta;
    }
}
