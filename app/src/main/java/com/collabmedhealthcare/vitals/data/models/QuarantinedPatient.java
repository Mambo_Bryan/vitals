package com.collabmedhealthcare.vitals.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuarantinedPatient {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("patient_id")
    @Expose
    public Integer patientId;
    @SerializedName("patient_name")
    @Expose
    public String patientName;
    @SerializedName("registered_by")
    @Expose
    public String registeredBy;
    @SerializedName("days_left")
    @Expose
    public Integer daysLeft;
    @SerializedName("no_days")
    @Expose
    public Integer noDays;
    @SerializedName("approved")
    @Expose
    public Integer approved;
    @SerializedName("approved_by")
    @Expose
    public Object approvedBy;
    @SerializedName("start_date")
    @Expose
    public String startDate;
    @SerializedName("approval_status")
    @Expose
    public String approvalStatus;

    public Integer getId() {
        return id;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public String getRegisteredBy() {
        return registeredBy;
    }

    public Integer getDaysLeft() {
        return daysLeft;
    }

    public Integer getNoDays() {
        return noDays;
    }

    public Integer getApproved() {
        return approved;
    }

    public Object getApprovedBy() {
        return approvedBy;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }
}
