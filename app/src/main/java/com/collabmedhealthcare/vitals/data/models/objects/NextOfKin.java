package com.collabmedhealthcare.vitals.data.models.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NextOfKin {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("patient_id")
    @Expose
    private Integer patientId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("middle_name")
    @Expose
    private String middleName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("relationship")
    @Expose
    private String relationship;
    @SerializedName("relationship_id")
    @Expose
    private Integer relationshipId;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("created_at_w3c")
    @Expose
    private String createdAtW3c;

    public Integer getId() {
        return id;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getRelationship() {
        return relationship;
    }

    public Integer getRelationshipId() {
        return relationshipId;
    }

    public String getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getCreatedAtW3c() {
        return createdAtW3c;
    }
}
