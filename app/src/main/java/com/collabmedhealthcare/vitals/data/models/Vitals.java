package com.collabmedhealthcare.vitals.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Vitals {

    @SerializedName("temperature")
    @Expose
    private String temperature;

    @SerializedName("symptoms")
    @Expose
    private List<String> symptoms = null;

    @SerializedName("date")
    @Expose
    private String date;

    public Object getTemperature() {
        return temperature;
    }

    public List<String> getSymptoms() {
        return symptoms;
    }

    public String getDate() {
        return date;
    }
}
