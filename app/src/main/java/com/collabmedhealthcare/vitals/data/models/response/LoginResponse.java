package com.collabmedhealthcare.vitals.data.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("errors")
    @Expose
    private Boolean errors;
    @SerializedName("auth")
    @Expose
    private Boolean auth;
    @SerializedName("user_names")
    @Expose
    private String userNames;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("refresh_token")
    @Expose
    private String refreshToken;

    @SerializedName("0")
    @Expose
    private RegionDetails regionDetails;

    public String getMessage() {
        return message;
    }

    public Boolean getErrors() {
        return errors;
    }

    public Boolean getAuth() {
        return auth;
    }

    public String getUserNames() {
        return userNames;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public RegionDetails getRegionDetails() {
        return regionDetails;
    }
}
