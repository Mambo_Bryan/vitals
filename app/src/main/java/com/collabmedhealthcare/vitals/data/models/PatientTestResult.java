package com.collabmedhealthcare.vitals.data.models;

import com.collabmedhealthcare.vitals.data.models.objects.Procedure;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PatientTestResult {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("patient_name")
    @Expose
    private String patientName;
    @SerializedName("patient_id")
    @Expose
    private Integer patientId;
    @SerializedName("visit_id")
    @Expose
    private Integer visitId;
    @SerializedName("sample")
    @Expose
    private String sample;
    @SerializedName("procedures")
    @Expose
    private List<Procedure> procedures = null;
    @SerializedName("method")
    @Expose
    private Object method;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("queued")
    @Expose
    private Integer queued;
    @SerializedName("queued_by")
    @Expose
    private String queuedBy;

    public Integer getId() {
        return id;
    }

    public String getPatientName() {
        return patientName;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public Integer getVisitId() {
        return visitId;
    }

    public String getSample() {
        return sample;
    }

    public List<Procedure> getProcedures() {
        return procedures;
    }

    public Object getMethod() {
        return method;
    }

    public String getDate() {
        return date;
    }

    public String getResult() {
        return result;
    }

    public String getUser() {
        return user;
    }

    public Integer getQueued() {
        return queued;
    }

    public String getQueuedBy() {
        return queuedBy;
    }
}
