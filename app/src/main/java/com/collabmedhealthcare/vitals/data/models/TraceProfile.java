package com.collabmedhealthcare.vitals.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TraceProfile {
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("middle_name")
    @Expose
    private String middleName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("id_no")
    @Expose
    private String idNumber;
    @SerializedName("dob")
    @Expose
    private String  dob = null;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("age_in")
    @Expose
    private String ageIn = "Years";
    @SerializedName("gender")
    @Expose
    private String gender = null;
    @SerializedName("town")
    @Expose
    private String town;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("secondary_email")
    @Expose
    private String secondaryEmail;
    @SerializedName("telephone")
    @Expose
    private String telephone;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setIdNo(String idNumber) {
        this.idNumber = idNumber;
    }

    public void setDob(String  dob) {
        this.dob = dob;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setAgeIn(String ageIn) {
        this.ageIn = ageIn;
    }

    public void setGender(String  gender) {
        this.gender = gender;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSecondaryEmail(String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
