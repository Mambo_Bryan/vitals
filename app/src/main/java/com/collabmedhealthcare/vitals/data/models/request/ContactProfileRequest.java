package com.collabmedhealthcare.vitals.data.models.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactProfileRequest extends PatientProfileRequest {

    @SerializedName("patient_id")
    @Expose
    private int patientID;

    public void setPatientID(Integer patientID) {
        this.patientID = patientID;
    }
}
