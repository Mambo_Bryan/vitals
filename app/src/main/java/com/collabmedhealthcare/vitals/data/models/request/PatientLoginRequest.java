package com.collabmedhealthcare.vitals.data.models.request;

import com.google.gson.annotations.SerializedName;

public class PatientLoginRequest {

    @SerializedName("moile")
    private String mobile;

    @SerializedName("mobile_id")
    private String patientId;

    public PatientLoginRequest(String mobile, String patientId) {
        this.mobile = mobile;
        this.patientId = patientId;
    }
}
