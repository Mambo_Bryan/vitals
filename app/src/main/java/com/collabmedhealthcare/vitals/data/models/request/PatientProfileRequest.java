package com.collabmedhealthcare.vitals.data.models.request;

import com.collabmedhealthcare.vitals.data.models.PatientLocationDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PatientProfileRequest {

    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("middle_name")
    @Expose
    private String middleName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("id_no")
    @Expose
    private String idNo;
    @SerializedName("dob")
    @Expose
    private String dob = "";
    @SerializedName("mobile")
    @Expose
    private String mobile;

    @SerializedName("home_location")
    @Expose
    private PatientLocationDetails home;

    @SerializedName("work_location")
    @Expose
    private PatientLocationDetails work;

    @SerializedName("sex")
    @Expose
    private String sex;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setHome(PatientLocationDetails home) {
        this.home = home;
    }

    public void setWork(PatientLocationDetails work) {
        this.work = work;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
