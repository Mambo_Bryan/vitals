package com.collabmedhealthcare.vitals.data.models.response;

import com.google.gson.annotations.SerializedName;

public class PatientLoginResponse {

    @SerializedName("auth")
    private boolean confirm;

    public boolean isConfirm() {
        return confirm;
    }
}
