package com.collabmedhealthcare.vitals.data.interfaces;

/**
 * Method to load more in the list
 */
public interface OnLoadMoreListener {

    void onLoadMore();

}
