package com.collabmedhealthcare.vitals.data.models.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Procedure {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("has_results")
    @Expose
    public String hasResults;

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public String getHasResults() {
        return hasResults;
    }
}
