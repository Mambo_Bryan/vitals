package com.collabmedhealthcare.vitals.utils;

import com.collabmedhealthcare.vitals.data.models.NextOfKinRelationship;

import java.util.ArrayList;

public class RelationshipsUtils {

    private static ArrayList<NextOfKinRelationship> relationships;

    private RelationshipsUtils() {
    }

    public static RelationshipsUtils getInstance() {

        RelationshipsUtils relationshipsUtils = new RelationshipsUtils();

        relationships = new ArrayList<>();

        relationships.add(new NextOfKinRelationship(234, "Father"));
        relationships.add(new NextOfKinRelationship(235, "Mother"));
        relationships.add(new NextOfKinRelationship(236, "Guardian"));
        relationships.add(new NextOfKinRelationship(237, "Brother"));
        relationships.add(new NextOfKinRelationship(238, "Sister"));
        relationships.add(new NextOfKinRelationship(239, "Cousin"));
        relationships.add(new NextOfKinRelationship(240, "Grandmother"));
        relationships.add(new NextOfKinRelationship(241, "Grandfather"));
        relationships.add(new NextOfKinRelationship(242, "Friend"));
        relationships.add(new NextOfKinRelationship(243, "Husband"));
        relationships.add(new NextOfKinRelationship(244, "Wife"));
        relationships.add(new NextOfKinRelationship(245, "Self"));
        relationships.add(new NextOfKinRelationship(246, "Uncle"));
        relationships.add(new NextOfKinRelationship(247, "Aunt"));
        relationships.add(new NextOfKinRelationship(248, "Son"));
        relationships.add(new NextOfKinRelationship(249, "Daughter"));
        relationships.add(new NextOfKinRelationship(283, "Niece"));
        relationships.add(new NextOfKinRelationship(284, "Nephew"));
        relationships.add(new NextOfKinRelationship(285, "Grandchild"));
        relationships.add(new NextOfKinRelationship(367, "Chief"));
        relationships.add(new NextOfKinRelationship(368, "Assistant Chief"));
        relationships.add(new NextOfKinRelationship(326, "Other"));

        return relationshipsUtils;
    }

    public static ArrayList<NextOfKinRelationship> getRelationships() {

        relationships = new ArrayList<>();

        relationships.add(new NextOfKinRelationship(234, "Father"));
        relationships.add(new NextOfKinRelationship(235, "Mother"));
        relationships.add(new NextOfKinRelationship(236, "Guardian"));
        relationships.add(new NextOfKinRelationship(237, "Brother"));
        relationships.add(new NextOfKinRelationship(238, "Sister"));
        relationships.add(new NextOfKinRelationship(239, "Cousin"));
        relationships.add(new NextOfKinRelationship(240, "Grandmother"));
        relationships.add(new NextOfKinRelationship(241, "Grandfather"));
        relationships.add(new NextOfKinRelationship(242, "Friend"));
        relationships.add(new NextOfKinRelationship(243, "Husband"));
        relationships.add(new NextOfKinRelationship(244, "Wife"));
        relationships.add(new NextOfKinRelationship(245, "Self"));
        relationships.add(new NextOfKinRelationship(246, "Uncle"));
        relationships.add(new NextOfKinRelationship(247, "Aunt"));
        relationships.add(new NextOfKinRelationship(248, "Son"));
        relationships.add(new NextOfKinRelationship(249, "Daughter"));
        relationships.add(new NextOfKinRelationship(283, "Niece"));
        relationships.add(new NextOfKinRelationship(284, "Nephew"));
        relationships.add(new NextOfKinRelationship(285, "Grandchild"));
        relationships.add(new NextOfKinRelationship(367, "Chief"));
        relationships.add(new NextOfKinRelationship(368, "Assistant Chief"));
        relationships.add(new NextOfKinRelationship(326, "Other"));

        return relationships;
    }

    public String getRelationshipName(int relationshipId) {

        for (NextOfKinRelationship relationship : relationships) {
            if (relationship.getId() == relationshipId) {
                return relationship.getName();
            }
        }

        return "Not Related";
    }

}
