package com.collabmedhealthcare.vitals.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;

import com.bumptech.glide.Glide;
import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;

import java.io.File;

import pub.devrel.easypermissions.EasyPermissions;

public class PictureUtils {

    private static PictureUtils pictureUtils;

    private String[] permissions;
    private Context context;
    private Activity activity;

    private PictureUtils(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
        createPermissions();
    }

    public static PictureUtils getInstance(Context context, Activity activity) {
        if (pictureUtils == null) {
            pictureUtils = new PictureUtils(context, activity);
        }

        return pictureUtils;
    }

    private void createPermissions() {
        permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA};
    }

    public static void loadProfileImageDefault(Context context, ImageView imageView) {
        SharedPrefsUtil prefsUtil = SharedPrefsUtil.getInstance(context);
        Glide.with(context)
                .load(getProfileResourceByName("Patient"))
                .centerCrop()
                .into(imageView);
    }

    public static void loadDrawableImageDefault(Context context, int drawableId, ImageView imageView) {
        Glide.with(context).load(drawableId).into(imageView);
    }

    public static void loadUrlImageDefault(Context context, int drawableId, ImageView imageView) {
        Glide.with(context).load(drawableId).into(imageView);
    }

    public static void setImageFromBitmap(Context context, Bitmap drawableId, ImageView imageView) {
        Glide.with(context).load(drawableId).centerCrop().into(imageView);
    }

    public static void loadImageFromFile(Context context, ImageView imageView, String path) {
        File image = new File(path);
        if (image.exists()) {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
            Glide.with(context).load(bitmap).into(imageView);
        }
    }

    public void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {


                if (EasyPermissions.hasPermissions(activity, permissions)) {
                    if (options[item].equals("Take Photo")) {
                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        activity.startActivityForResult(takePicture, AppConstantsUtils.REQUEST_CODE_TAKE_PHOTO);

                    } else if (options[item].equals("Choose from Gallery")) {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        activity.startActivityForResult(pickPhoto, AppConstantsUtils.REQUEST_CODE_CHOOSE_FROM_GALLERY);

                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                } else {
                    EasyPermissions.requestPermissions(activity, "Access for storage",
                            101, permissions);
                }
            }
        });
        builder.show();
    }


    public static int getProfileResourceByName(String name) {

        String[] nameArray = name.toLowerCase().trim().split("");
        int resourceId = AppConstantsUtils.ERROR;

        switch (nameArray[1]) {
            case "a":
                resourceId = R.drawable.a_contact;
                break;

            case "b":
                resourceId = R.drawable.b;
                break;
            case "c":
                resourceId = R.drawable.c;
                break;
            case "d":
                resourceId = R.drawable.d;
                break;
            case "e":
                resourceId = R.drawable.e;
                break;
            case "f":
                resourceId = R.drawable.f;
                break;
            case "g":
                resourceId = R.drawable.g;
                break;
            case "h":
                resourceId = R.drawable.h;
                break;
            case "i":
                resourceId = R.drawable.i;
                break;
            case "j":
                resourceId = R.drawable.j;
                break;
            case "k":
                resourceId = R.drawable.k;
                break;

            case "l":
                resourceId = R.drawable.l;
                break;
            case "m":
                resourceId = R.drawable.m;
                break;
            case "n":
                resourceId = R.drawable.n;
                break;
            case "o":
                resourceId = R.drawable.o;
                break;
            case "p":
                resourceId = R.drawable.p;
                break;
            case "q":
                resourceId = R.drawable.q;
                break;
            case "r":
                resourceId = R.drawable.r;
                break;
            case "s":
                resourceId = R.drawable.s;
                break;
            case "t":
                resourceId = R.drawable.t;
                break;
            case "u":
                resourceId = R.drawable.u;
                break;
            case "v":
                resourceId = R.drawable.v;
                break;
            case "w":
                resourceId = R.drawable.w;
                break;
            case "x":
                resourceId = R.drawable.x;
                break;

            case "y":
                resourceId = R.drawable.y;
                break;

            case "z":
                resourceId = R.drawable.z;
                break;

            default:
                break;
        }

        return resourceId;
    }


}
