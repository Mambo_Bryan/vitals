package com.collabmedhealthcare.vitals.utils;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.service.NotificationReceiver;
import com.collabmedhealthcare.vitals.ui.views.activities.MainActivity;


public class NotificationUtils {

    private NotificationManagerCompat notificationManagerCompat;
    private Context context;

    public NotificationUtils(Context context) {
        notificationManagerCompat = NotificationManagerCompat.from(context);
        this.context = context;
    }

    public void makeImportantNotificationWithoutFunction(String title, String message) {
        Notification notification = new NotificationCompat.Builder(context, AppConstantsUtils.NOTIFICATION_CHANNEL_1_NAME)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationManagerCompat.IMPORTANCE_LOW)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setColor(Color.BLUE)
                .build();

        notificationManagerCompat.notify(AppConstantsUtils.NOTIFICATION_CHANNEL_1_ID, notification);
    }

    public void makeImportantNotificationWithFunction(String title, String message) {

        Intent activityIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, activityIntent, 0);

        Notification notification = new NotificationCompat.Builder(context, AppConstantsUtils.NOTIFICATION_CHANNEL_1_NAME)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationManagerCompat.IMPORTANCE_LOW)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(contentIntent)
                .build();

        notificationManagerCompat.notify(AppConstantsUtils.NOTIFICATION_CHANNEL_1_ID, notification);
    }

    public void makeImportantNotificationWithFunctionAndButton(String title, String message) {

        Intent broadCastIntent = new Intent(context, NotificationReceiver.class);
        PendingIntent contentIntent = PendingIntent.getBroadcast(context,
                0, broadCastIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(context, AppConstantsUtils.NOTIFICATION_CHANNEL_1_NAME)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationManagerCompat.IMPORTANCE_LOW)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setAutoCancel(true)
                .setContentIntent(contentIntent)
                .addAction(R.mipmap.ic_launcher, "approve", contentIntent)
                .build();

        notificationManagerCompat.notify(AppConstantsUtils.NOTIFICATION_CHANNEL_1_ID, notification);
    }
}
