package com.collabmedhealthcare.vitals.utils;

import com.collabmedhealthcare.vitals.data.models.objects.SampleType;

import java.util.ArrayList;

public class SampleTypeUtils {

    public static ArrayList<SampleType> getSampleTypes() {

        ArrayList<SampleType> sampleTypes = new ArrayList<>();

        sampleTypes.add(new SampleType(57, "Nasopharyngeal Swab"));
        sampleTypes.add(new SampleType(69, "Oropharyngeal Swab"));

        return sampleTypes;
    }

}
