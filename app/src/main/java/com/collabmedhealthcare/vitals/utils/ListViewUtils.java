package com.collabmedhealthcare.vitals.utils;

import android.view.View;
import android.widget.TextView;

import com.collabmedhealthcare.vitals.R;

public class ListViewUtils {

    private View loading;
    private View noData;
    private View data;
    private View error;
    private View.OnClickListener mlistener;
    private TextView noDataMessage;

    public ListViewUtils(View loading, View noData, View data, View error, View.OnClickListener mlistener) {
        this.loading = loading;
        this.noData = noData;
        this.data = data;
        this.error = error;
        this.mlistener = mlistener;

        initViews();

        showLoading();
    }

    private void initViews() {

        noDataMessage = noData.findViewById(R.id.tv_template_no_data_message);
        error.findViewById(R.id.btn_template_retry).setOnClickListener(mlistener);

    }

    public void setNoDataMessage(String message) {
        this.noDataMessage.setText(message);
    }

    public void showLoading() {
        data.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        noData.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
    }


    public void showError() {
        data.setVisibility(View.GONE);
        noData.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);
        error.setVisibility(View.VISIBLE);
    }

    public void showNoData() {
        error.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);
        data.setVisibility(View.GONE);
        noData.setVisibility(View.VISIBLE);

    }

    public void showContent() {
        error.setVisibility(View.GONE);
        noData.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);
        data.setVisibility(View.VISIBLE);

    }

}
