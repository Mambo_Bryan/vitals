package com.collabmedhealthcare.vitals.utils;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class CustomViewsUtils {

    private RecyclerView recyclerView;
    private View loading;
    private View noData;

    public CustomViewsUtils(RecyclerView recyclerView, View loading, View noData) {
        this.recyclerView = recyclerView;
        this.loading = loading;
        this.noData = noData;
    }

    public void showLoading() {
        recyclerView.setVisibility(View.GONE);
        noData.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
    }


    public void showError() {
        loading.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        noData.setVisibility(View.VISIBLE);
    }

    public void showContent() {
        noData.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

    }


}
