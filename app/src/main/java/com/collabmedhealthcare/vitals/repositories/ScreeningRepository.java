package com.collabmedhealthcare.vitals.repositories;

import androidx.lifecycle.MutableLiveData;

import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.response.ScreenedPatientsResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScreeningRepository {

    private static ScreeningRepository repository;
    private ApiService apiService;
    private String authToken;

    private MutableLiveData<ScreenedPatientsResponse> screenedPatientsResponseMutableLiveData = new MutableLiveData<>();

    private ScreeningRepository(String authToken) {
        apiService = ApiClient.createService(ApiService.class);
        this.authToken = authToken;
    }

    public static ScreeningRepository getInstance(String token) {
        if (repository == null) {
            repository = new ScreeningRepository(token);
        }
        return repository;
    }


    public MutableLiveData<ScreenedPatientsResponse> getScreenedPatients(int pageNumber) {

        Call<ScreenedPatientsResponse> call = apiService.getScreenedPatients(authToken, pageNumber, true);
        call.enqueue(new Callback<ScreenedPatientsResponse>() {
            @Override
            public void onResponse(Call<ScreenedPatientsResponse> call, Response<ScreenedPatientsResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    screenedPatientsResponseMutableLiveData.setValue(response.body());
                    return;
                }

                screenedPatientsResponseMutableLiveData.setValue(null);

            }

            @Override
            public void onFailure(Call<ScreenedPatientsResponse> call, Throwable t) {
                screenedPatientsResponseMutableLiveData.setValue(null);
            }
        });

        return screenedPatientsResponseMutableLiveData;
    }

    public MutableLiveData<ScreenedPatientsResponse> getSearchedScreenedPatients(String query, int pageNumber) {

        Call<ScreenedPatientsResponse> call = apiService.getSearchedScreenedPatient(authToken, query, true, pageNumber);
        call.enqueue(new Callback<ScreenedPatientsResponse>() {
            @Override
            public void onResponse(Call<ScreenedPatientsResponse> call, Response<ScreenedPatientsResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    screenedPatientsResponseMutableLiveData.setValue(response.body());
                    return;
                }

                screenedPatientsResponseMutableLiveData.setValue(null);

            }

            @Override
            public void onFailure(Call<ScreenedPatientsResponse> call, Throwable t) {
                screenedPatientsResponseMutableLiveData.setValue(null);
            }
        });

        return screenedPatientsResponseMutableLiveData;
    }
}
