package com.collabmedhealthcare.vitals.repositories;

import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.request.RequestSampleBody;
import com.collabmedhealthcare.vitals.data.models.response.DefaultMessageResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SampleRepository {

    private static SampleRepository repository;
    private ApiService apiService;
    private String authToken;

    private DefaultMessageResponse defaultResponse;

    private SampleRepository(String authToken) {
        apiService = ApiClient.createService(ApiService.class);
        this.authToken = authToken;
    }

    public static SampleRepository getInstance(String token) {
        if (repository == null) {
            repository = new SampleRepository(token);
        }
        return repository;
    }

    public DefaultMessageResponse requestSampleCollection(int patientId, RequestSampleBody body) {

        Call<DefaultMessageResponse> call = apiService.requestSampleCollection(
                authToken,
                patientId,
                body
        );

        call.enqueue(new Callback<DefaultMessageResponse>() {
            @Override
            public void onResponse(Call<DefaultMessageResponse> call, Response<DefaultMessageResponse> response) {

                if (response.isSuccessful() && response.body() != null) {
                    defaultResponse = response.body();
                    return;
                }

                defaultResponse = null;

            }

            @Override
            public void onFailure(Call<DefaultMessageResponse> call, Throwable t) {
                defaultResponse = null;
            }
        });

        return defaultResponse;
    }
}
