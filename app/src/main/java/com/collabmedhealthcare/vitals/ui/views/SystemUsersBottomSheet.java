package com.collabmedhealthcare.vitals.ui.views;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.interfaces.RecyclerItemListener;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.objects.SystemUser;
import com.collabmedhealthcare.vitals.data.models.response.UsersResponse;
import com.collabmedhealthcare.vitals.ui.adapter.UserRecyclerAdapter;
import com.collabmedhealthcare.vitals.utils.ListViewUtils;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.textfield.TextInputLayout;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SystemUsersBottomSheet extends BottomSheetDialogFragment implements View.OnClickListener, RecyclerItemListener {

    private AddTrackersListener mListener;

    private ArrayList<SystemUser> users;
    private ArrayList<SystemUser> selectedUsers;

    private UsersResponse usersResponse;

    private XRecyclerView recyclerView;
    private View rootView;
    private ListViewUtils mainViews;
    private UserRecyclerAdapter adapter;

    private String searchQuery;
    private int pageNumber;
    private int totalPageNumber;
    private boolean isUserSearching;

    private String authToken;
    private ApiService apiService;
    private TextView tvNoTrackers;
    private ChipGroup usersChips;

    public SystemUsersBottomSheet(UsersResponse response, AddTrackersListener mListener) {

        SharedPrefsUtil prefsUtil = SharedPrefsUtil.getInstance(getContext());
        authToken = prefsUtil.getUserAccessToken();
        apiService = ApiClient.createService(ApiService.class);

        users = new ArrayList<>();
        selectedUsers = new ArrayList<>();

        if (response == null) {
            getTrackers();
        } else {
            this.usersResponse = response;
            users.addAll(response.getSystemUsers());
            totalPageNumber = response.getMeta().getTotal();
        }

        this.mListener = mListener;

        isUserSearching = false;
        pageNumber = 1;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.layout_bottom_sheet_users, container, false);

        tvNoTrackers = rootView.findViewById(R.id.tv_bottomSheet_users_no_trackers);
        usersChips = rootView.findViewById(R.id.chip_group_users);

        checkSelectedUsers();

        TextInputLayout textInputLayout = rootView.findViewById(R.id.textInputLayout_bottomSheet_users);
        textInputLayout.addOnEndIconChangedListener(new TextInputLayout.OnEndIconChangedListener() {
            @Override
            public void onEndIconChanged(@NonNull TextInputLayout textInputLayout, int previousIcon) {

            }
        });

        EditText edtSearchedUser = rootView.findViewById(R.id.edt_itemBottomSheet_tracker);
        edtSearchedUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtSearchedUser.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchQuery = edtSearchedUser.getText().toString();
                    isUserSearching = true;
                    getSearchedTrackers();
                    mainViews.showLoading();
                    return true;
                }
                return false;
            }
        });

        textInputLayout.setEndIconOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchQuery = edtSearchedUser.getText().toString();
                isUserSearching = true;
                getSearchedTrackers();
                mainViews.showLoading();
            }
        });

        edtSearchedUser.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchQuery = edtSearchedUser.getText().toString();
                    getSearchedTrackers();
                    mainViews.showLoading();
                    return true;
                }
                return false;
            }
        });


        rootView.findViewById(R.id.cardView_trackerBottomSheet_select).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!selectedUsers.isEmpty()) {
                    mListener.onTrackersSelected(selectedUsers);
                    dismiss();
                } else {
                    Toast.makeText(getContext(), "select trackers", Toast.LENGTH_SHORT).show();
                }
            }
        });

        initRecyclerCompleteViews();

        if (usersResponse != null) {
            initRecycler();
            mainViews.showContent();
        }

        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog1) {

                BottomSheetDialog d = (BottomSheetDialog) dialog1;
                setupFullHeight(d);

            }
        });

        // Do something with your dialog like setContentView() or whatever
        return dialog;
    }

    private void initRecyclerCompleteViews() {
        View completeItemViews = rootView.findViewById(R.id.include_bottomSheet_users);

        View loading = completeItemViews.findViewById(R.id.include_template_loading);
        View noData = completeItemViews.findViewById(R.id.include_template_no_data);
        View error = completeItemViews.findViewById(R.id.include_template_error);
        View data = completeItemViews.findViewById(R.id.include_template_data);

        mainViews = new ListViewUtils(
                loading,
                noData,
                data,
                error,
                this
        );

        mainViews.setNoDataMessage("No user found");
        recyclerView = data.findViewById(R.id.rv_template_data);

    }

    private void initRecycler() {

        selectedUsers = new ArrayList<>();

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new UserRecyclerAdapter(users, this);

        recyclerView.setAdapter(adapter);

        recyclerView.setPullRefreshEnabled(true);
        recyclerView.setLoadingMoreEnabled(true);

        recyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                pageNumber = 1;
                getTrackers();
            }

            @Override
            public void onLoadMore() {
                pageNumber++;

                if (isUserSearching) {
                    getSearchedTrackers();
                } else {
                    getTrackers();
                }
            }
        });

    }

    private void updateRecycler() {

        if (usersResponse != null && usersResponse.getSystemUsers() != null) {

            users.addAll(usersResponse.getSystemUsers());
            adapter.notifyDataSetChanged();

        }

        if (pageNumber >= totalPageNumber || isUserSearching)
            recyclerView.setLoadingMoreEnabled(false);
    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = getWindowHeight();
        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        mListener.onTrackersSelected(null);
        super.onDismiss(dialog);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            mListener = (AddTrackersListener) context;
        } catch (ClassCastException exception) {
            throw new ClassCastException(context.toString() +
                    "must implement bottom sheet listener");
        }
    }

    private int getWindowHeight() {
        // Calculate window height for fullscreen use
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_template_retry) {
            getTrackers();
            mainViews.showLoading();
        }

    }

    @Override
    public void onItemSelected(int position) {

        SystemUser user = users.get(position - 1);

        if (selectedUsers.contains(user)) {
            Toast.makeText(getContext(), "User already added", Toast.LENGTH_SHORT).show();
            return;
        }

        selectedUsers.add(user);
        addUserChip(user);

    }

    private void checkSelectedUsers() {

        if (selectedUsers.isEmpty()) {
            tvNoTrackers.setVisibility(View.VISIBLE);
        } else {
            tvNoTrackers.setVisibility(View.GONE);
        }

    }

    private void addUserChip(SystemUser user) {

        usersChips.setVisibility(View.VISIBLE);

        Chip userChip = new Chip(usersChips.getContext());
        userChip.setId(selectedUsers.indexOf(user));
        String nextOfKinName = user.getFullName();
        userChip.setText(nextOfKinName);
        userChip.setCloseIconVisible(true);
        userChip.setOnCloseIconClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usersChips.removeView(v);
                selectedUsers.remove(v.getId());

                checkSelectedUsers();
            }
        });

        usersChips.addView(userChip);
        checkSelectedUsers();

    }

    public interface AddTrackersListener {
        void onTrackersSelected(ArrayList<SystemUser> selectedTrackers);
    }

    private void getTrackers() {

        Call<UsersResponse> call = apiService.getUsers(authToken, true, pageNumber);
        call.enqueue(new Callback<UsersResponse>() {
            @Override
            public void onResponse(Call<UsersResponse> call, Response<UsersResponse> response) {

                if (response.isSuccessful() && response.body() != null) {
                    usersResponse = response.body();

                    if (searchQuery == null) {
                        totalPageNumber = usersResponse.getMeta().getLastPage();
                    }

                    if (pageNumber == 1) {
                        initRecycler();
                        recyclerView.refreshComplete();
                        mainViews.showContent();
                    } else {
                        updateRecycler();
                    }
                    return;
                }

                mainViews.showError();
                Toast.makeText(getContext(), "failed to get results", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<UsersResponse> call, Throwable t) {

                mainViews.showError();
                Toast.makeText(getContext(), "failed to connect", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void getSearchedTrackers() {

        Call<UsersResponse> call = apiService.getSearchedUsers(authToken, true, searchQuery, pageNumber);
        call.enqueue(new Callback<UsersResponse>() {
            @Override
            public void onResponse(Call<UsersResponse> call, Response<UsersResponse> response) {

                if (response.isSuccessful() && response.body() != null) {
                    usersResponse = response.body();

                    if (pageNumber == 1) {
                        initRecycler();
                        mainViews.showContent();
                    } else {
                        updateRecycler();
                    }
                    return;
                }

                mainViews.showError();
                Toast.makeText(getContext(), "failed to get results", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<UsersResponse> call, Throwable t) {
                mainViews.showError();
                Toast.makeText(getContext(), "failed to connect", Toast.LENGTH_SHORT).show();

            }
        });


    }

}
