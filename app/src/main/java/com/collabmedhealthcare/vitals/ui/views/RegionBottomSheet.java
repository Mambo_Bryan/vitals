package com.collabmedhealthcare.vitals.ui.views;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.interfaces.RecyclerBooleanItemListener;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.UserRegion;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;

public class RegionBottomSheet extends BottomSheetDialogFragment implements RadioGroup.OnCheckedChangeListener, RecyclerBooleanItemListener {

    private AddRegionSelectionListener mListener;

    private ArrayList<UserRegion> regions;

    private RadioGroup radioGroup;

    private UserRegion userRegion;

    private SharedPrefsUtil prefsUtil;

    public RegionBottomSheet(ArrayList<UserRegion> regions) {
        this.regions = regions;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);

        prefsUtil = SharedPrefsUtil.getInstance(getContext());

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.layout_bottom_sheet_region, container, false);

        radioGroup = rootView.findViewById(R.id.radioGroup_region_bottomSheet);
        radioGroup.setOnCheckedChangeListener(this);

        for (UserRegion region : regions) {

            RadioButton button = new RadioButton(radioGroup.getContext());

            button.setText(region.getRegionName());
            button.setId(region.getRegionId());

            radioGroup.addView(button);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) button.getLayoutParams();
            params.setMargins(0, 4, 0, 4); //substitute parameters for left, top, right, bottom
            button.setLayoutParams(params);

        }

        rootView.findViewById(R.id.cardView_regionBottom_select).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userRegion != null) {
                    prefsUtil.storeUserRegion(userRegion);
                    mListener.onRegionSelected(true);
                } else {
                    Toast.makeText(getContext(), "select region", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog1) {
                BottomSheetDialog d = (BottomSheetDialog) dialog1;

                setupFullHeight(d);
            }
        });

        // Do something with your dialog like setContentView() or whatever
        return dialog;
    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = getWindowHeight();
        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        mListener.onRegionSelected(false);
        super.onDismiss(dialog);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            mListener = (AddRegionSelectionListener) context;
        } catch (ClassCastException exception) {
            throw new ClassCastException(context.toString() +
                    "must implement bottom sheet listener");
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        for (UserRegion region : regions) {

            if (region.getRegionId() == checkedId) {
                userRegion = region;
            }

        }

    }

    private int getWindowHeight() {
        // Calculate window height for fullscreen use
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    @Override
    public void onBooleanItemSelected(boolean isSelected, int position) {
        if (isSelected) {
            userRegion = regions.get(position);
            Toast.makeText(getContext(), userRegion.getRegionName(), Toast.LENGTH_SHORT).show();
        } else {
            userRegion = null;
            Toast.makeText(getContext(), "No region selected", Toast.LENGTH_SHORT).show();
        }
    }

    public interface AddRegionSelectionListener {
        void onRegionSelected(boolean isSelected);
    }


}
