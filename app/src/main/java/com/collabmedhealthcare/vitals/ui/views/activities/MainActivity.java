package com.collabmedhealthcare.vitals.ui.views.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.response.DetailsResponse;
import com.collabmedhealthcare.vitals.ui.views.LoadingDialog;
import com.collabmedhealthcare.vitals.utils.StatusBarUtils;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private SharedPrefsUtil prefsUtil;
    private Class<?> mClss;
    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    private void initViews() {

        prefsUtil = SharedPrefsUtil.getInstance(this);

        StatusBarUtils.setUpStatusBar(this, R.color.white2);

        Toolbar toolbar = findViewById(R.id.toolbar_mainActivity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView tvGreeting = findViewById(R.id.textView_mainActivity_greeting);
        String username = prefsUtil.getUsername();
        String greeting;
        if (username != null) {
            greeting = "Hi " + username + " !";
        } else {
            greeting = "Welcome!";
        }
        tvGreeting.setText(greeting);


        findViewById(R.id.cardView_mainActivity_screen).setOnClickListener(this);
        findViewById(R.id.cardView_mainActivity_sample).setOnClickListener(this);
        findViewById(R.id.cardView_mainActivity_trace).setOnClickListener(this);
        findViewById(R.id.cardView_mainActivity_digitalID).setOnClickListener(this);
        findViewById(R.id.cardView_mainActivity_quarantine).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.cardView_mainActivity_screen:
                Intent screenIntent = new Intent(this, ScreenedPatientsListActivity.class);
                startActivity(screenIntent);
                break;

            case R.id.cardView_mainActivity_sample:
                Intent sampleIntent = new Intent(this, SampleCollectionListActivity.class);
                startActivity(sampleIntent);
                break;

            case R.id.cardView_mainActivity_trace:
                Intent contactIntent = new Intent(this, LevelOneListActivity.class);
                startActivity(contactIntent);
                break;

            case R.id.cardView_mainActivity_digitalID:
                checkCameraPermission(ScanBarcodeActivity.class);
                break;

            case R.id.cardView_mainActivity_quarantine:
                Intent quarantineIntent = new Intent(this, QuarantinePatientsActivity.class);
                startActivity(quarantineIntent);
                break;

            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_logout, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_item_logout) {
            prefsUtil.clearAllPreferencesData();
            Intent myIntent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(myIntent);
            finish();
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == AppConstantsUtils.ZXING_CAMERA_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (mClss != null) {
                    Intent intent = new Intent(this, mClss);
                    startActivity(intent);
                }
            } else {
                Toast.makeText(this, "Please grant camera permission to use the QR Scanner", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case AppConstantsUtils.REQUEST_CODE_BARCODE_SCAN:
                    getScannedData(data);
                    break;

                default:
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void checkCameraPermission(Class<?> clss) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            mClss = clss;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, AppConstantsUtils.ZXING_CAMERA_PERMISSION);
        } else {
            Intent intent = new Intent(this, clss);
            startActivityForResult(intent, AppConstantsUtils.REQUEST_CODE_BARCODE_SCAN);
        }
    }

    private void getScannedData(Intent data) {

        loadingDialog = new LoadingDialog(this);
        loadingDialog.showDialog();

        String scannedCode = data.getStringExtra(AppConstantsUtils.SCAN_BARCODE_EXTRA);

        getPatientData(scannedCode);

    }

    private void getPatientData(String scannedCode) {

        ApiService apiService = ApiClient.createService(ApiService.class);
        SharedPrefsUtil prefsUtil = SharedPrefsUtil.getInstance(this);

        Call<DetailsResponse> call = apiService.getPatientDetails(prefsUtil.getUserAccessToken(), scannedCode);
        call.enqueue(new Callback<DetailsResponse>() {
            @Override
            public void onResponse(Call<DetailsResponse> call, Response<DetailsResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    ScreenedPatient patient = response.body().getPatient();

                    Gson gson = new Gson();
                    String clickedPatient = gson.toJson(patient);

                    Intent myIntent = new Intent(MainActivity.this, PatientActivity.class);
                    myIntent.putExtra(AppConstantsUtils.SCREENED_PATIENT_ACTIVITY_EXTRA, clickedPatient);
                    startActivity(myIntent);
                    loadingDialog.hideDialog();
                    return;

                }

                Toast.makeText(MainActivity.this, "cannot find patient", Toast.LENGTH_SHORT).show();
                loadingDialog.hideDialog();

            }

            @Override
            public void onFailure(Call<DetailsResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, "unable to connect", Toast.LENGTH_SHORT).show();
                loadingDialog.hideDialog();
            }
        });

    }
}
