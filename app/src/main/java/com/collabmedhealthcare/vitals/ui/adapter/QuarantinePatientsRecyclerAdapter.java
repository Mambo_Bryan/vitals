package com.collabmedhealthcare.vitals.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.QuarantinedPatient;

import java.util.ArrayList;
import java.util.List;

public class QuarantinePatientsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private ArrayList<QuarantinedPatient> allPatients;
    private ArrayList<QuarantinedPatient> filteredPatients;
    private QuarantinePatientListener mListener;

    public QuarantinePatientsRecyclerAdapter(ArrayList<QuarantinedPatient> patientsList,
                                             QuarantinePatientListener mListener) {
        this.allPatients = patientsList;
        this.filteredPatients = new ArrayList<>(patientsList);
        this.mListener = mListener;
    }

    @Override
    public Filter getFilter() {
        return contactsFilter;
    }

    private Filter contactsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String queryString = constraint.toString().toLowerCase();

            if (queryString.isEmpty()) {
                filteredPatients = allPatients;
            } else {

                List<QuarantinedPatient> filteredList = new ArrayList<>();

                for (QuarantinedPatient patient : allPatients) {
                    if (patient.getPatientName() != null) {
                        if (patient.getPatientName().toLowerCase().contains(queryString.trim())) {
                            filteredList.add(patient);
                        }
                    }
                }

                filteredPatients = new ArrayList<>(filteredList);
            }

            FilterResults results = new FilterResults();
            results.values = filteredPatients;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredPatients = ((ArrayList<QuarantinedPatient>) results.values);
            notifyDataSetChanged();
        }
    };

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View dataView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_quarantine_patient, parent, false);
        QuarantinePatientViewHolder dataViewHolder = new QuarantinePatientViewHolder(dataView, mListener);

        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        QuarantinedPatient patient = filteredPatients.get(position);

        QuarantinePatientViewHolder viewHolder = (QuarantinePatientViewHolder) holder;

        viewHolder.patientName.setText(patient.getPatientName());
        viewHolder.patientStartDate.setText(patient.getStartDate());
        viewHolder.patientDaysLeft.setText(String.valueOf(patient.getDaysLeft()));
        viewHolder.patientStatus.setText(patient.getApprovalStatus());

    }

    @Override
    public int getItemCount() {
        return filteredPatients.size();
    }

    public class QuarantinePatientViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView patientName;
        TextView patientStartDate;
        TextView patientDaysLeft;
        TextView patientStatus;

        QuarantinePatientListener mListener;

        public QuarantinePatientViewHolder(@NonNull View itemView, QuarantinePatientListener mListener) {
            super(itemView);

            patientName = itemView.findViewById(R.id.tv_itemQuarantine_name);
            patientStartDate = itemView.findViewById(R.id.tv_itemQuarantine_start_date);
            patientStatus = itemView.findViewById(R.id.tv_itemQuarantine_status);
            patientDaysLeft = itemView.findViewById(R.id.tv_itemQuarantine_days_left);

            this.mListener = mListener;
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            mListener.onQuarantinePatientClicked(getAdapterPosition());
        }
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    public interface QuarantinePatientListener {
        void onQuarantinePatientClicked(int position);
    }
}
