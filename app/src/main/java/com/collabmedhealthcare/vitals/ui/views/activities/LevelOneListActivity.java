package com.collabmedhealthcare.vitals.ui.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.ContactData;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.response.ContactsTracingResponse;
import com.collabmedhealthcare.vitals.ui.adapter.ContactTracingRecyclerAdapter;
import com.collabmedhealthcare.vitals.utils.ListViewUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LevelOneListActivity extends AppCompatActivity implements
        ContactTracingRecyclerAdapter.OnContactTracingListener, SearchView.OnQueryTextListener,
        View.OnClickListener {


    //variables
    ArrayList<ContactData> dataArrayList = new ArrayList<>();
    ContactTracingRecyclerAdapter adapter;

    //
    ApiService apiService;
    String token;

    //layout items
    private ListViewUtils contactTracingViews;
    private XRecyclerView recyclerView;
    private SearchView searchView;
    private String searchQuery;

    private int pageNumber;
    private int totalPageNumber;
    private boolean isUserSearching = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template_recycler);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Contact Tracing");

        initViews();

        pageNumber = 1;

        contactTracingViews.showLoading();

        getContactTracingList();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        pageNumber = 1;
        getContactTracingList();
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {

            pageNumber = 1;
            isUserSearching = false;
            searchQuery = null;

            searchView.setIconified(true);
            contactTracingViews.showLoading();
            getContactTracingList();

        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem mSearchMenuItem = menu.findItem(R.id.menu_item_search);
        searchView = (SearchView) mSearchMenuItem.getActionView();
        searchView.setQueryHint("Search Patient");
        searchView.setOnQueryTextListener(this);
        return true;
    }

    private void getContactTracingList() {
        Call<ContactsTracingResponse> call = apiService.getGeneralContactTracingList(token, pageNumber);
        call.enqueue(new Callback<ContactsTracingResponse>() {
            @Override
            public void onResponse(Call<ContactsTracingResponse> call, Response<ContactsTracingResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    totalPageNumber = response.body().getMeta().getLastPage();

                    if (pageNumber == 1) {

                        if (!response.body().getData().isEmpty()) {

                            initRecycler(response.body());
                            contactTracingViews.showContent();
                            recyclerView.refreshComplete();

                        } else {
                            contactTracingViews.showNoData();
                        }

                    } else {
                        updateRecycler(response.body());

                    }

                    return;
                }

                contactTracingViews.showError();
            }

            @Override
            public void onFailure(Call<ContactsTracingResponse> call, Throwable t) {
                contactTracingViews.showError();
                Toast.makeText(LevelOneListActivity.this, "failed to connect", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getSearchedContactTracingList() {
        Call<ContactsTracingResponse> call = apiService.getSearchedGeneralContactTracingList(token, searchQuery, pageNumber);
        call.enqueue(new Callback<ContactsTracingResponse>() {
            @Override
            public void onResponse(Call<ContactsTracingResponse> call, Response<ContactsTracingResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    totalPageNumber = response.body().getMeta().getLastPage();

                    if (pageNumber == 1) {

                        if (!response.body().getData().isEmpty()) {

                            initRecycler(response.body());
                            contactTracingViews.showContent();
                            recyclerView.refreshComplete();

                        } else {
                            contactTracingViews.showNoData();
                        }

                    } else {
                        updateRecycler(response.body());

                    }

                    return;
                }

                contactTracingViews.showError();
                Toast.makeText(LevelOneListActivity.this, "error getting results", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<ContactsTracingResponse> call, Throwable t) {
                contactTracingViews.showError();
                Toast.makeText(LevelOneListActivity.this, "failed to connect", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initRecycler(ContactsTracingResponse body) {
        dataArrayList = new ArrayList<>();

        dataArrayList.addAll(body.getData());

        if (!dataArrayList.isEmpty()) {


            adapter = new ContactTracingRecyclerAdapter(dataArrayList, this);
            LinearLayoutManager layoutManager
                    = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);

            recyclerView.setPullRefreshEnabled(true);
            recyclerView.setLoadingMoreEnabled(true);

            recyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
                @Override
                public void onRefresh() {
                    pageNumber = 1;
                    getContactTracingList();
                }

                @Override
                public void onLoadMore() {
                    pageNumber++;

                    if (isUserSearching) {
                        getSearchedContactTracingList();
                    } else {
                        getContactTracingList();
                    }
                }
            });
        } else {
            contactTracingViews.showNoData();
        }

    }

    private void updateRecycler(ContactsTracingResponse response) {

        if (response != null && response.getData() != null) {

            dataArrayList.addAll(response.getData());
            adapter.notifyDataSetChanged();

        }

        if (pageNumber >= totalPageNumber)
            recyclerView.setLoadingMoreEnabled(false);
    }

    private void initViews() {

        SharedPrefsUtil prefsUtil = SharedPrefsUtil.getInstance(this);

        apiService = ApiClient.createService(ApiService.class);
        token = prefsUtil.getUserAccessToken();

        FloatingActionButton addContactFab = findViewById(R.id.fab_templateRecyler);
        addContactFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addContactIntent = new Intent(LevelOneListActivity.this, AddContactTraceActivity.class);

                addContactIntent.putExtra(AppConstantsUtils.CONTACT_TRACE_STRING_EXTRA,
                        AppConstantsUtils.LEVEL_1_CONTACT_TRACE_INT);

                startActivityForResult(addContactIntent, AppConstantsUtils.LEVEL_1_CONTACT_TRACE_INT);
            }
        });

        initRecyclerCompleteViews();
    }

    private void initRecyclerCompleteViews() {
        View completeItemViews = findViewById(R.id.inlcude_templateRecycler_complete);

        View loading = completeItemViews.findViewById(R.id.include_template_loading);
        View noData = completeItemViews.findViewById(R.id.include_template_no_data);
        View error = completeItemViews.findViewById(R.id.include_template_error);
        View data = completeItemViews.findViewById(R.id.include_template_data);

        contactTracingViews = new ListViewUtils(
                loading,
                noData,
                data,
                error,
                this
        );

        contactTracingViews.setNoDataMessage("There are no tracing requests");
        recyclerView = data.findViewById(R.id.rv_template_data);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstantsUtils.LEVEL_1_CONTACT_TRACE_INT) {
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(this, "updating list", Toast.LENGTH_SHORT).show();
                contactTracingViews.showLoading();
                getContactTracingList();
            }
        }

    }

    @Override
    public void onContactTrackingClicked(int position) {
        Intent myIntent = new Intent(this, GeneralTracingContactActivity.class);
        myIntent.putExtra(AppConstantsUtils.GENERAL_TRACE_CONTACT_EXTRA, dataArrayList.get(position - 1));
        startActivity(myIntent);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        searchQuery = query;

        if (!query.isEmpty()) {
            adapter.getFilter().filter(query);
            contactTracingViews.showLoading();
            getSearchedContactTracingList();
        }

        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (dataArrayList != null && !dataArrayList.isEmpty()) {
            adapter.getFilter().filter(newText);
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_template_retry) {
            getContactTracingList();
            contactTracingViews.showLoading();
        }
    }
}
