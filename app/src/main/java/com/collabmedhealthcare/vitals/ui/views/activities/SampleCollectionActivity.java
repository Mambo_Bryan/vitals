package com.collabmedhealthcare.vitals.ui.views.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.PatientLocationDetails;
import com.collabmedhealthcare.vitals.data.models.SampleRecordRequestBody;
import com.collabmedhealthcare.vitals.data.models.SampleRequest;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.objects.SampleType;
import com.collabmedhealthcare.vitals.data.models.response.CollectionResponse;
import com.collabmedhealthcare.vitals.ui.views.LoadingDialog;
import com.collabmedhealthcare.vitals.utils.SampleTypeUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SampleCollectionActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int PERMISSION_ID = 44;

    TextView tvPatientName;
    AutoCompleteTextView sampleTypeAutoComplete;
    AutoCompleteTextView procedureAutoComplete;

    EditText otherDetailsEditText;
    EditText edtCollectionPoint;

    private String barcodeDefault = "click to scan code";
    private String collectionPointLocation = null;
    private String collectionPointLocationName = null;

    private LoadingDialog dialog;

    private String patientName;
    private int patientId;

    private ApiService apiService;
    private SampleRecordRequestBody collection;
    private FusedLocationProviderClient mFusedLocationClient;
    private PatientLocationDetails collectionLocationDetails;

    private String token;

    private Class<?> mClass;
    private TextView tvBarcode;

    private boolean isBarcodeScanned = false;
    private Button scanBarcode;
    private ArrayList<SampleType> sampleTypes;
    private SampleType selectedSampleType;
    private SampleRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_collection);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Collect Sample");

        SharedPrefsUtil prefsUtil = SharedPrefsUtil.getInstance(this);
        token = prefsUtil.getUserAccessToken();
        apiService = ApiClient.createService(ApiService.class);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        Intent intent = getIntent();

        int activityCode = intent.getIntExtra(AppConstantsUtils.SAMPLE_COLLECTION_ACTIVITY_CODE_EXTRA,
                AppConstantsUtils.ERROR);

        switch (activityCode) {
            case AppConstantsUtils.CODE_ADD_PATIENT_ACTIVITY:

                patientName = intent.getStringExtra(AppConstantsUtils.PATIENT_NAME_STRING);
                patientId = intent.getIntExtra(AppConstantsUtils.PATIENT_ID_STRING, -1);
                break;

            case AppConstantsUtils.CODE_SAMPLE_LIST_ACTIVITY:

                Gson gson = new Gson();

                String data = intent.getStringExtra(AppConstantsUtils.SAMPLE_COLLECTION_ACTIVITY_EXTRA);
                request = gson.fromJson(data, SampleRequest.class);

                patientName = request.getPatient();
                patientId = request.getId();

                break;

            default:
                break;
        }

        initViews();
        updateViews();
        getLastLocation();
    }

    private void updateViews() {
        tvPatientName.setText(patientName);
    }

    private void initViews() {

        dialog = new LoadingDialog(this);

        tvPatientName = findViewById(R.id.tv_sampleCollect_name);

        tvBarcode = findViewById(R.id.tv_sampleCollect_barcode);
        tvBarcode.setText(barcodeDefault);

        procedureAutoComplete = findViewById(R.id.autoComplete_sampleCollect_procedure);
        procedureAutoComplete.setEnabled(false);
        String[] procedureTypeArray = new String[]{"Covid 19"};
        ArrayAdapter<String> procedureAdapter =
                new ArrayAdapter<>(
                        this,
                        android.R.layout.simple_list_item_1,
                        procedureTypeArray);

        procedureAutoComplete.setAdapter(procedureAdapter);
        procedureAutoComplete.setText(procedureTypeArray[0], false);

        sampleTypeAutoComplete = findViewById(R.id.autoComplete_sampleCollect_sample_type);

        sampleTypes = SampleTypeUtils.getSampleTypes();

        String[] sampleTypeArray = new String[sampleTypes.size()];

        for (int i = 0; i < sampleTypes.size(); i++) {

            SampleType sampleType = sampleTypes.get(i);
            sampleTypeArray[i] = sampleType.getSampleName();

        }

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(
                        this,
                        android.R.layout.simple_list_item_1,
                        sampleTypeArray);

        sampleTypeAutoComplete.setAdapter(adapter);

        if (request == null) {
            sampleTypeAutoComplete.setText(sampleTypeArray[0], false);
            selectedSampleType = sampleTypes.get(0);
        } else {

            int code = request.getSampleTypeId();

            for (int i = 0; i < sampleTypes.size(); i++) {

                SampleType sampleType = sampleTypes.get(i);

                if (sampleType.getSampleId() == code) {
                    sampleTypeAutoComplete.setText(sampleTypeArray[i], false);
                    selectedSampleType = sampleTypes.get(i);
                }
            }

        }


        sampleTypeAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedSampleType = sampleTypes.get(position);
            }
        });

        otherDetailsEditText = findViewById(R.id.edt_sampleCollect_other);
        edtCollectionPoint = findViewById(R.id.edt_sampleCollect_collection_point);
        edtCollectionPoint.setEnabled(false);

        scanBarcode = findViewById(R.id.btn_scan_barcode);
        scanBarcode.setOnClickListener(this);
        findViewById(R.id.btn_sampleCollect_save).setOnClickListener(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_scan_barcode:

                if (isBarcodeScanned) {

                    tvBarcode.setText(barcodeDefault);
                    showButtonDefault();
                    isBarcodeScanned = false;

                } else {

                    checkCameraPermission(ScanBarcodeActivity.class);

                }

                break;
            case R.id.btn_sampleCollect_save:

                if (isValidUserInputs()) {
                    dialog.showDialog();
                    createSampleObject();
                    sendSampleToServer();
                } else {
                    Toast.makeText(this, "Invalid collection details", Toast.LENGTH_SHORT).show();
                }

                break;
            default:
                break;
        }
    }

    private void sendSampleToServer() {
        Call<CollectionResponse> call = apiService.recordSampleCollection(token, patientId, collection);
        call.enqueue(new Callback<CollectionResponse>() {
            @Override
            public void onResponse(Call<CollectionResponse> call, Response<CollectionResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Toast.makeText(SampleCollectionActivity.this, "Sample Saved ", Toast.LENGTH_SHORT).show();
                    openMainActivity();
                    return;
                }

                dialog.hideDialog();
                Toast.makeText(SampleCollectionActivity.this, "Error Saving Sample", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<CollectionResponse> call, Throwable t) {
                dialog.hideDialog();
                Toast.makeText(SampleCollectionActivity.this, "Error Connecting", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createSampleObject() {

        collection = new SampleRecordRequestBody();

        collection.setCollectionPointName(collectionPointLocationName);
        collection.setCollectionPoint(collectionLocationDetails);
        collection.setOtherDetails(otherDetailsEditText.getText().toString());

    }

    public boolean isValidUserInputs() {

        boolean isValidInputs = true;

        if (edtCollectionPoint.getText().toString().trim().isEmpty()) {
            edtCollectionPoint.setError("Add Collection point");
            isValidInputs = false;
        }

        return isValidInputs;
    }

    private void openMainActivity() {
        Intent myIntent = new Intent(SampleCollectionActivity.this, SampleCollectionListActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(myIntent);
        finish();
        dialog.hideDialog();

    }

    private void checkCameraPermission(Class<?> clss) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            mClass = clss;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, AppConstantsUtils.ZXING_CAMERA_PERMISSION);
        } else {
            Intent intent = new Intent(this, clss);
            startActivityForResult(intent, AppConstantsUtils.REQUEST_CODE_BARCODE_SCAN);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == AppConstantsUtils.ZXING_CAMERA_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (mClass != null) {
                    Intent intent = new Intent(this, mClass);
                    startActivity(intent);
                }
            } else {
                Toast.makeText(this, "Please grant camera permission to use the QR Scanner", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case AppConstantsUtils.REQUEST_CODE_BARCODE_SCAN:
                    getScannedData(data, tvBarcode);
                    break;

                case AppConstantsUtils.REQUEST_CODE_MAPS_LOCATION_COLLECTION_POINT:
                    getLocationData(data, edtCollectionPoint);
                    break;

                default:
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private boolean checkPermissions() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {

                                    collectionLocationDetails = new PatientLocationDetails();

                                    collectionLocationDetails.setLatitude(String.valueOf(location.getLatitude()));
                                    collectionLocationDetails.setLongitude(String.valueOf(location.getLongitude()));
                                    collectionLocationDetails.setPlaceName(getAddress(location.getLatitude(), location.getLongitude()));

                                    edtCollectionPoint.setText(collectionLocationDetails.getPlaceName());
                                }
                            }
                        }
                );
            } else {
                Toast.makeText(this, "Turn on Location", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                },
                PERMISSION_ID
        );
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            edtCollectionPoint.setText(mLastLocation.getLatitude() + " , " + mLastLocation.getLongitude());
        }
    };

    private String getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            return obj.getAddressLine(0);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "unknown area";
        }
    }

    private void getLocationData(Intent data, EditText editText) {
        if (data != null) {

            Place place = Autocomplete.getPlaceFromIntent(data);
            LatLng activityLocation = place.getLatLng();

            if (activityLocation != null) {
                collectionPointLocation = activityLocation.latitude + "," + activityLocation.longitude;
                collectionPointLocationName = place.getName();
                editText.setText(place.getName());
            }
        }
    }

    private void getScannedData(Intent data, TextView textView) {

        String scannedCode = data.getStringExtra(AppConstantsUtils.SCAN_BARCODE_EXTRA);

        if (scannedCode != null && !scannedCode.isEmpty()) {

            String barcode = "CODE : " + scannedCode;
            textView.setText(barcode);

            isBarcodeScanned = true;
        }

        if (isBarcodeScanned) {

            showButtonScanned();

        }
    }

    private void showButtonDefault() {

        scanBarcode.setTextColor(getResources().getColor(R.color.quantum_googblue));
        scanBarcode.setText("scan barcode");

    }

    private void showButtonScanned() {

        scanBarcode.setTextColor(getResources().getColor(R.color.quantum_googred));
        scanBarcode.setText("clear scan");

    }


}
