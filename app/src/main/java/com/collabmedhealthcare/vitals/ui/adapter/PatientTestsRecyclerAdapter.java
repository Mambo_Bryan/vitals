package com.collabmedhealthcare.vitals.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.interfaces.RecyclerItemListener;
import com.collabmedhealthcare.vitals.data.models.PatientTestResult;

import java.util.ArrayList;

public class PatientTestsRecyclerAdapter extends RecyclerView.Adapter<PatientTestsRecyclerAdapter.PatientTestViewHolder> {

    private ArrayList<PatientTestResult> patientTestResults;
    private RecyclerItemListener mListener;

    public PatientTestsRecyclerAdapter(ArrayList<PatientTestResult> patientTestResults, RecyclerItemListener mListener) {
        this.patientTestResults = patientTestResults;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public PatientTestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_patient_test_result, parent, false);
        return new PatientTestViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PatientTestViewHolder holder, int position) {

        PatientTestResult testResult = patientTestResults.get(position);

        holder.resultDate.setText(testResult.getDate());
        holder.resultTest.setText(testResult.getResult());
        holder.resultSample.setText(testResult.getSample());

    }

    @Override
    public int getItemCount() {
        return patientTestResults.size();
    }


    public void removeItem(int position) {
        patientTestResults.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(PatientTestResult item, int position) {
        patientTestResults.add(position, item);
        notifyItemInserted(position);
    }

    public ArrayList<PatientTestResult> getData() {
        return patientTestResults;
    }

    public class PatientTestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView resultDate;
        private TextView resultSample;
        private TextView resultTest;

        private RecyclerItemListener mListener;

        public PatientTestViewHolder(@NonNull View itemView, RecyclerItemListener mListener) {
            super(itemView);

            resultDate = itemView.findViewById(R.id.tv_itemPatientTest_date);
            resultSample = itemView.findViewById(R.id.tv_itemPatientTest_sample);
            resultTest = itemView.findViewById(R.id.tv_itemPatientTest_result);

            this.mListener = mListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onItemSelected(getAdapterPosition());
        }
    }

}
