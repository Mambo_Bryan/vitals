package com.collabmedhealthcare.vitals.ui.views.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.PatientData;
import com.collabmedhealthcare.vitals.data.models.PatientLocationDetails;
import com.collabmedhealthcare.vitals.data.models.PatientRequestBody;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.objects.NextOfKin;
import com.collabmedhealthcare.vitals.data.models.request.ContactProfileRequest;
import com.collabmedhealthcare.vitals.data.models.request.NextOfKinRequest;
import com.collabmedhealthcare.vitals.data.models.request.PatientProfileRequest;
import com.collabmedhealthcare.vitals.data.models.request.PatientVitalsRequest;
import com.collabmedhealthcare.vitals.data.models.request.UpdatePatientRequest;
import com.collabmedhealthcare.vitals.data.models.response.PatientResponse;
import com.collabmedhealthcare.vitals.data.models.response.UpdateResponse;
import com.collabmedhealthcare.vitals.ui.views.LoadingDialog;
import com.collabmedhealthcare.vitals.ui.views.NextOfKinsBottomSheet;
import com.collabmedhealthcare.vitals.ui.views.RequestSampleBottomSheet;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPatientActivity extends AppCompatActivity implements View.OnClickListener,
        CompoundButton.OnCheckedChangeListener, ChipGroup.OnCheckedChangeListener, NextOfKinsBottomSheet.AddNextOfKinListener, RequestSampleBottomSheet.OnSampleRequestListener {

    public static final int PERMISSION_ID = 44;

    String[] resultsArray = {"negative", "positive"};
    String result = null;
    String[] testedArray = {"no", "yes"};
    String tested = null;
    String gender = null;

    boolean isCollectingSample = false;

    ArrayList<String> symptoms = new ArrayList<>();
    ArrayList<NextOfKinRequest> nextOfKinsList = new ArrayList<>();

    private EditText edtName;
    private EditText edtId;
    private EditText edtAge;
    private EditText edtMobile;

    AutoCompleteTextView edtHome;
    private EditText edtWork;
    private EditText edtOtherDetails;

    private LinearLayout layoutVitals;

    private EditText edtTemperature;
    private EditText edtVisits;
    private EditText edtCollectionPoint;

    CheckBox checkBoxDryCough;
    CheckBox checkBoxFever;
    CheckBox checkBoxTiredness;

    ChipGroup chipTested;
    boolean isTested = false;
    ChipGroup chipResult;
    boolean isResult = false;
    ChipGroup chipGroupGender;

    PatientLocationDetails homeLocationDetails;
    PatientLocationDetails workLocationDetails;
    PatientLocationDetails collectionLocationDetails;

    private LinearLayout linearLayoutResults;
    private LoadingDialog loadingDialog;
    private FusedLocationProviderClient mFusedLocationClient;
    private PatientRequestBody myPatientRequestBody;
    private ApiService apiService;
    private String token;

    private String[] permissions;
    private ImageView patientImage;
    private ChipGroup chipGroupNextOfKins;

    private ScreenedPatient screenedPatient;
    private Button btnSaveAndCollect;
    private Button btnSave;
    private PatientData responseData;
    private UpdatePatientRequest updatePatientRequest;
    private int activityCode;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_patient);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent myIntent = getIntent();

        Gson gson = new Gson();

        activityCode = myIntent.getIntExtra(AppConstantsUtils.SCREENED_PATIENT_ACTIVITY_CODE_EXTRA, AppConstantsUtils.ERROR);

        initViews();

        switch (activityCode) {
            case AppConstantsUtils.PATIENT_ADD:
                getSupportActionBar().setTitle("Add Patient");
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
                break;
            case AppConstantsUtils.PATIENT_EDIT:
            case AppConstantsUtils.PATIENT_ADD_FROM_CONTACT:
                String patientJson = myIntent.getStringExtra(AppConstantsUtils.SCREENED_PATIENT_ACTIVITY_EXTRA);
                screenedPatient = gson.fromJson(patientJson, ScreenedPatient.class);
                getSupportActionBar().setTitle("Update Patient ");
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_24);
                showUpdateViews();
                break;
            default:
                break;
        }

        loadingDialog = new LoadingDialog(this);

        apiService = ApiClient.createService(ApiService.class);

        SharedPrefsUtil prefsUtil = SharedPrefsUtil.getInstance(this);
        token = prefsUtil.getUserAccessToken();

        hideResultsLayout();
        getLastLocation();
    }

    private void showUpdateViews() {

        edtName.setText(screenedPatient.getFullName());
        edtId.setText(String.valueOf(screenedPatient.getIdNo()));

        String[] age = screenedPatient.getAgeFriendly().split(" ");
        edtAge.setText(age[0]);

        edtMobile.setText(screenedPatient.getMobile());
        PatientLocationDetails homePlaceName = screenedPatient.getHomeLocation();
        if (homePlaceName != null)
            edtHome.setText(homePlaceName.getPlaceName());
        PatientLocationDetails workPlaceName = screenedPatient.getWorkLocation();
        if (workPlaceName != null)
            edtWork.setText(workPlaceName.getPlaceName());

        //TODO add method for next of kins
        List<NextOfKin> kins = screenedPatient.getNok();

        if (kins != null && !kins.isEmpty()) {

            nextOfKinsList = new ArrayList<>();

            for (NextOfKin kin : kins) {

                NextOfKinRequest kinRequest = new NextOfKinRequest();
                kinRequest.setFirstName(kin.getFirstName());
                kinRequest.setMiddleName(kin.getMiddleName());
                kinRequest.setLastName(kin.getLastName());
                kinRequest.setMobile(kin.getMobile());
                kinRequest.setEmail(kin.getEmail());
                kinRequest.setRelationshipId(kin.getRelationshipId());

                nextOfKinsList.add(kinRequest);
            }

            createNextOfKins(nextOfKinsList);

        }

        String gender = screenedPatient.getSex();

        if (gender.equals("Male")) {
            chipGroupGender.check(R.id.chip_male);
        } else {
            chipGroupGender.check(R.id.chip_female);
        }

        if (activityCode != AppConstantsUtils.PATIENT_ADD_FROM_CONTACT) {
            layoutVitals.setVisibility(View.GONE);
            btnSave.setText("update");
            btnSave.setBackgroundColor(getResources().getColor(R.color.quantum_googgreen));
            btnSaveAndCollect.setVisibility(View.GONE);
        }

    }

    private void initViews() {

        btnSaveAndCollect = findViewById(R.id.btn_save_and_collect);
        btnSaveAndCollect.setOnClickListener(this);

        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);

        findViewById(R.id.btn_set_home).setOnClickListener(this);
        findViewById(R.id.btn_set_work).setOnClickListener(this);
        findViewById(R.id.fab_patient_screen).setOnClickListener(this);
        findViewById(R.id.tv_patient_next_of_kins).setOnClickListener(this);

        patientImage = findViewById(R.id.iv_patient_image);

        edtName = findViewById(R.id.edt_patient_name);
        edtId = findViewById(R.id.edt_patient_id);
        edtAge = findViewById(R.id.edt_patient_age);
        edtMobile = findViewById(R.id.edt_patient_mobile);

        edtHome = findViewById(R.id.edt_patient_home);
        edtWork = findViewById(R.id.edt_patient_work);
        edtOtherDetails = findViewById(R.id.edt_patient_other_details);

        layoutVitals = findViewById(R.id.linearLayout_screenPatient_vitals);

        edtTemperature = findViewById(R.id.edt_patient_temperature);
        edtVisits = findViewById(R.id.edt_patient_visits);
        edtCollectionPoint = findViewById(R.id.edt_patient_collection_point);
        edtCollectionPoint.setEnabled(false);

        chipGroupNextOfKins = findViewById(R.id.chip_group_next_of_kins);
        chipGroupNextOfKins.setVisibility(View.GONE);

        checkBoxDryCough = findViewById(R.id.checkbox_symptoms_dry_cough);
        checkBoxDryCough.setOnCheckedChangeListener(this);

        checkBoxFever = findViewById(R.id.checkbox_symptoms_fever);
        checkBoxFever.setOnCheckedChangeListener(this);
        checkBoxTiredness = findViewById(R.id.checkbox_symptoms_tiredness);
        checkBoxTiredness.setOnCheckedChangeListener(this);

        chipTested = findViewById(R.id.chip_group_tested);
        chipTested.setOnCheckedChangeListener(this);

        chipResult = findViewById(R.id.chip_group_result);
        chipResult.setOnCheckedChangeListener(this);

        chipGroupGender = findViewById(R.id.chip_group_gender);
        chipGroupGender.setOnCheckedChangeListener(this);
        chipGroupGender.check(R.id.chip_male);

        linearLayoutResults = findViewById(R.id.linearLayout_result);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

    }

    private void showResultsLayout() {
        linearLayoutResults.setVisibility(View.VISIBLE);
    }

    private void hideResultsLayout() {
        linearLayoutResults.setVisibility(View.GONE);
        chipTested.check(R.id.chip_tested_no);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View v) {

        /**
         * Initialize Places. For simplicity, the API key is hard-coded. In a production
         * environment we recommend using a secure mechanism to manage API keys.
         */
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), AppConstantsUtils.LOCATION_API_KEY);
        }

        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .build(this);

        switch (v.getId()) {

            case R.id.btn_save_and_collect:
                if (isValidUserInputs()) {
                    loadingDialog.showDialog();

                    switch (activityCode) {
                        case AppConstantsUtils.PATIENT_ADD:
                        case AppConstantsUtils.PATIENT_ADD_FROM_CONTACT:
                            createPatientObject();
                            isCollectingSample = true;
                            createNewPatient();
                            break;
                        default:
                            break;
                    }
                }

                break;

            case R.id.btn_save:
                if (isValidUserInputs()) {
                    loadingDialog.showDialog();

                    switch (activityCode) {
                        case AppConstantsUtils.PATIENT_ADD:
                        case AppConstantsUtils.PATIENT_ADD_FROM_CONTACT:

                            createPatientObject();
                            isCollectingSample = false;
                            createNewPatient();
                            break;
                        case AppConstantsUtils.PATIENT_EDIT:
                            createUpdatedPatientDetails();
                            updatePatientDetails();
                            break;
                        default:
                            break;
                    }

                }
                break;

            case R.id.btn_set_home:
                startActivityForResult(intent, AppConstantsUtils.REQUEST_CODE_MAPS_LOCATION_HOME);
                break;

            case R.id.btn_set_work:
                startActivityForResult(intent, AppConstantsUtils.REQUEST_CODE_MAPS_LOCATION_WORk);
                break;

            case R.id.fab_patient_screen:
                selectImage();
                break;

            case R.id.tv_patient_next_of_kins:

                NextOfKinsBottomSheet bottomSheet = new NextOfKinsBottomSheet(this);
                bottomSheet.show(getSupportFragmentManager(), "Add Kins");

                break;

            default:
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppConstantsUtils.REQUEST_CODE_MAPS_LOCATION_HOME:
                    getLocationData(data, edtHome, AppConstantsUtils.REQUEST_CODE_MAPS_LOCATION_HOME);
                    break;

                case AppConstantsUtils.REQUEST_CODE_MAPS_LOCATION_WORk:
                    getLocationData(data, edtWork, AppConstantsUtils.REQUEST_CODE_MAPS_LOCATION_WORk);
                    break;

                case AppConstantsUtils.REQUEST_CODE_TAKE_PHOTO:
                    assert data != null;
                    Bitmap takenPictureImage = (Bitmap) data.getExtras().get("data");
                    patientImage.setImageBitmap(takenPictureImage);
                    break;

                case AppConstantsUtils.REQUEST_CODE_CHOOSE_FROM_GALLERY:
                    assert data != null;
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    if (selectedImage != null) {
                        Cursor cursor = getContentResolver().query(selectedImage,
                                filePathColumn, null, null, null);
                        if (cursor != null) {

                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            String picturePath = cursor.getString(columnIndex);

                            Bitmap bitmapImage = BitmapFactory.decodeFile(picturePath);

                            patientImage.setImageBitmap(bitmapImage);

                            cursor.close();
                        }

                    }

                    break;

                default:
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getLocationData(Intent data, EditText editText, int intentCode) {
        if (data != null) {

            Place place = Autocomplete.getPlaceFromIntent(data);
            LatLng activityLocation = place.getLatLng();

            if (activityLocation != null) {

                if (intentCode == AppConstantsUtils.REQUEST_CODE_MAPS_LOCATION_HOME) {

                    homeLocationDetails = new PatientLocationDetails();

                    homeLocationDetails.setLatitude(String.valueOf(activityLocation.latitude));
                    homeLocationDetails.setLongitude(String.valueOf(activityLocation.longitude));
                    homeLocationDetails.setPlaceName(place.getName());

                    editText.setText(place.getName());
                    return;
                }
                if (intentCode == AppConstantsUtils.REQUEST_CODE_MAPS_LOCATION_WORk) {

                    workLocationDetails = new PatientLocationDetails();

                    workLocationDetails.setLatitude(String.valueOf(activityLocation.latitude));
                    workLocationDetails.setLongitude(String.valueOf(activityLocation.longitude));
                    workLocationDetails.setPlaceName(place.getName());

                    editText.setText(place.getName());
                }
            }
        }
    }


    private void createUpdatedPatientDetails() {
        updatePatientRequest = new UpdatePatientRequest();

        updatePatientRequest.setAge(edtAge.getText().toString());
        updatePatientRequest.setProfile(createPatientProfile());
        updatePatientRequest.setNoks(getNextOfKins());
        updatePatientRequest.setPatientId(screenedPatient.getId());
    }

    private void createPatientObject() {

        myPatientRequestBody = new PatientRequestBody();

        myPatientRequestBody.setAge(Integer.valueOf(edtAge.getText().toString()));
        myPatientRequestBody.setVitals(createPatientVitals());

        if (activityCode == AppConstantsUtils.PATIENT_ADD)
            myPatientRequestBody.setProfile(createPatientProfile());
        else myPatientRequestBody.setProfile(createPatientProfileFromContact());
        myPatientRequestBody.setKins(getNextOfKins());

    }

    private ContactProfileRequest createPatientProfileFromContact() {

        ContactProfileRequest profileRequest = new ContactProfileRequest();

        String[] name = edtName.getText().toString().split(" ");

        profileRequest.setFirstName(name[0]);
        profileRequest.setMiddleName(name[1]);

        if (name.length > 2) {
            profileRequest.setLastName(name[2]);
        } else {
            profileRequest.setLastName("...");
        }

        profileRequest.setIdNo(edtId.getText().toString());
        profileRequest.setPatientID(screenedPatient.getId());
        profileRequest.setMobile(edtMobile.getText().toString());

        if (screenedPatient != null) {
            if (homeLocationDetails == null) {
                homeLocationDetails = screenedPatient.getHomeLocation();
            }

            if (workLocationDetails == null) {
                workLocationDetails = screenedPatient.getWorkLocation();
            }

        }

        profileRequest.setHome(homeLocationDetails);
        profileRequest.setWork(workLocationDetails);

        profileRequest.setSex(gender);

        return profileRequest;
    }

    private PatientProfileRequest createPatientProfile() {

        PatientProfileRequest profileRequest = new PatientProfileRequest();

        String[] name = edtName.getText().toString().split(" ");

        profileRequest.setFirstName(name[0]);
        profileRequest.setMiddleName(name[1]);

        if (name.length > 2) {
            profileRequest.setLastName(name[2]);
        } else {
            profileRequest.setLastName("...");
        }

        profileRequest.setIdNo(edtId.getText().toString());
        profileRequest.setMobile(edtMobile.getText().toString());

        if (screenedPatient != null) {
            if (homeLocationDetails == null) {
                homeLocationDetails = screenedPatient.getHomeLocation();
            }

            if (workLocationDetails == null) {
                workLocationDetails = screenedPatient.getWorkLocation();
            }

        }

        profileRequest.setHome(homeLocationDetails);
        profileRequest.setWork(workLocationDetails);

        profileRequest.setSex(gender);

        return profileRequest;
    }

    private PatientVitalsRequest createPatientVitals() {

        PatientVitalsRequest vitalRequestBody = new PatientVitalsRequest();

        vitalRequestBody.setSymptoms(symptoms);
        vitalRequestBody.setVisitedPlaces(edtVisits.getText().toString());
        vitalRequestBody.setTemperature(Double.valueOf(edtTemperature.getText().toString()));
        vitalRequestBody.setCollectionPoint(collectionLocationDetails);

        return vitalRequestBody;
    }

    private String getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            return obj.getAddressLine(0);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "unknown area";
        }
    }

    private void createNewPatient() {

        Call<PatientResponse> call = apiService.screenNewPatient(token, myPatientRequestBody);
        call.enqueue(new Callback<PatientResponse>() {
            @Override
            public void onResponse(Call<PatientResponse> call, Response<PatientResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    PatientData patient = response.body().getPatientData();

                    if (isCollectingSample) {
                        showToastDetails("Patient Saved", Toast.LENGTH_SHORT);
                        requestSampleCollection(patient);
                    } else {
                        openMainActivity();
                    }
                    return;
                }

                loadingDialog.hideDialog();
                Toast.makeText(AddPatientActivity.this, "Couldn't save patient" +
                        " maybe a duplicate ID number", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<PatientResponse> call, Throwable t) {
                loadingDialog.hideDialog();
                Toast.makeText(AddPatientActivity.this, "Error Connecting", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void updatePatientDetails() {

        Call<UpdateResponse> call = apiService.updateScreenedPatient(token, screenedPatient.getId(),
                updatePatientRequest);

        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    showToastDetails("patient updated", Toast.LENGTH_SHORT);
                    openMainActivity();
                    return;
                }

                loadingDialog.hideDialog();
                showToastDetails("error updating patient", Toast.LENGTH_SHORT);
            }

            @Override
            public void onFailure(Call<UpdateResponse> call, Throwable t) {
                showToastDetails("failed connecting", Toast.LENGTH_SHORT);
                loadingDialog.hideDialog();

            }
        });

    }

    private void requestSampleCollection(PatientData patientData) {

        responseData = patientData;

        RequestSampleBottomSheet bottomSheet = new RequestSampleBottomSheet(responseData.getPatient(), this);
        bottomSheet.show(getSupportFragmentManager(), "Request Sample");

    }

    @Override
    public void onRequestSentSuccess(boolean isSuccessful) {
        if (isSuccessful) {
            Intent myIntent = new Intent(AddPatientActivity.this, SampleCollectionActivity.class);
            myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

            myIntent.putExtra(AppConstantsUtils.SAMPLE_COLLECTION_ACTIVITY_CODE_EXTRA, AppConstantsUtils.CODE_ADD_PATIENT_ACTIVITY);
            myIntent.putExtra(AppConstantsUtils.PATIENT_NAME_STRING, edtName.getText().toString());
            myIntent.putExtra(AppConstantsUtils.PATIENT_ID_STRING, responseData.getPatient().getId());

            startActivity(myIntent);
            finish();
        }
    }

    private List<NextOfKinRequest> getNextOfKins() {
        return nextOfKinsList;
    }

    private void openMainActivity() {
        Intent myIntent = new Intent(AddPatientActivity.this, ScreenedPatientsListActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(myIntent);
        loadingDialog.hideDialog();
        finish();
    }


    private void showToastDetails(String s, int lengthShort) {
        Toast.makeText(this, s, lengthShort).show();
    }

    private boolean isValidUserInputs() {

        getLastLocation();

        boolean isValidInput = true;

        if (edtName.getText().toString().trim().isEmpty()) {
            edtName.setError("Enter Name");
            isValidInput = false;
        }

        String[] username = edtName.getText().toString().split(" ");
        if (username.length < 1) {
            edtName.setError("Enter Full Names");
            isValidInput = false;
        }

        if (edtId.getText().toString().trim().isEmpty()) {
            edtId.setError("Enter ID");
            isValidInput = false;
        }

        if (edtAge.getText().toString().trim().isEmpty()) {
            edtAge.setError("Enter Age");
            isValidInput = false;
        }
        if (edtMobile.getText().toString().trim().isEmpty()) {
            edtMobile.setError("Enter Mobile");
            isValidInput = false;
        }
        if (edtHome.getText().toString().trim().isEmpty()) {
            edtHome.setError("Enter Home Address");
            isValidInput = false;
        }
        if (edtWork.getText().toString().trim().isEmpty()) {
            edtWork.setError("Enter Work Address");
            isValidInput = false;
        }

        if (activityCode != AppConstantsUtils.PATIENT_EDIT) {
            if (edtTemperature.getText().toString().trim().isEmpty()) {
                edtTemperature.setError("Enter Temperature");
                isValidInput = false;
            }

            if (edtVisits.getText().toString().trim().isEmpty()) {
                edtVisits.setError("Enter Valid Places");
                isValidInput = false;
            }
        }

        if (edtCollectionPoint.getText().toString().trim().isEmpty()) {
            edtVisits.setError("Enter Valid Collection Point");
            isValidInput = false;
        }

        if (nextOfKinsList.isEmpty()) {
            Toast.makeText(this, "Please add Next of kin", Toast.LENGTH_SHORT).show();
            isValidInput = false;
        }

        return isValidInput;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.checkbox_symptoms_dry_cough:
                if (isChecked) {
                    symptoms.add("dry cough");
                } else {
                    symptoms.remove("dry cough");
                }
                break;
            case R.id.checkbox_symptoms_fever:
                if (isChecked) {
                    symptoms.add("fever");
                } else {
                    symptoms.remove("fever");
                }
                break;
            case R.id.checkbox_symptoms_tiredness:
                if (isChecked) {
                    symptoms.add("tiredness");
                } else {
                    symptoms.remove("tiredness");
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onCheckedChanged(ChipGroup group, int checkedId) {
        switch (group.getId()) {
            case R.id.chip_group_tested:
                switch (checkedId) {
                    case R.id.chip_tested_yes:
                        showResultsLayout();
                        isTested = true;
                        tested = testedArray[1];
                        break;
                    case R.id.chip_tested_no:
                        hideResultsLayout();
                        isTested = false;
                        tested = testedArray[0];
                    default:
                        hideResultsLayout();
                        isTested = false;
                        tested = null;
                        break;
                }
                break;

            case R.id.chip_group_result:
                switch (checkedId) {
                    case R.id.chip_posititve:
                        isResult = true;
                        result = resultsArray[1];
                        break;
                    case R.id.chip_negative:
                        isResult = false;
                        result = resultsArray[0];
                    default:
                        isResult = false;
                        result = null;
                        break;
                }
                break;

            case R.id.chip_group_gender:
                switch (checkedId) {
                    case R.id.chip_male:
                        gender = "male";
                        break;
                    case R.id.chip_female:
                        gender = "female";
                    default:
                        gender = "male";
                        break;
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Granted. Start getting the location information
//                getLastLocation();
            }
        }
    }

    private boolean checkPermissions() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                },
                PERMISSION_ID
        );
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {

                                    collectionLocationDetails = new PatientLocationDetails();

                                    collectionLocationDetails.setLatitude(String.valueOf(location.getLatitude()));
                                    collectionLocationDetails.setLongitude(String.valueOf(location.getLongitude()));
                                    collectionLocationDetails.setPlaceName(getAddress(location.getLatitude(), location.getLongitude()));

                                    edtCollectionPoint.setText(collectionLocationDetails.getPlaceName());
                                }
                            }
                        }
                );
            } else {
                showToastDetails("Turn on location", Toast.LENGTH_LONG);
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            edtCollectionPoint.setText(mLastLocation.getLatitude() + " , " + mLastLocation.getLongitude());
        }
    };


    private void createPermissions() {
        permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA};
    }

    private void selectImage() {

        createPermissions();

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (checkPermissions()) {

                    if (item == 0) {

                        Toast.makeText(AddPatientActivity.this, "photo selected", Toast.LENGTH_SHORT).show();

                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        AddPatientActivity.this.startActivityForResult(takePicture,
                                AppConstantsUtils.REQUEST_CODE_TAKE_PHOTO);

                    } else if (options[item].equals("Choose from Gallery")) {

                        Toast.makeText(AddPatientActivity.this, "gallery selected", Toast.LENGTH_SHORT).show();

                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        AddPatientActivity.this.startActivityForResult(pickPhoto,
                                AppConstantsUtils.REQUEST_CODE_CHOOSE_FROM_GALLERY);

                    } else if (options[item].equals("Cancel")) {

                        dialog.dismiss();

                    }

                } else {

                    Toast.makeText(AddPatientActivity.this, "request permissions", Toast.LENGTH_SHORT).show();

                    requestPermissions();
                }
            }
        });
        builder.show();
    }


    @Override
    public void onNextOfKinSaved(ArrayList<NextOfKinRequest> nextOfKins) {

        chipGroupNextOfKins.removeAllViews();

        if (nextOfKins.isEmpty()) {

            chipGroupNextOfKins.setVisibility(View.GONE);

        } else {

            this.nextOfKinsList = nextOfKins;

            createNextOfKins(nextOfKins);

        }

    }

    private void createNextOfKins(ArrayList<NextOfKinRequest> nextOfKins) {
        chipGroupNextOfKins.setVisibility(View.VISIBLE);

        for (int i = 0; i < nextOfKins.size(); i++) {

            NextOfKinRequest nextOfKin = nextOfKinsList.get(i);

            Chip nextOfKinChip = new Chip(this);
            nextOfKinChip.setId(i);
            String nextOfKinName = nextOfKin.getFirstName() + " " + nextOfKin.getMiddleName();
            nextOfKinChip.setText(nextOfKinName);
            nextOfKinChip.setCloseIconVisible(true);

            nextOfKinChip.setOnCloseIconClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chipGroupNextOfKins.removeView(v);
                    nextOfKinsList.remove(v.getId());

                    if (nextOfKinsList.isEmpty()) {
                        chipGroupNextOfKins.setVisibility(View.GONE);
                    }
                }
            });

            chipGroupNextOfKins.addView(nextOfKinChip);

        }
    }

}