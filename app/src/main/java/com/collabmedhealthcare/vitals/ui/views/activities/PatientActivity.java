package com.collabmedhealthcare.vitals.ui.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.collabmedhealthcare.vitals.ui.adapter.TemplatePageAdapter;
import com.collabmedhealthcare.vitals.ui.viewmodels.ScreenedPatientViewModel;
import com.collabmedhealthcare.vitals.ui.views.fragments.PatientDetailsFragment;
import com.collabmedhealthcare.vitals.ui.views.fragments.PatientTestHistoryFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import java.util.ArrayList;

public class PatientActivity extends AppCompatActivity {

    ScreenedPatient patient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracing_contact_details);

        Toolbar toolbar = findViewById(R.id.toolbar_template_contactTracing);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Intent myIntent = getIntent();

        Gson gson = new Gson();

        String patientJson = myIntent.getStringExtra(AppConstantsUtils.SCREENED_PATIENT_ACTIVITY_EXTRA);
        patient = gson.fromJson(patientJson, ScreenedPatient.class);

        getSupportActionBar().setTitle(patient.getFullName());

        initViews();

        ScreenedPatientViewModel viewModel = new ViewModelProvider(this).get(ScreenedPatientViewModel.class);
        viewModel.init(patient);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void initViews() {

        TabLayout tabLayout = findViewById(R.id.tabLayout_levelOne);
        ViewPager viewPager = findViewById(R.id.viewpager_levelOne);

        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new PatientDetailsFragment());
        fragments.add(new PatientTestHistoryFragment());

        String[] titles = {"Details", "Test History"};

        TemplatePageAdapter adapter = new TemplatePageAdapter(getSupportFragmentManager(),
                fragments, titles);

        viewPager.setAdapter(adapter);
        // Give the TabLayout the ViewPager
        tabLayout.setupWithViewPager(viewPager);

        FloatingActionButton fab = findViewById(R.id.fab_levelOne);
        fab.setVisibility(View.GONE);

    }


}