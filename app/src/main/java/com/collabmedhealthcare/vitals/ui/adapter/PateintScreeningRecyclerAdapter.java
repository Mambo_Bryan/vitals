package com.collabmedhealthcare.vitals.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.DummyPatient;

import java.util.ArrayList;

public class PateintScreeningRecyclerAdapter extends RecyclerView.Adapter<PateintScreeningRecyclerAdapter.ScreeningViewHolder> {

    ArrayList<DummyPatient> dummyPatients;

    public PateintScreeningRecyclerAdapter(ArrayList<DummyPatient> dummyPatients) {
        this.dummyPatients = dummyPatients;
    }

    @NonNull
    @Override
    public ScreeningViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_screened_patient, parent, false);
        return new ScreeningViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ScreeningViewHolder holder, int position) {

        holder.patientName.setText(dummyPatients.get(position).getName());
        holder.patientTemperature.setText(String.valueOf(dummyPatients.get(position).getTemprature()));
        holder.patientTested.setText(dummyPatients.get(position).getTested().toString());

        int remainder = position % 2;
        if (remainder == 0) {
            holder.background.setBackgroundColor(holder.background.getResources().getColor(R.color.white1));
        } else {
            holder.background.setBackgroundColor(holder.background.getResources().getColor(R.color.white2));
        }

    }

    @Override
    public int getItemCount() {
        return dummyPatients.size();
    }

    class ScreeningViewHolder extends RecyclerView.ViewHolder {

        TextView patientName;
        TextView patientTemperature;
        TextView patientTested;
        ImageView background;

        public ScreeningViewHolder(@NonNull View itemView) {
            super(itemView);

            patientName = itemView.findViewById(R.id.tv_screen_name);
            patientTemperature = itemView.findViewById(R.id.tv_screen_temprature);
            patientTested = itemView.findViewById(R.id.tv_screening_tested);
            background = itemView.findViewById(R.id.iv_screen_background);
        }
    }
}
