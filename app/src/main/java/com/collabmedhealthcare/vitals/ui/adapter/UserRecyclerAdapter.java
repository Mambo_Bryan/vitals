package com.collabmedhealthcare.vitals.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.interfaces.RecyclerItemListener;
import com.collabmedhealthcare.vitals.data.models.objects.SystemUser;

import java.util.ArrayList;
import java.util.List;

public class UserRecyclerAdapter extends RecyclerView.Adapter<UserRecyclerAdapter.UserViewHolder> implements Filterable {

    private ArrayList<SystemUser> systemUsers;
    private ArrayList<SystemUser> filteredUsers;
    private RecyclerItemListener mListener;

    public UserRecyclerAdapter(ArrayList<SystemUser> users, RecyclerItemListener mListener) {
        this.systemUsers = users;
        filteredUsers = new ArrayList<>(users);
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_system_user, parent, false);
        return new UserViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {

        SystemUser user = filteredUsers.get(position);

        holder.userFullName.setText(user.getFullName());

    }

    @Override
    public int getItemCount() {
        return filteredUsers.size();
    }

    @Override
    public Filter getFilter() {
        return usersFilter;
    }

    private Filter usersFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String queryString = constraint.toString().toLowerCase();

            if (queryString.isEmpty()) {
                filteredUsers = systemUsers;
            } else {

                List<SystemUser> filteredList = new ArrayList<>();

                for (SystemUser user : systemUsers) {
                    if (user.getFullName().toLowerCase().contains(queryString.trim())) {
                        filteredList.add(user);
                    }
                }

                filteredUsers = new ArrayList<>(filteredList);
            }

            FilterResults results = new FilterResults();
            results.values = filteredUsers;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredUsers = ((ArrayList<SystemUser>) results.values);
            notifyDataSetChanged();
        }
    };

    public void removeItem(int position) {
        systemUsers.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(SystemUser item, int position) {
        systemUsers.add(position, item);
        notifyItemInserted(position);
    }

    public class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView userFullName;
        private ImageView addUser;

        private RecyclerItemListener mListener;

        public UserViewHolder(@NonNull View itemView, RecyclerItemListener mListener) {
            super(itemView);

            userFullName = itemView.findViewById(R.id.tv_bottomSheet_username);
            addUser = itemView.findViewById(R.id.iv_bottomSheet_add);


            this.mListener = mListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onItemSelected(getAdapterPosition());
        }
    }

}
