package com.collabmedhealthcare.vitals.ui.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.collabmedhealthcare.vitals.data.models.response.DefaultMessageResponse;
import com.collabmedhealthcare.vitals.data.models.response.ScreenedPatientsResponse;
import com.collabmedhealthcare.vitals.repositories.SampleRepository;
import com.collabmedhealthcare.vitals.repositories.ScreeningRepository;

public class ScreenedPatientsViewModel extends ViewModel {

    private static ScreeningRepository repository;
    private static SampleRepository sampleRepository;

    private MutableLiveData<ScreenedPatientsResponse> screenedPatientsResponse = new MutableLiveData<>();
    private MutableLiveData<DefaultMessageResponse> requestSampleResponse;

    public void init(String token) {

        int page = 1;

        repository = ScreeningRepository.getInstance(token);
        sampleRepository = SampleRepository.getInstance(token);

        getScreenedPatientList(page);

    }

    public LiveData<ScreenedPatientsResponse> getScreenedPatientsResponse() {

        if (screenedPatientsResponse == null) {
            screenedPatientsResponse = new MutableLiveData<>();
        }

        return screenedPatientsResponse;
    }

    public void getScreenedPatientList(int page) {

        screenedPatientsResponse = repository.getScreenedPatients(page);

    }

    public void getSearchedScreenedPatientList(String query, int page) {

        screenedPatientsResponse = repository.getSearchedScreenedPatients(query, page);

    }

    public LiveData<DefaultMessageResponse> getRequestSampleResponse() {

        if (requestSampleResponse == null) {
            requestSampleResponse = new MutableLiveData<>();
        }

        return requestSampleResponse;
    }

    public void refresh() {
        screenedPatientsResponse = repository.getScreenedPatients(1);
    }
}
