package com.collabmedhealthcare.vitals.ui.views.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.interfaces.RecyclerItemListener;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.Contact;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.collabmedhealthcare.vitals.data.models.objects.NextOfKin;
import com.collabmedhealthcare.vitals.ui.adapter.NextOfKinRecyclerAdapter;
import com.collabmedhealthcare.vitals.ui.viewmodels.ScreenedPatientViewModel;
import com.collabmedhealthcare.vitals.ui.views.RequestSampleBottomSheet;
import com.collabmedhealthcare.vitals.ui.views.activities.AddPatientActivity;
import com.collabmedhealthcare.vitals.ui.views.activities.AddVitalActivity;
import com.collabmedhealthcare.vitals.ui.views.activities.SampleCollectionActivity;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PatientDetailsFragment extends Fragment implements View.OnClickListener, RecyclerItemListener, RequestSampleBottomSheet.OnSampleRequestListener {

    private View rootView;
    private ScreenedPatient currentPatient;

    private TextView contactName;
    private TextView contactMobile;
    private TextView contactHomeLocation;
    private TextView contactWorkLocation;
    private TextView contactScreenedDate;
    private TextView contactScreenedResults;
    private TextView contactTracker;
    private TextView contactTrackingStatus;
    private TextView contactIDNumber;
    private TextView contactAge;
    private TextView contactGender;
    private TextView contactOtherDetails;

    private XRecyclerView recyclerView;
    private ArrayList<NextOfKin> nextOfKins;
    private NextOfKinRecyclerAdapter adapter;
    private View noKinsView;

    private boolean isCollectingSample = false;

    public PatientDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_screened_patient, container, false);

        initViews();

        return rootView;
    }

    private void initViews() {

        contactName = rootView.findViewById(R.id.tv_screenedPatient_name);
        contactMobile = rootView.findViewById(R.id.tv_screenedPatient_mobile);
        contactHomeLocation = rootView.findViewById(R.id.tv_screenedPatient_location);
        contactHomeLocation.setOnClickListener(this);
        contactWorkLocation = rootView.findViewById(R.id.tv_screenedPatient_location_work);
        contactWorkLocation.setOnClickListener(this);

        contactIDNumber = rootView.findViewById(R.id.tv_screenedPatient_screened_id_number);
        contactAge = rootView.findViewById(R.id.tv_screenedPatient_screened_age);
        contactGender = rootView.findViewById(R.id.tv_screenedPatient_screened_gender);
        contactOtherDetails = rootView.findViewById(R.id.tv_screenedPatient_screened_other_details);

        contactScreenedDate = rootView.findViewById(R.id.tv_screenedPatient_screened_date);
        contactScreenedResults = rootView.findViewById(R.id.tv_screenedPatient_screened_results);

        contactTracker = rootView.findViewById(R.id.tv_screenedPatient_tracking_tracker);
        contactTrackingStatus = rootView.findViewById(R.id.tv_screenedPatient_tracking_status);

        noKinsView = rootView.findViewById(R.id.inlcude_screenedPatient_no_kins);
        noKinsView.setVisibility(View.GONE);

        TextView noDataText = noKinsView.findViewById(R.id.tv_template_no_data_message);
        noDataText.setText("No Next of Kin found for this patient.");

        recyclerView = rootView.findViewById(R.id.rv_screenedPatient_next_of_kins);

        rootView.findViewById(R.id.tv_screenedPatient_edit_details).setOnClickListener(this);
        rootView.findViewById(R.id.tv_screenedPatient_add_vital).setOnClickListener(this);
        rootView.findViewById(R.id.tv_screenedPatient_request_sample).setOnClickListener(this);
        rootView.findViewById(R.id.tv_screenedPatient_collect_sample).setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ScreenedPatientViewModel viewModel = new ViewModelProvider(requireActivity()).get(ScreenedPatientViewModel.class);
        viewModel.getScreenedPatient().observe(getViewLifecycleOwner(), new Observer<ScreenedPatient>() {
            @Override
            public void onChanged(ScreenedPatient patient) {
                if (patient != null) {

                    currentPatient = patient;

                    updateContactViews();

                    if (currentPatient.getNok() != null) {

                        nextOfKins = new ArrayList<>(currentPatient.getNok());

                        if (!nextOfKins.isEmpty()) {
                            initRecyclerViews();

                            return;
                        }
                    }

                    noKinsView.setVisibility(View.VISIBLE);

                }
            }
        });
    }

    private void initRecyclerViews() {

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new NextOfKinRecyclerAdapter(nextOfKins, this);

        recyclerView.setAdapter(adapter);

        recyclerView.setPullRefreshEnabled(false);
        recyclerView.setLoadingMoreEnabled(false);

    }

    private void updateContactViews() {

        setContactDetailsText(contactName, currentPatient.getFullName());
        setContactDetailsText(contactMobile, currentPatient.getMobile());
        setContactDetailsText(contactHomeLocation, currentPatient.getHomeLocation().getPlaceName());
        setContactDetailsText(contactWorkLocation, currentPatient.getWorkLocation().getPlaceName());
        setContactDetailsText(contactIDNumber, currentPatient.getIdNo());
        setContactDetailsText(contactAge, currentPatient.getAgeFriendly());
        setContactDetailsText(contactGender, currentPatient.getSex());
        setContactDetailsText(contactOtherDetails, "No details");

        if (currentPatient.getPatientTestResults() != null && !currentPatient.getPatientTestResults().isEmpty()) {
            setContactDetailsText(contactScreenedDate, currentPatient.getPatientTestResults().get(0).getDate());
            setContactDetailsText(contactScreenedResults, currentPatient.getPatientTestResults().get(0).getResult());
        } else {
            setContactDetailsText(contactScreenedDate, "No Date found");
            setContactDetailsText(contactScreenedResults, "Unknown Status");
        }

        List<Contact> contacts = currentPatient.getContacts();

        if (contacts != null && !contacts.isEmpty() &&
                contacts.get(0).getTrackee() != null && contacts.get(0).getTrackingStatus() != null) {
            contactTracker.setText(contacts.get(0).getTrackee());
            contactTrackingStatus.setText(contacts.get(0).getTrackingStatus());
        } else {
            contactTracker.setText("No Tracker");
            contactTrackingStatus.setText("Pending");
        }

    }

    public void setContactDetailsText(TextView textView, String text) {

        if (text == null) {
            textView.setText("Empty");
        } else {
            textView.setText(text);
        }

    }

    @Override
    public void onClick(View v) {

        Gson gson = new Gson();
        String clickedPatient = gson.toJson(currentPatient);

        switch (v.getId()) {

            case R.id.tv_screenedPatient_location:

                if (currentPatient.getHomeLocation() != null) {
                    Uri mapsIntentUri = Uri.parse("geo:" + currentPatient.getHomeLocation().getLatitude() +
                            "," + currentPatient.getHomeLocation().getLongitude());
                    Intent locationIntent = new Intent(Intent.ACTION_VIEW, mapsIntentUri);
                    locationIntent.setPackage("com.google.android.apps.maps");
                    if (locationIntent.resolveActivity(getContext().getPackageManager()) != null) {
                        startActivity(locationIntent);
                    }
                } else {
                    Toast.makeText(getContext(),
                            "Patient home location not specified"
                            , Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.tv_screenedPatient_location_work:

                if (currentPatient.getWorkLocation() != null) {
                    Uri mapsIntentUri = Uri.parse("geo:" + currentPatient.getWorkLocation().getLatitude() +
                            "," + currentPatient.getWorkLocation().getLongitude());
                    Intent locationIntent = new Intent(Intent.ACTION_VIEW, mapsIntentUri);
                    locationIntent.setPackage("com.google.android.apps.maps");
                    if (locationIntent.resolveActivity(getContext().getPackageManager()) != null) {
                        startActivity(locationIntent);
                    }
                } else {
                    Toast.makeText(getContext(),
                            "Patient work location not specified"
                            , Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.tv_screenedPatient_edit_details:

                Intent myIntent = new Intent(getContext(), AddPatientActivity.class);
                myIntent.putExtra(AppConstantsUtils.SCREENED_PATIENT_ACTIVITY_CODE_EXTRA,
                        AppConstantsUtils.PATIENT_EDIT);
                myIntent.putExtra(AppConstantsUtils.SCREENED_PATIENT_ACTIVITY_EXTRA, clickedPatient);
                startActivity(myIntent);

                break;

            case R.id.tv_screenedPatient_add_vital:

                Intent addVitalIntent = new Intent(getContext(), AddVitalActivity.class);
                addVitalIntent.putExtra(AppConstantsUtils.SCREENED_PATIENT_ACTIVITY_EXTRA, clickedPatient);
                startActivity(addVitalIntent);

                break;

            case R.id.tv_screenedPatient_request_sample:
                requestContactSample();
                break;

            case R.id.tv_screenedPatient_collect_sample:
                isCollectingSample = true;
                requestContactSample();
                break;

            default:
                break;
        }
    }

    private void requestContactSample() {

        RequestSampleBottomSheet bottomSheet = new RequestSampleBottomSheet(currentPatient, this);
        bottomSheet.show(getFragmentManager(), "Request Sample");

    }

    @Override
    public void onItemSelected(int position) {

    }

    @Override
    public void onRequestSentSuccess(boolean isSuccessful) {

        if (isCollectingSample) {

            if (isSuccessful) {
                Toast.makeText(getContext(), "request successful", Toast.LENGTH_SHORT).show();

                Intent myIntent = new Intent(getContext(), SampleCollectionActivity.class);

                myIntent.putExtra(AppConstantsUtils.PATIENT_NAME_STRING, currentPatient.getFullName());
                myIntent.putExtra(AppConstantsUtils.PATIENT_ID_STRING, currentPatient.getId());

                startActivity(myIntent);
                return;
            }

            isCollectingSample = false;

        }

    }
}
