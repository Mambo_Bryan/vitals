package com.collabmedhealthcare.vitals.ui.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.ui.adapter.TemplatePageAdapter;
import com.collabmedhealthcare.vitals.ui.views.fragments.SpecificContactTracingFragment;
import com.collabmedhealthcare.vitals.ui.views.fragments.SpecificContactTracingNotesFragment;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.collabmedhealthcare.vitals.ui.viewmodels.SpecificContactTraceViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class SpecificTracingActivity extends AppCompatActivity {

    ScreenedPatient patient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracing_contact_details);

        Toolbar toolbar = findViewById(R.id.toolbar_template_contactTracing);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent myIntent = getIntent();

        patient = myIntent.getParcelableExtra(AppConstantsUtils.SPECIFIC_TRACE_CONTACT_EXTRA);
        getSupportActionBar().setTitle(patient.getFullName());

        initViews();

        SpecificContactTraceViewModel viewModel = new ViewModelProvider(this).get(SpecificContactTraceViewModel.class);
        viewModel.init(patient);
    }

    private void initViews() {

        TabLayout tabLayout = findViewById(R.id.tabLayout_levelOne);
        ViewPager viewPager = findViewById(R.id.viewpager_levelOne);

        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new SpecificContactTracingFragment());
        fragments.add(new SpecificContactTracingNotesFragment());

        String[] titles = {"Details", "Notes"};

        TemplatePageAdapter adapter = new TemplatePageAdapter(getSupportFragmentManager(),
                fragments, titles);

        viewPager.setAdapter(adapter);
        // Give the TabLayout the ViewPager
        tabLayout.setupWithViewPager(viewPager);

        FloatingActionButton fab = findViewById(R.id.fab_levelOne);
        fab.setVisibility(View.GONE);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
