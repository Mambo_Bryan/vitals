package com.collabmedhealthcare.vitals.ui.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.request.RequestSampleBody;
import com.collabmedhealthcare.vitals.data.models.response.DefaultMessageResponse;
import com.collabmedhealthcare.vitals.data.models.response.ScreenedPatientsResponse;
import com.collabmedhealthcare.vitals.ui.adapter.ScreenedPatientsRecyclerAdapter;
import com.collabmedhealthcare.vitals.ui.viewmodels.ScreenedPatientsViewModel;
import com.collabmedhealthcare.vitals.ui.views.LoadingDialog;
import com.collabmedhealthcare.vitals.utils.ListViewUtils;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScreenedPatientsListActivity extends AppCompatActivity implements ScreenedPatientsRecyclerAdapter.OnScreenedPatientListener,
        SearchView.OnQueryTextListener, View.OnClickListener {

    //
    ArrayList<ScreenedPatient> screenedPatients;
    ScreenedPatientsRecyclerAdapter adapter;

    //
    private ApiService apiService;
    private String token;

    private SearchView searchView;
    private ListViewUtils mainViews;
    private XRecyclerView recyclerView;

    private int pageNumber;
    private int totalPageNumber;

    private boolean isUserSearching = false;
    private String searchQuery;
    private LoadingDialog loadingDialog;
    private ScreenedPatientsViewModel viewModel;
    private ScreenedPatient selectedPatient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template_recycler);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Screened Patients");

        initViews();
        initRecyclerCompleteViews();

        pageNumber = 1;

        mainViews.showLoading();

        viewModel = new ViewModelProvider(this).get(ScreenedPatientsViewModel.class);
        viewModel.init(token);
        viewModel.getScreenedPatientsResponse().observe(this, new Observer<ScreenedPatientsResponse>() {
            @Override
            public void onChanged(ScreenedPatientsResponse response) {
                if (response != null) {

                    if (searchQuery == null) {
                        totalPageNumber = response.getMeta().getLastPage();
                    }

                    if (pageNumber == 1) {

                        if (!response.getScreenedPatients().isEmpty()) {

                            initRecycler(response);
                            mainViews.showContent();
                            recyclerView.refreshComplete();

                        } else {
                            mainViews.showNoData();
                        }

                    } else {

                        updateRecycler(response);

                    }

                    return;
                }

                mainViews.showError();
                Toast.makeText(ScreenedPatientsListActivity.this, "error getting results", Toast.LENGTH_SHORT).show();
            }
        });

        viewModel.getRequestSampleResponse().observe(this, new Observer<DefaultMessageResponse>() {
            @Override
            public void onChanged(DefaultMessageResponse response) {
                if (response != null && response.getMessage() != null) {

                    Toast.makeText(ScreenedPatientsListActivity.this, "request successful", Toast.LENGTH_SHORT).show();

                    Intent myIntent = new Intent(ScreenedPatientsListActivity.this, SampleCollectionActivity.class);

                    myIntent.putExtra(AppConstantsUtils.PATIENT_NAME_STRING, selectedPatient.getFullName());
                    myIntent.putExtra(AppConstantsUtils.PATIENT_ID_STRING, selectedPatient.getId());

                    startActivity(myIntent);
                    loadingDialog.hideDialog();

                    return;

                }

                loadingDialog.hideDialog();
                Toast.makeText(ScreenedPatientsListActivity.this, "unable to collect sample", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        viewModel.refresh();
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {

            pageNumber = 1;
            isUserSearching = false;
            searchQuery = null;

            searchView.setIconified(true);
            mainViews.showLoading();
            viewModel.getScreenedPatientList(pageNumber);

        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem mSearchMenuItem = menu.findItem(R.id.menu_item_search);
        searchView = (SearchView) mSearchMenuItem.getActionView();
        searchView.setQueryHint("Search Patient");
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        searchQuery = query;

        if (!query.isEmpty()) {

            isUserSearching = true;
            pageNumber = 1;

            viewModel.getSearchedScreenedPatientList(searchQuery, pageNumber);
            mainViews.showLoading();

        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (screenedPatients != null && !screenedPatients.isEmpty()) {
            adapter.getFilter().filter(newText);
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_template_retry) {
            viewModel.getScreenedPatientList(pageNumber);
            mainViews.showLoading();
        } else if (v.getId() == R.id.fab_templateRecyler) {
            Intent myIntent = new Intent(this, AddPatientActivity.class);
            myIntent.putExtra(AppConstantsUtils.SCREENED_PATIENT_ACTIVITY_CODE_EXTRA,
                    AppConstantsUtils.PATIENT_ADD);
            startActivity(myIntent);
        }
    }

    private void initViews() {

        loadingDialog = new LoadingDialog(this);

        findViewById(R.id.fab_templateRecyler).setOnClickListener(this);

        SharedPrefsUtil prefsUtil = SharedPrefsUtil.getInstance(this);

        apiService = ApiClient.createService(ApiService.class);
        token = prefsUtil.getUserAccessToken();
    }

    private void initRecycler(ScreenedPatientsResponse patientsResponse) {

        screenedPatients = new ArrayList<>(patientsResponse.getScreenedPatients());

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new ScreenedPatientsRecyclerAdapter(recyclerView, screenedPatients, this,
                AppConstantsUtils.CODE_SCREENED_ACTIVITY);

        recyclerView.setAdapter(adapter);

        recyclerView.setPullRefreshEnabled(true);
        recyclerView.setLoadingMoreEnabled(true);

        recyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                pageNumber = 1;
                viewModel.getScreenedPatientList(pageNumber);
            }

            @Override
            public void onLoadMore() {
                pageNumber++;

                if (isUserSearching) {
                    viewModel.getSearchedScreenedPatientList(searchQuery, pageNumber);
                } else {
                    viewModel.getScreenedPatientList(pageNumber);
                }
            }
        });

    }

    private void updateRecycler(ScreenedPatientsResponse patientsResponse) {

        if (patientsResponse != null && patientsResponse.getScreenedPatients() != null) {

            screenedPatients.addAll(patientsResponse.getScreenedPatients());
            adapter.notifyDataSetChanged();

        }

        if (pageNumber >= totalPageNumber)
            recyclerView.setLoadingMoreEnabled(false);
    }

    private void initRecyclerCompleteViews() {
        View completeItemViews = findViewById(R.id.inlcude_templateRecycler_complete);

        View loading = completeItemViews.findViewById(R.id.include_template_loading);
        View noData = completeItemViews.findViewById(R.id.include_template_no_data);
        View error = completeItemViews.findViewById(R.id.include_template_error);
        View data = completeItemViews.findViewById(R.id.include_template_data);

        mainViews = new ListViewUtils(
                loading,
                noData,
                data,
                error,
                this
        );

        mainViews.setNoDataMessage("You haven't screened any contact, " +
                "please click below to screen new patient.");
        recyclerView = data.findViewById(R.id.rv_template_data);

    }

    @Override
    public void onScreenedPatientClicked(int menuItem, int position) {

        ScreenedPatient patient = screenedPatients.get(position);

        Gson gson = new Gson();

        String clickedPatient = gson.toJson(patient);

        Intent myIntent = new Intent(this, PatientActivity.class);
        myIntent.putExtra(AppConstantsUtils.SCREENED_PATIENT_ACTIVITY_EXTRA, clickedPatient);
        startActivity(myIntent);
    }

    private void requestSampleCollection(ScreenedPatient patient) {

        selectedPatient = patient;

        RequestSampleBody body = new RequestSampleBody();

        body.setOtherDetails("Auto initiated sample request");
        body.setSampleType(69);

        SharedPrefsUtil prefsUtil = SharedPrefsUtil.getInstance(this);
        ApiService apiService = ApiClient.createService(ApiService.class);

        Call<DefaultMessageResponse> call = apiService.requestSampleCollection(
                prefsUtil.getUserAccessToken(),
                patient.getId(),
                body
        );

        call.enqueue(new Callback<DefaultMessageResponse>() {
            @Override
            public void onResponse(Call<DefaultMessageResponse> call, Response<DefaultMessageResponse> response) {

                if (response.isSuccessful() && response.body() != null) {

                    Toast.makeText(ScreenedPatientsListActivity.this, "request successful", Toast.LENGTH_SHORT).show();

                    Intent myIntent = new Intent(ScreenedPatientsListActivity.this, SampleCollectionActivity.class);

                    myIntent.putExtra(AppConstantsUtils.PATIENT_NAME_STRING, patient.getFullName());
                    myIntent.putExtra(AppConstantsUtils.PATIENT_ID_STRING, patient.getId());

                    startActivity(myIntent);
                    loadingDialog.hideDialog();
                    return;

                }

                loadingDialog.hideDialog();
                Toast.makeText(ScreenedPatientsListActivity.this, "failed to initiate request", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<DefaultMessageResponse> call, Throwable t) {
                loadingDialog.hideDialog();
                Toast.makeText(ScreenedPatientsListActivity.this, "failed to initiate request", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
