package com.collabmedhealthcare.vitals.ui.views.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkIfUserIsLoggedIn();
    }

    private void checkIfUserIsLoggedIn() {

        SharedPrefsUtil prefsUtil = SharedPrefsUtil.getInstance(this);

        if (prefsUtil.isUserLoggedIn()) {

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();

        } else {

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}