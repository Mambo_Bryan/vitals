package com.collabmedhealthcare.vitals.ui.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.ContactData;
import com.collabmedhealthcare.vitals.ui.adapter.TemplatePageAdapter;
import com.collabmedhealthcare.vitals.ui.viewmodels.GeneralContactTracingViewModel;
import com.collabmedhealthcare.vitals.ui.views.fragments.GeneralContactTracingFragment;
import com.collabmedhealthcare.vitals.ui.views.fragments.GeneralContactTracingListFragment;
import com.collabmedhealthcare.vitals.ui.views.fragments.GeneralContactTracingNotesFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import java.util.ArrayList;

public class GeneralTracingContactActivity extends AppCompatActivity implements View.OnClickListener, SearchView.OnQueryTextListener {


    private ContactData contactData;
    private int tabLayoutPosition;
    private SearchView searchView;
    private GeneralContactTracingViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracing_contact_details);

        Toolbar toolbar = findViewById(R.id.toolbar_template_contactTracing);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent myIntent = getIntent();

        contactData = myIntent.getParcelableExtra(AppConstantsUtils.GENERAL_TRACE_CONTACT_EXTRA);
        getSupportActionBar().setTitle(contactData.getContact());

        initViews();

        viewModel = new ViewModelProvider(this).get(GeneralContactTracingViewModel.class);


    }


    private void initViews() {

        TabLayout tabLayout = findViewById(R.id.tabLayout_levelOne);
        ViewPager viewPager = findViewById(R.id.viewpager_levelOne);

        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new GeneralContactTracingFragment(contactData));
        fragments.add(new GeneralContactTracingNotesFragment());
        fragments.add(new GeneralContactTracingListFragment(contactData.getId()));

        String[] titles = {"Details", "Notes", "Contacts"};

        TemplatePageAdapter adapter = new TemplatePageAdapter(getSupportFragmentManager(),
                fragments, titles);

        viewPager.setAdapter(adapter);
        // Give the TabLayout the ViewPager
        tabLayout.setupWithViewPager(viewPager);

        FloatingActionButton fab =
                findViewById(R.id.fab_levelOne);
        fab.setOnClickListener(this);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                tabLayoutPosition = tab.getPosition();

                switch (tab.getPosition()) {

                    case 0:
                        fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_black_24dp));
                        break;

                    case 1:
                        fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_mode_comment_black_24dp));
                        break;

                    case 2:

                    default:
                        fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_24dp));
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem mSearchMenuItem = menu.findItem(R.id.menu_item_search);
        searchView = (SearchView) mSearchMenuItem.getActionView();
        searchView.setQueryHint("Search Patient");
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.fab_levelOne) {

            switch (tabLayoutPosition) {

                case 0:
                    Intent updateContact = new Intent(this, AddContactTraceActivity.class);

                    Gson gson = new Gson();
                    String contactDataString = gson.toJson(contactData);

                    updateContact.putExtra(AppConstantsUtils.CONTACT_TRACE_STRING_EXTRA,
                            AppConstantsUtils.LEVEL_1_CONTACT_TRACE_UPDATE_INT);

                    updateContact.putExtra(AppConstantsUtils.GENERAL_TRACE_CONTACT_EXTRA,
                            contactDataString);

                    startActivity(updateContact);
                    break;

                case 1:
                    Toast.makeText(this, "Cannot add notes yet", Toast.LENGTH_SHORT).show();
                    break;

                case 2:
                default:
                    Intent addContactIntent = new Intent(this, AddContactTraceActivity.class);

                    addContactIntent.putExtra(AppConstantsUtils.CONTACT_TRACE_STRING_EXTRA,
                            AppConstantsUtils.LEVEL_2_CONTACT_TRACE_INT);
                    addContactIntent.putExtra(AppConstantsUtils.GENERAL_TRACE_CONTACT_EXTRA,
                            contactData);

                    startActivity(addContactIntent);
                    break;

            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstantsUtils.LEVEL_2_CONTACT_TRACE_INT) {
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(this, "updating list", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        viewModel.setSearchQuery(query);
        viewModel.setIsSearching(true);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        viewModel.setSearchQuery(newText);
        return true;
    }
}
