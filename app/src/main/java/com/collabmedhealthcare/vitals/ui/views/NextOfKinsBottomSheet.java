package com.collabmedhealthcare.vitals.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.NextOfKinRelationship;
import com.collabmedhealthcare.vitals.utils.RelationshipsUtils;
import com.collabmedhealthcare.vitals.data.models.request.NextOfKinRequest;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;

public class NextOfKinsBottomSheet extends BottomSheetDialogFragment {

    private final ArrayList<NextOfKinRelationship> relationships;
    private EditText name;
    private EditText mobile;
    private EditText email;

    private AddNextOfKinListener mListener;
    private ArrayList<NextOfKinRequest> nextOfKinRelationshipsRequest;

    private NextOfKinRelationship selectedRelationship;

    public NextOfKinsBottomSheet(AddNextOfKinListener mListener) {
        this.mListener = mListener;
        nextOfKinRelationshipsRequest = new ArrayList<>();

        relationships = RelationshipsUtils.getRelationships();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog1) {
                BottomSheetDialog d = (BottomSheetDialog) dialog1;

                FrameLayout bottomSheet = (FrameLayout) d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        // Do something with your dialog like setContentView() or whatever
        return dialog;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.layout_bottom_sheet_next_of_kin, container, false);

        name = rootView.findViewById(R.id.edt_nextOfKinBottom_name);
        mobile = rootView.findViewById(R.id.edt_nextOfKinBottom_mobile);
        email = rootView.findViewById(R.id.edt_nextOfKinBottom_email);

        AutoCompleteTextView relationshipAutoComplete =
                rootView.findViewById(R.id.autocomplete_nextOfKinBottom_relationship);
        relationshipAutoComplete.setEnabled(false);

        name.requestFocus();

        String[] relationshipArray = new String[relationships.size()];

        for (int i = 0; i < relationships.size(); i++) {

            NextOfKinRelationship relationship = relationships.get(i);
            relationshipArray[i] = relationship.getName();

        }

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(
                        getContext(),
                        android.R.layout.simple_list_item_1,
                        relationshipArray);

        relationshipAutoComplete.setAdapter(adapter);
        relationshipAutoComplete.setText(relationshipArray[0], false);
        selectedRelationship = relationships.get(0);

        relationshipAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedRelationship = relationships.get(position);
            }
        });

        rootView.findViewById(R.id.btn_nextOfKinBottom_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValidDetails()) {

                    saveNextOfKin();

                }
            }
        });

        return rootView;
    }

    private void saveNextOfKin() {

        NextOfKinRequest request = new NextOfKinRequest();

        String[] username = name.getText().toString().split(" ");

        request.setFirstName(username[0]);
        request.setMiddleName(username[1]);

        if (username.length > 2) {
            request.setLastName(username[2]);

        } else {
            request.setLastName("...");
        }

        request.setEmail(email.getText().toString());
        request.setMobile(mobile.getText().toString());

        request.setRelationshipId(selectedRelationship.getId());

        nextOfKinRelationshipsRequest.add(request);

        mListener.onNextOfKinSaved(nextOfKinRelationshipsRequest);

        dismiss();

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            mListener = (AddNextOfKinListener) context;
        } catch (ClassCastException exception) {
            throw new ClassCastException(context.toString() +
                    "must implement listener");
        }
    }

    private boolean isValidDetails() {

        boolean isValid = true;

        if (name.getText().toString().trim().isEmpty()) {
            isValid = false;
            name.setError("Invalid name");
        }

        String[] username = name.getText().toString().split(" ");
        if (username.length < 1) {
            name.setError("Enter Full Names");
            isValid = false;
        }

        if (mobile.getText().toString().trim().isEmpty()) {
            mobile.setError("Mobile cannot be empty");
            isValid = false;
        }

        return isValid;

    }


    public interface AddNextOfKinListener {
        void onNextOfKinSaved(ArrayList<NextOfKinRequest> nextOfKins);
    }

}
