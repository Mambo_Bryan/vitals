package com.collabmedhealthcare.vitals.ui.views.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.ContactData;

/**
 * A simple {@link Fragment} subclass.
 */
public class GeneralContactTracingFragment extends Fragment implements View.OnClickListener {

    private ContactData contactData;
    private View rootView;

    private TextView contactName;
    private TextView contactMobile;
    private TextView contactPatient;
    private TextView contactLocation;
    private TextView contactTrackingEmail;
    private TextView contactTrackingComments;

    public GeneralContactTracingFragment(ContactData contactData) {
        // Required empty public constructor

        this.contactData = contactData;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_general_contact_trace, container, false);

        initViews();

        return rootView;
    }

    private void initViews() {

        contactPatient = rootView.findViewById(R.id.tv_traceContact_patient);
        contactMobile = rootView.findViewById(R.id.tv_traceConact_mobile);
        contactMobile.setOnClickListener(this);
        contactLocation = rootView.findViewById(R.id.tv_traceContact_location);
        contactLocation.setOnClickListener(this);
        contactTrackingEmail = rootView.findViewById(R.id.tv_traceContact_trackingEmail);
        contactTrackingEmail.setOnClickListener(this);
        contactTrackingComments = rootView.findViewById(R.id.tv_traceContact_tracingComments);

        updateContact();
    }

    private void updateContact() {

        contactPatient.setText(contactData.getPatient());
        contactMobile.setText(contactData.getMobile());
        contactLocation.setText(contactData.getLocation());

        if (contactData.getEmail() == null) {
            contactTrackingEmail.setText("No Email");
        } else {
            contactTrackingEmail.setText(contactData.getEmail());
        }

        if (contactData.getComments() == null) {
            contactTrackingComments.setText("No Comments Added");
        } else {
            contactTrackingComments.setText(contactData.getComments());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_traceConact_mobile:
                if (contactData.getMobile() != null) {
                    Toast.makeText(getContext(), "dialing contact", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Intent.ACTION_DIAL,
                            Uri.parse("tel:" + contactData.getMobile()));
                    startActivity(intent);
                }
                break;
            case R.id.tv_traceContact_location:
                Toast.makeText(getContext(), "geo location not set", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tv_traceContact_trackingEmail:

                if (contactData.getEmail() != null) {
                    String[] addresses = {contactData.getEmail(), contactData.getSecondaryEmail()};

                    Intent myIntent = new Intent(Intent.ACTION_SENDTO);
                    myIntent.setData(Uri.parse("mailto:")); // only email apps should handle this
                    myIntent.putExtra(Intent.EXTRA_EMAIL, addresses);
                    myIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                    if (myIntent.resolveActivity(getContext().getPackageManager()) != null) {
                        startActivity(myIntent);
                    }
                }
                break;

            default:
                break;
        }
    }
}
