package com.collabmedhealthcare.vitals.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.interfaces.OnLoadMoreListener;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.PatientLocationDetails;
import com.collabmedhealthcare.vitals.data.models.ProfilePhotoUtils;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;

import java.util.ArrayList;
import java.util.List;

public class ScreenedPatientsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private int visibleThreshold = 5;
    private int lastVisibleItem;
    private int totalItemCount;

    private OnLoadMoreListener loadMore;
    private boolean isLoading;


    private ArrayList<ScreenedPatient> allScreenedPatients = null;
    private ArrayList<ScreenedPatient> filteredScreenedPatients = null;
    private OnScreenedPatientListener onScreenedPatientListener;

    private int activityCode;

    public ScreenedPatientsRecyclerAdapter(RecyclerView recyclerView,
                                           ArrayList<ScreenedPatient> allScreenedPatients,
                                           OnScreenedPatientListener onScreenedPatientListener,
                                           int code) {
        this.allScreenedPatients = allScreenedPatients;
        this.filteredScreenedPatients = allScreenedPatients;
        this.onScreenedPatientListener = onScreenedPatientListener;

        activityCode = code;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View dataView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.layout_item_screened_patient, parent, false);
        return new ScreenedPatientsViewHolder(dataView, onScreenedPatientListener);

    }

    @Override
    public Filter getFilter() {
        return contactsFilter;
    }

    private Filter contactsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String queryString = constraint.toString().toLowerCase();

            if (queryString.isEmpty()) {
                filteredScreenedPatients = allScreenedPatients;
            } else {

                List<ScreenedPatient> filteredList = new ArrayList<>();

                for (ScreenedPatient screenedPatient : allScreenedPatients) {
                    if (screenedPatient.getFullName().toLowerCase().contains(queryString.trim())) {
                        filteredList.add(screenedPatient);
                    }
                }

                filteredScreenedPatients = new ArrayList<>(filteredList);
            }

            FilterResults results = new FilterResults();
            results.values = filteredScreenedPatients;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredScreenedPatients = ((ArrayList<ScreenedPatient>) results.values);
            notifyDataSetChanged();
        }
    };

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        ScreenedPatient patientContact = filteredScreenedPatients.get(position);
        ScreenedPatientsViewHolder viewHolder = (ScreenedPatientsViewHolder) holder;

        viewHolder.patientName.setText(patientContact.getFullName());
        PatientLocationDetails homeLocation = patientContact.getHomeLocation();
        if (homeLocation != null) {
            viewHolder.patientLocation.setText(homeLocation.getPlaceName());
        } else {
            viewHolder.patientLocation.setText("No Location Set");
        }

        viewHolder.patientMobileNumber.setText(patientContact.getMobile());
        viewHolder.patientNumber.setText(String.valueOf(patientContact.getId()));

        if (patientContact.getPatientTestResults().isEmpty()) {
            viewHolder.patientStatus.setText("pending");
            Glide.with(viewHolder.patientStatusImage.getContext())
                    .load(R.drawable.ic_hourglass_empty_24dp)
                    .centerCrop()
                    .into(viewHolder.patientStatusImage);
        } else {
            viewHolder.patientStatus.setText("tested");
            Glide.with(viewHolder.patientStatusImage.getContext())
                    .load(R.drawable.ic_check_24dp)
                    .centerCrop()
                    .into(viewHolder.patientStatusImage);
        }

        Glide.with(viewHolder.patientImage.getContext())
                .load(ProfilePhotoUtils.getProfileResourceByName(patientContact.getFirstName()))
                .centerCrop()
                .into(viewHolder.patientImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(holder.itemView.getContext(), holder.itemView);
//
//                if (activityCode == AppConstantsUtils.CODE_SCREENED_ACTIVITY) {
//                    //Creating the instance of PopupMenu
//                    //Inflating the Popup using xml file
//                    popup.getMenuInflater()
//                            .inflate(R.menu.menu_patient_options, popup.getMenu());
//                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                        @Override
//                        public boolean onMenuItemClick(MenuItem item) {
//
//                            switch (item.getItemId()) {
//
//                                case R.id.menu_item_collect_sample:
//
//                                    onScreenedPatientListener.onScreenedPatientClicked(R.id.menu_item_collect_sample, position);
//                                    break;
//
//                                case R.id.menu_item_request_sample:
//
//                                    onScreenedPatientListener.onScreenedPatientClicked(R.id.menu_item_request_sample, position);
//                                    break;
//
//                                default:
//                                    break;
//
//                            }
//
//                            return true;
//                        }
//                    });
//                    popup.show(); //showing popup menu
//                } else {
                onScreenedPatientListener.onScreenedPatientClicked(AppConstantsUtils.ERROR,
                        position);
//                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredScreenedPatients.size();
    }

    public class ScreenedPatientsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView patientImage;
        ImageView patientStatusImage;
        TextView patientName;
        TextView patientMobileNumber;
        TextView patientLocation;
        TextView patientStatus;
        TextView patientNumber;

        OnScreenedPatientListener onScreenedPatientListener;

        public ScreenedPatientsViewHolder(@NonNull View itemView, OnScreenedPatientListener onScreenedPatientListener) {
            super(itemView);

            patientImage = itemView.findViewById(R.id.iv_patientContact_image);
            patientStatusImage = itemView.findViewById(R.id.iv_patientContact_status);
            patientName = itemView.findViewById(R.id.tv_patientContact_name);
            patientMobileNumber = itemView.findViewById(R.id.tv_patientContact_mobile);
            patientLocation = itemView.findViewById(R.id.tv_patientContact_location);
            patientStatus = itemView.findViewById(R.id.tv_patientContact_status);
            patientNumber = itemView.findViewById(R.id.tv_patientContact_number);

            this.onScreenedPatientListener = onScreenedPatientListener;

        }

        @Override
        public void onClick(View v) {

        }
    }

    public interface OnScreenedPatientListener {
        void onScreenedPatientClicked(int menuChoice, int position);
    }
}
