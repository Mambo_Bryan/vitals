package com.collabmedhealthcare.vitals.ui.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerDragListener, GoogleMap.OnMapLongClickListener {

    private GoogleMap mMap;
    private Button btnSetLocation;
    private LatLng activityLocation;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Map");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        btnSetLocation = findViewById(R.id.btn_mapLocation);
        btnSetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (activityLocation != null) {
                    Intent intent = getIntent();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(AppConstantsUtils.GOOGLE_MAPS_LOCATION_EXTRA, activityLocation);
                    intent.putExtra(AppConstantsUtils.GOOGLE_MAPS_BUNDLE_EXTRA, bundle);
                    setResult(RESULT_OK, intent);
                    finish();

                } else {

                    Toast.makeText(MapsActivity.this, "long press map to set location", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLongClickListener(this);
        // Add a marker in Sydney and move the camera
        double latitude = -1.279275;
        double longitude = 36.770232;

        LatLng lavington = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(lavington).title("Marker in Lavington"))
                .setDraggable(true);
        mMap.setOnMarkerDragListener(this);

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        setResult(RESULT_CANCELED, intent);
        super.onBackPressed();
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {
        btnSetLocation.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        btnSetLocation.setVisibility(View.VISIBLE);
        activityLocation = marker.getPosition();
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(latLng).title("Location set"))
                .setDraggable(true);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12.0f));
        activityLocation = latLng;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
