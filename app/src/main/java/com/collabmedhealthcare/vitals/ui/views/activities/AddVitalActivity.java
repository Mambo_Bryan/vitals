package com.collabmedhealthcare.vitals.ui.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.request.VitalRequestBody;
import com.collabmedhealthcare.vitals.data.models.response.VitalResponseBody;
import com.collabmedhealthcare.vitals.ui.views.LoadingDialog;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddVitalActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private LoadingDialog loadingDialog;
    private SharedPrefsUtil prefsUtil;
    private ApiService apiService;
    private EditText patientTemperature;
    private EditText patientVisits;

    private ArrayList<String> symptoms = new ArrayList<>();
    private LinearLayout accountSelectionLinearLayout;
    private Spinner accountSpinner;
    private ScreenedPatient selectedPatient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vital);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Patient's Vitals");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        Intent myIntent = getIntent();

        Gson gson = new Gson();
        String patientJson = myIntent.getStringExtra(AppConstantsUtils.SCREENED_PATIENT_ACTIVITY_EXTRA);
        selectedPatient = gson.fromJson(patientJson, ScreenedPatient.class);

        initViews();
    }

    private void initViews() {

        prefsUtil = SharedPrefsUtil.getInstance(this);

        patientTemperature = findViewById(R.id.edt_patient_temperature);
        patientVisits = findViewById(R.id.edt_patient_visits);

        CheckBox dryCough = findViewById(R.id.checkbox_symptoms_dry_cough);
        dryCough.setOnCheckedChangeListener(this);
        CheckBox fever = findViewById(R.id.checkbox_symptoms_fever);
        fever.setOnCheckedChangeListener(this);
        CheckBox tiredness = findViewById(R.id.checkbox_symptoms_tiredness);
        tiredness.setOnCheckedChangeListener(this);
        CheckBox shortBreaths = findViewById(R.id.checkbox_symptoms_breaths);
        shortBreaths.setOnCheckedChangeListener(this);
        CheckBox lostTaste = findViewById(R.id.checkbox_symptoms_taste);
        lostTaste.setOnCheckedChangeListener(this);

        loadingDialog = new LoadingDialog(this);
        apiService = ApiClient.createService(ApiService.class);

        accountSelectionLinearLayout = findViewById(R.id.linearLayout_add_vitals_account);
        accountSpinner = findViewById(R.id.spinner_add_vitals_account);

        findViewById(R.id.btn_save).setOnClickListener(this);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_save) {

            if (isValidPasswordCredentials()) {
                loadingDialog.showDialog();

                uploadVitalToServer();

            } else {

                Toast.makeText(this, "fill correct details", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void uploadVitalToServer() {


        VitalRequestBody requestBody = new VitalRequestBody();

        requestBody.setPatientId(selectedPatient.getId());
        requestBody.setTemprature(Double.valueOf(patientTemperature.getText().toString()));
        requestBody.setSymptoms(symptoms);

        Call<VitalResponseBody> call = apiService.sendVitalsToServer(prefsUtil.getUserAccessToken(),
                requestBody);

        call.enqueue(new Callback<VitalResponseBody>() {
            @Override
            public void onResponse(Call<VitalResponseBody> call, Response<VitalResponseBody> response) {

                if (response.isSuccessful() && response.body() != null && response.body().getVital() != null) {

                    onBackPressed();
                    Toast.makeText(AddVitalActivity.this, "Saved Successfully", Toast.LENGTH_SHORT).show();
                    return;
                }

                loadingDialog.hideDialog();
                Toast.makeText(AddVitalActivity.this, "Invalid vital details", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<VitalResponseBody> call, Throwable t) {
                loadingDialog.hideDialog();
                Toast.makeText(AddVitalActivity.this, "couldn't connect", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean isValidPasswordCredentials() {

        boolean isValid = true;

        if (patientTemperature.getText().toString().isEmpty()) {
            patientTemperature.setError("Invalid Temperature");
            isValid = false;
        }

        if (patientVisits.getText().toString().isEmpty()) {
            patientVisits.setError("Invalid Visit");
            isValid = false;
        }

        return isValid;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()) {

            case R.id.checkbox_symptoms_dry_cough:
                if (isChecked) {
                    symptoms.add("dry cough");
                } else {
                    symptoms.remove("dry cough");
                }
                break;

            case R.id.checkbox_symptoms_fever:
                if (isChecked) {
                    symptoms.add("fever");
                } else {
                    symptoms.remove("fever");
                }
                break;

            case R.id.checkbox_symptoms_tiredness:
                if (isChecked) {
                    symptoms.add("tiredness");
                } else {
                    symptoms.remove("tiredness");
                }
                break;

            case R.id.checkbox_symptoms_breaths:
                if (isChecked) {
                    symptoms.add("short breathes");
                } else {
                    symptoms.remove("short breathes");
                }
                break;

            case R.id.checkbox_symptoms_taste:
                if (isChecked) {
                    symptoms.add("loss of taste");
                } else {
                    symptoms.remove("loss of taste");
                }
                break;

            default:
                break;

        }

    }

}