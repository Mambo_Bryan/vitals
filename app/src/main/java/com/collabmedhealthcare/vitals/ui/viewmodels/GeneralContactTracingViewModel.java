package com.collabmedhealthcare.vitals.ui.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class GeneralContactTracingViewModel extends ViewModel {

    private MutableLiveData<String> searchQuery = new MutableLiveData<>();
    private MutableLiveData<Boolean> isSearching = new MutableLiveData<>();

    public LiveData<String> getSearchQuery() {
        if (searchQuery == null) {
            searchQuery = new MutableLiveData<>();
        }
        return searchQuery;
    }

    public LiveData<Boolean> getIsSearching() {
        if (isSearching == null) {
            isSearching = new MutableLiveData<>();
        }
        return isSearching;
    }

    public void setSearchQuery(String query) {
        searchQuery.setValue(query);
    }

    public void setIsSearching(boolean isSearching) {
        this.isSearching.setValue(isSearching);
    }
}
