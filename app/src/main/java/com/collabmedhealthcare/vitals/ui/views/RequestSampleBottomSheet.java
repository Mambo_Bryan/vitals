package com.collabmedhealthcare.vitals.ui.views;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.objects.SampleType;
import com.collabmedhealthcare.vitals.data.models.request.RequestSampleBody;
import com.collabmedhealthcare.vitals.data.models.response.DefaultMessageResponse;
import com.collabmedhealthcare.vitals.utils.SampleTypeUtils;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestSampleBottomSheet extends BottomSheetDialogFragment {

    private EditText otherDetails;
    private ScreenedPatient patient;
    private ProgressBar progressBar;
    private Button button;

    private OnSampleRequestListener mListener;

    private SampleType selectedSampleType;
    private ArrayList<SampleType> sampleTypes;

    public RequestSampleBottomSheet(ScreenedPatient patient) {
        this.patient = patient;
    }

    public RequestSampleBottomSheet(ScreenedPatient patient, OnSampleRequestListener mListener) {
        this.patient = patient;
        this.mListener = mListener;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog1) {
                BottomSheetDialog d = (BottomSheetDialog) dialog1;

                FrameLayout bottomSheet = (FrameLayout) d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        // Do something with your dialog like setContentView() or whatever
        return dialog;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.layout_bottom_sheet_request_sample, container, false);

        otherDetails = rootView.findViewById(R.id.edt_bottomSheet_sample_request_details);

        AutoCompleteTextView sampleTypeAutoComplete =
                rootView.findViewById(R.id.autoComplete_bottomSheet_sample_request);
        sampleTypeAutoComplete.setEnabled(false);

        otherDetails.requestFocus();

        sampleTypes = SampleTypeUtils.getSampleTypes();

        String[] sampleTypeArray = new String[sampleTypes.size()];

        for (int i = 0; i < sampleTypes.size(); i++) {

            SampleType sampleType = sampleTypes.get(i);
            sampleTypeArray[i] = sampleType.getSampleName();

        }

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(
                        getContext(),
                        android.R.layout.simple_list_item_1,
                        sampleTypeArray);

        sampleTypeAutoComplete.setAdapter(adapter);
        sampleTypeAutoComplete.setText(sampleTypeArray[0], false);
        selectedSampleType = sampleTypes.get(0);

        sampleTypeAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedSampleType = sampleTypes.get(position);
            }
        });

        progressBar = rootView.findViewById(R.id.progressBar_bottomSheet_sample_request);
        button = rootView.findViewById(R.id.btn_bottomSheet_sample_request);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showLoadingContact();
                requestSampleCollection();

            }
        });

        return rootView;
    }

    private void requestSampleCollection() {

        RequestSampleBody body = new RequestSampleBody();

        body.setOtherDetails(otherDetails.getText().toString());
        body.setSampleType(selectedSampleType.getSampleId());

        SharedPrefsUtil prefsUtil = SharedPrefsUtil.getInstance(getContext());
        ApiService apiService = ApiClient.createService(ApiService.class);

        Call<DefaultMessageResponse> call = apiService.requestSampleCollection(
                prefsUtil.getUserAccessToken(),
                patient.getId(),
                body
        );

        call.enqueue(new Callback<DefaultMessageResponse>() {
            @Override
            public void onResponse(Call<DefaultMessageResponse> call, Response<DefaultMessageResponse> response) {

                if (response.isSuccessful() && response.body() != null) {

                    Toast.makeText(getContext(), "request successful", Toast.LENGTH_SHORT).show();

                    if (mListener != null) {
                        mListener.onRequestSentSuccess(true);
                    }
                    dismiss();
                    return;

                }

                showErrorContact();

            }

            @Override
            public void onFailure(Call<DefaultMessageResponse> call, Throwable t) {
                showErrorContact();
            }
        });

    }

    private void showLoadingContact() {

        otherDetails.setEnabled(false);
        button.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);

    }

    private void showErrorContact() {

        otherDetails.setEnabled(true);
        progressBar.setVisibility(View.INVISIBLE);
        button.setBackgroundColor(getResources().getColor(R.color.quantum_googred));
        button.setText("retry");
        button.setVisibility(View.VISIBLE);

    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        if (mListener != null) {
            mListener.onRequestSentSuccess(false);
        }
        super.onDismiss(dialog);
    }

    public interface OnSampleRequestListener {
        void onRequestSentSuccess(boolean isSuccessful);
    }

}
