package com.collabmedhealthcare.vitals.ui.views.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.utils.ListViewUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class SpecificContactTracingNotesFragment extends Fragment implements View.OnClickListener {

    private View rootView;
    private ListViewUtils tracingViews;
    private RecyclerView recyclerView;

    private SharedPrefsUtil prefsUtil;
    private ApiService apiService;

    public SpecificContactTracingNotesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_level_one_contact_list, container, false);

        initRecyclerCompleteViews();

        return rootView;
    }

    private void initRecyclerCompleteViews() {

        prefsUtil = SharedPrefsUtil.getInstance(getContext());
        apiService = ApiClient.createService(ApiService.class);

        View completeItemViews = rootView.findViewById(R.id.include_template_fragment_list);

        View loading = completeItemViews.findViewById(R.id.include_template_loading);
        View noData = completeItemViews.findViewById(R.id.include_template_no_data);
        View error = completeItemViews.findViewById(R.id.include_template_error);
        View data = completeItemViews.findViewById(R.id.include_template_data);

        tracingViews = new ListViewUtils(
                loading,
                noData,
                data,
                error,
                this
        );

        tracingViews.setNoDataMessage("This is an upcoming feature in the next release.");
        recyclerView = data.findViewById(R.id.rv_template_data);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                tracingViews.showNoData();
            }
        }, 1500);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_template_retry) {
            //TODO add method to reload user notes
        }

    }
}
