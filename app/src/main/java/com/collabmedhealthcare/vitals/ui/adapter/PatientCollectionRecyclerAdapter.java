package com.collabmedhealthcare.vitals.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.DummyPatient;

import java.util.ArrayList;

public class PatientCollectionRecyclerAdapter extends RecyclerView.Adapter<PatientCollectionRecyclerAdapter.CollectionViewHolder> {

    ArrayList<DummyPatient> dummyPatients;
    OnCollectSampleListener onCollectSampleListener;

    public PatientCollectionRecyclerAdapter(ArrayList<DummyPatient> dummyPatients, OnCollectSampleListener onCollectSampleListener) {
        this.dummyPatients = dummyPatients;
        this.onCollectSampleListener = onCollectSampleListener;
    }

    @NonNull
    @Override
    public CollectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_collect_patient, parent, false);
        return new CollectionViewHolder(view, onCollectSampleListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CollectionViewHolder holder, int position) {
        holder.patientName.setText(dummyPatients.get(position).getName());
        holder.pateintHome.setText(String.valueOf(dummyPatients.get(position).getHome()));
        holder.patientTested.setText(dummyPatients.get(position).getTestedResult());
        holder.patientTraced.setText(dummyPatients.get(position).getTracedStatus());

        int remainder = position % 2;
        if (remainder == 0) {
            holder.backgroundImageview.setBackgroundColor(holder.backgroundImageview.getResources().getColor(R.color.white1));
        } else {
            holder.backgroundImageview.setBackgroundColor(holder.backgroundImageview.getResources().getColor(R.color.white2));
        }

    }

    @Override
    public int getItemCount() {
        return dummyPatients.size();
    }

    class CollectionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView patientName;
        TextView pateintHome;
        TextView patientTested;
        TextView patientTraced;

        ImageView backgroundImageview;
        Button collect;

        OnCollectSampleListener onCollectSampleListener;

        public CollectionViewHolder(@NonNull View itemView, OnCollectSampleListener onCollectSampleListener) {
            super(itemView);

            patientName = itemView.findViewById(R.id.tv_collect_name);
            pateintHome = itemView.findViewById(R.id.tv_collect_home);
            patientTested = itemView.findViewById(R.id.tv_collect_tested);
            patientTraced = itemView.findViewById(R.id.tv_collect_traced);

            backgroundImageview = itemView.findViewById(R.id.iv_collect_bg);
            collect = itemView.findViewById(R.id.collect_sample);
            collect.setOnClickListener(this::onClick);

            this.onCollectSampleListener = onCollectSampleListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onCollectSampleListener.onCollectionClicked(getAdapterPosition());
        }
    }

    public interface OnCollectSampleListener {
        void onCollectionClicked(int position);
    }
}
