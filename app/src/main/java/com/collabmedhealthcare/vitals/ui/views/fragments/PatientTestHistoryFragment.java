package com.collabmedhealthcare.vitals.ui.views.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.interfaces.RecyclerItemListener;
import com.collabmedhealthcare.vitals.data.models.PatientTestResult;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.collabmedhealthcare.vitals.ui.adapter.PatientTestsRecyclerAdapter;
import com.collabmedhealthcare.vitals.ui.viewmodels.ScreenedPatientViewModel;
import com.collabmedhealthcare.vitals.utils.ListViewUtils;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PatientTestHistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PatientTestHistoryFragment extends Fragment implements View.OnClickListener, RecyclerItemListener {

    private View rootView;
    private ListViewUtils tracingViews;
    private XRecyclerView recyclerView;

    private ScreenedPatient currentPatient;
    private ArrayList<PatientTestResult> results;
    private PatientTestsRecyclerAdapter adapter;

    public PatientTestHistoryFragment() {
        // Required empty public constructor
    }

    public static PatientTestHistoryFragment newInstance(String param1, String param2) {
        PatientTestHistoryFragment fragment = new PatientTestHistoryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_level_one_contact_list, container, false);

        initRecyclerCompleteViews();

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ScreenedPatientViewModel viewModel = new ViewModelProvider(requireActivity()).get(ScreenedPatientViewModel.class);
        viewModel.getScreenedPatient().observe(getViewLifecycleOwner(), new Observer<ScreenedPatient>() {
            @Override
            public void onChanged(ScreenedPatient patient) {
                if (patient != null) {

                    currentPatient = patient;

                    if (currentPatient.getPatientTestResults() != null && !currentPatient.getPatientTestResults().isEmpty()) {

                        results = new ArrayList<>(currentPatient.getPatientTestResults());

                        if (!results.isEmpty()) {
                            initRecyclerViews();
                            tracingViews.showContent();
                            return;
                        }
                    }

                    tracingViews.showNoData();

                }
            }
        });
    }

    private void initRecyclerViews() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new PatientTestsRecyclerAdapter(results, this);

        recyclerView.setAdapter(adapter);

        recyclerView.setPullRefreshEnabled(false);
        recyclerView.setLoadingMoreEnabled(false);
    }


    private void initRecyclerCompleteViews() {

        View completeItemViews = rootView.findViewById(R.id.include_template_fragment_list);

        View loading = completeItemViews.findViewById(R.id.include_template_loading);
        View noData = completeItemViews.findViewById(R.id.include_template_no_data);
        View error = completeItemViews.findViewById(R.id.include_template_error);
        View data = completeItemViews.findViewById(R.id.include_template_data);

        tracingViews = new ListViewUtils(
                loading,
                noData,
                data,
                error,
                this
        );

        tracingViews.setNoDataMessage("Patient hasn't been tested yet!");
        recyclerView = data.findViewById(R.id.rv_template_data);

        tracingViews.showLoading();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_template_retry) {
            //TODO add method to reload user notes
        }
    }

    @Override
    public void onItemSelected(int position) {

    }
}