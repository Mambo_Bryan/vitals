package com.collabmedhealthcare.vitals.ui.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.ContactTracingDetailsData;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.response.ContactTraceDetailsResponse;
import com.collabmedhealthcare.vitals.ui.adapter.ScreenedPatientsRecyclerAdapter;
import com.collabmedhealthcare.vitals.ui.viewmodels.GeneralContactTracingViewModel;
import com.collabmedhealthcare.vitals.ui.views.activities.SpecificTracingActivity;
import com.collabmedhealthcare.vitals.utils.ListViewUtils;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class GeneralContactTracingListFragment extends Fragment implements View.OnClickListener, ScreenedPatientsRecyclerAdapter.OnScreenedPatientListener {

    private List<ContactTracingDetailsData> contactDetails;
    private ArrayList<ScreenedPatient> screenedPatients;
    private ScreenedPatientsRecyclerAdapter adapter;

    private View rootView;
    private ListViewUtils tracingViews;
    private XRecyclerView recyclerView;

    private int pageNumber;
    private int totalPageNumber;

    private int contactId;
    private SharedPrefsUtil prefsUtil;
    private ApiService apiService;

    private ContactTraceDetailsResponse contactTraceDetailsResponse;

    public GeneralContactTracingListFragment(int contactId) {
        // Required empty public constructor

        this.contactId = contactId;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_level_one_contact_list, container, false);

        initRecyclerCompleteViews();

        pageNumber = 1;

        loadContractTracingList();

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        GeneralContactTracingViewModel viewModel = new ViewModelProvider(requireActivity()).get(GeneralContactTracingViewModel.class);
        viewModel.getSearchQuery().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null) {
                    if (adapter != null) {
                        adapter.getFilter().filter(s);
                    }
                }
            }
        });

        viewModel.getIsSearching().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    String query = viewModel.getSearchQuery().getValue();
                    loadSearchedContractTracingList(query);
                }
            }
        });
    }

    private void initRecyclerCompleteViews() {

        prefsUtil = SharedPrefsUtil.getInstance(getContext());
        apiService = ApiClient.createService(ApiService.class);

        View completeItemViews = rootView.findViewById(R.id.include_template_fragment_list);

        View loading = completeItemViews.findViewById(R.id.include_template_loading);
        View noData = completeItemViews.findViewById(R.id.include_template_no_data);
        View error = completeItemViews.findViewById(R.id.include_template_error);
        View data = completeItemViews.findViewById(R.id.include_template_data);

        tracingViews = new ListViewUtils(
                loading,
                noData,
                data,
                error,
                this
        );

        tracingViews.setNoDataMessage("There are no contacts to be traced. Please add one below");
        recyclerView = data.findViewById(R.id.rv_template_data);

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_template_retry) {

            loadContractTracingList();
            tracingViews.showLoading();

        }

    }

    private void loadContractTracingList() {

        Call<ContactTraceDetailsResponse> call = apiService.getSpecificContactTracingDetails(
                prefsUtil.getUserAccessToken(),
                contactId, pageNumber);

        call.enqueue(new Callback<ContactTraceDetailsResponse>() {
            @Override
            public void onResponse(Call<ContactTraceDetailsResponse> call,
                                   Response<ContactTraceDetailsResponse> response) {

                if (response.isSuccessful() && response.body() != null) {

                    contactTraceDetailsResponse = response.body();
                    contactDetails = response.body().getData();

                    totalPageNumber = response.body().getMeta().getLastPage();

                    if (pageNumber == 1) {

                        if (!response.body().getData().isEmpty()) {

                            initRecycler();
                            tracingViews.showContent();
                            recyclerView.refreshComplete();

                        } else {
                            tracingViews.showNoData();
                        }

                    } else {

                        updateRecycler();

                    }

                    return;
                }

                tracingViews.showError();

            }

            @Override
            public void onFailure(Call<ContactTraceDetailsResponse> call, Throwable t) {
                tracingViews.showError();
            }
        });
    }

    private void loadSearchedContractTracingList(String query) {

        Call<ContactTraceDetailsResponse> call = apiService.getSearchedSpecificContactTracingDetails(
                prefsUtil.getUserAccessToken(),
                contactId, query, pageNumber);

        call.enqueue(new Callback<ContactTraceDetailsResponse>() {
            @Override
            public void onResponse(Call<ContactTraceDetailsResponse> call,
                                   Response<ContactTraceDetailsResponse> response) {

                if (response.isSuccessful() && response.body() != null) {

                    contactTraceDetailsResponse = response.body();
                    contactDetails = response.body().getData();

                    totalPageNumber = response.body().getMeta().getLastPage();

                    if (pageNumber == 1) {

                        if (!response.body().getData().isEmpty()) {

                            initRecycler();
                            tracingViews.showContent();
                            recyclerView.refreshComplete();

                        } else {
                            tracingViews.showNoData();
                        }

                    } else {

                        updateRecycler();

                    }

                    return;
                }

                tracingViews.showError();

            }

            @Override
            public void onFailure(Call<ContactTraceDetailsResponse> call, Throwable t) {
                tracingViews.showError();
            }
        });
    }

    private void initRecycler() {

        screenedPatients = new ArrayList<>();

        for (ContactTracingDetailsData data : contactDetails) {
            screenedPatients.add(data.getPatient());
        }
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        adapter = new ScreenedPatientsRecyclerAdapter(recyclerView, screenedPatients, this,
                AppConstantsUtils.CODE_TRACING_ACTIVITY);

        recyclerView.setAdapter(adapter);

        recyclerView.setPullRefreshEnabled(true);
        recyclerView.setLoadingMoreEnabled(true);

        recyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                pageNumber = 1;
                loadContractTracingList();
            }

            @Override
            public void onLoadMore() {

                pageNumber++;
                loadContractTracingList();

            }
        });
    }

    private void updateRecycler() {

        if (contactDetails != null) {

            for (ContactTracingDetailsData data : contactDetails) {
                screenedPatients.add(data.getPatient());
            }

            adapter.notifyDataSetChanged();

        }

        if (pageNumber >= totalPageNumber)
            recyclerView.setLoadingMoreEnabled(false);

    }

    @Override
    public void onScreenedPatientClicked(int menuChoice, int position) {

        ScreenedPatient patient = screenedPatients.get(position);

        Intent myIntent = new Intent(getContext(), SpecificTracingActivity.class);
        myIntent.putExtra(AppConstantsUtils.SPECIFIC_TRACE_CONTACT_EXTRA, patient);
        startActivity(myIntent);

    }

}
