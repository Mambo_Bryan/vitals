package com.collabmedhealthcare.vitals.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.PatientLocationDetails;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;

import java.util.ArrayList;
import java.util.List;

public class ScreenedPatientAdapter extends ArrayAdapter<ScreenedPatient> {

    private Context mContext;
    private int resourceId;
    private List<ScreenedPatient> patients, tempPatients, suggestedPatients;


    public ScreenedPatientAdapter(@NonNull Context context, int resource, @NonNull List<ScreenedPatient> objects) {
        super(context, resource, objects);

        this.patients = objects;
        this.mContext = context;
        this.resourceId = resource;
        tempPatients = new ArrayList<>(objects);
        suggestedPatients = new ArrayList<>();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;

        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                view = inflater.inflate(resourceId, parent, false);
            }
            ScreenedPatient patient = getItem(position);

            TextView name = view.findViewById(R.id.tv_screenedItem_name);
            name.setText(patient.getFullName());

            TextView home = view.findViewById(R.id.tv_screenedItem_homeName);

            PatientLocationDetails homeLocation = patient.getHomeLocation();

            if (homeLocation != null) {
                home.setText(homeLocation.getPlaceName());
            } else {
                home.setText("No Location Set");
            }

            TextView mobile = view.findViewById(R.id.tv_sceenedItem_mobile);
            mobile.setText(patient.getMobile());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Nullable
    @Override
    public ScreenedPatient getItem(int position) {
        return patients.get(position);
    }

    @Override
    public int getCount() {
        return patients.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return patientsFilter;
    }

    private Filter patientsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestedPatients.clear();
                for (ScreenedPatient fruit : tempPatients) {
                    if (fruit.getFullName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestedPatients.add(fruit);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestedPatients;
                filterResults.count = suggestedPatients.size();

                return filterResults;

            } else {

                return new FilterResults();

            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<ScreenedPatient> tempValues = (ArrayList<ScreenedPatient>) results.values;
            if (results != null && results.count > 0) {
                clear();

                for (ScreenedPatient patient : tempValues) {
                    add(patient);
                }
                notifyDataSetChanged();
            } else {
                clear();
                notifyDataSetChanged();
            }

        }
    };

}
