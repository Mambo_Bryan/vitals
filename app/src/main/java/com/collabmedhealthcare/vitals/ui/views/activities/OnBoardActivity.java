package com.collabmedhealthcare.vitals.ui.views.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;

public class OnBoardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_board);

        Spinner accountSpinner = findViewById(R.id.spinner_onBoard);
        String[] items = AppConstantsUtils.accountTypes;
        //create an adapter to describe how the items are displayed, adapters are used in several places in android.
        // There are multiple variations of this, but this is the basic variant.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        //set the spinners adapter to the previously created one.
        accountSpinner.setAdapter(adapter);
        accountSpinner.setSelection(0);
        accountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String account = null;

                switch (position) {
                    case 0:
                        account = items[0];
                        break;

                    case 1:
                        account = items[1];
                        break;

                    default:
                        break;
                }

                Intent myIntent = new Intent(OnBoardActivity.this, LoginActivity.class);
                myIntent.putExtra(AppConstantsUtils
                .ON_BOARD_ACCOUNT_TYPE_EXTRA, account);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(OnBoardActivity.this, "select account", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
