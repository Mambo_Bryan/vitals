package com.collabmedhealthcare.vitals.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.ContactData;
import com.collabmedhealthcare.vitals.data.models.ProfilePhotoUtils;

import java.util.ArrayList;
import java.util.List;

public class ContactTracingRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private ArrayList<ContactData> allContactData;
    private ArrayList<ContactData> filteredContactData;
    private OnContactTracingListener onContactTracingListener;

    public ContactTracingRecyclerAdapter(ArrayList<ContactData> allContactData, OnContactTracingListener onContactTracingListener) {
        this.allContactData = allContactData;
        this.filteredContactData = new ArrayList<>(allContactData);
        this.onContactTracingListener = onContactTracingListener;
    }

    @Override
    public Filter getFilter() {
        return contactsFilter;
    }

    private Filter contactsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String queryString = constraint.toString().toLowerCase();

            if (queryString.isEmpty()) {
                filteredContactData = allContactData;
            } else {

                List<ContactData> filteredList = new ArrayList<>();

                for (ContactData contact : allContactData) {
                    if (contact.getContact() != null) {
                        if (contact.getContact().toLowerCase().contains(queryString.trim())
                                || contact.getPatient().toLowerCase().contains(queryString.trim())) {
                            filteredList.add(contact);
                        }
                    }
                }

                filteredContactData = new ArrayList<>(filteredList);
            }

            FilterResults results = new FilterResults();
            results.values = filteredContactData;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredContactData = ((ArrayList<ContactData>) results.values);
            notifyDataSetChanged();
        }
    };

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View dataView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.layout_contact_trace, parent, false);

        return new ContactTracingViewHolder(dataView, onContactTracingListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ContactData contactData = filteredContactData.get(position);
        ContactTracingViewHolder viewHolder = (ContactTracingViewHolder) holder;

        viewHolder.traceContactName.setText(contactData.getContact());
        viewHolder.traceContactMobileNumber.setText(contactData.getMobile());
        viewHolder.traceContactLocation.setText(contactData.getLocation());
        String patientName = contactData.getPatient();

        if (patientName != null) {
            viewHolder.traceContactPatient.setText(patientName);
        } else {
            viewHolder.traceContactPatient.setText("No associated patient");
        }

        Glide.with(viewHolder.traceContactImage.getContext())
                .load(ProfilePhotoUtils.getProfileResourceByName(contactData.getContact()))
                .centerCrop()
                .into(viewHolder.traceContactImage);

    }

    @Override
    public int getItemCount() {
        return filteredContactData.size();
    }

    public class ContactTracingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView traceContactImage;

        TextView traceContactName;
        TextView traceContactMobileNumber;
        TextView traceContactPatient;
        TextView traceContactLocation;

        OnContactTracingListener onContactTracingListener;

        public ContactTracingViewHolder(@NonNull View itemView, OnContactTracingListener onContactTracingListener) {
            super(itemView);

            traceContactImage = itemView.findViewById(R.id.iv_traceContact_image);
            traceContactName = itemView.findViewById(R.id.tv_traceContact_name);
            traceContactMobileNumber = itemView.findViewById(R.id.tv_traceContact_mobile);
            traceContactLocation = itemView.findViewById(R.id.tv_traceContact_location);
            traceContactPatient = itemView.findViewById(R.id.tv_traceContact_patient);

            this.onContactTracingListener = onContactTracingListener;
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onContactTracingListener.onContactTrackingClicked(getAdapterPosition());
        }
    }

    public interface OnContactTracingListener {
        void onContactTrackingClicked(int position);
    }
}
