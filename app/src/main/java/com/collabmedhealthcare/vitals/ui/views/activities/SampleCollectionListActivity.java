package com.collabmedhealthcare.vitals.ui.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.SampleRequest;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.response.SampleRequestsResponse;
import com.collabmedhealthcare.vitals.ui.adapter.SampleRequestRecyclerAdapter;
import com.collabmedhealthcare.vitals.utils.ListViewUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SampleCollectionListActivity extends AppCompatActivity implements SampleRequestRecyclerAdapter.OnSampleRequestListener, View.OnClickListener, SearchView.OnQueryTextListener {

    //variables
    ArrayList<SampleRequest> sampleRequests;
    SampleRequestRecyclerAdapter adapter;

    //
    ApiService apiService;
    String token;

    //layout items
    private XRecyclerView recyclerView;
    private ListViewUtils mainViews;

    private SearchView searchView;
    private String searchQuery;

    private int pageNumber;
    private int totalPageNumber;
    private boolean isUserSearching = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template_recycler);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Collection Requests");

        initViews();

        pageNumber = 1;

        mainViews.showLoading();

        getSampleRequests(pageNumber);
    }

    private void initRecycler(SampleRequestsResponse body) {

        sampleRequests = new ArrayList<>(body.getSampleRequests());

        adapter = new SampleRequestRecyclerAdapter(sampleRequests, this);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView.setPullRefreshEnabled(true);
        recyclerView.setLoadingMoreEnabled(true);
//        recyclerView.setLimitNumberToCallLoadMore(screenedPatients.size() - 2);

        recyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                pageNumber = 1;
                getSampleRequests(pageNumber);
            }

            @Override
            public void onLoadMore() {
                pageNumber++;

                if (isUserSearching) {
                    getSearchedSampleRequests(pageNumber);
                } else {
                    getSampleRequests(pageNumber);
                }
            }
        });

    }

    private void updateRecycler(SampleRequestsResponse patientsResponse) {

        if (patientsResponse != null && patientsResponse.getSampleRequests() != null) {

            sampleRequests.addAll(patientsResponse.getSampleRequests());
            adapter.notifyDataSetChanged();

        }

        if (pageNumber >= totalPageNumber)
            recyclerView.setLoadingMoreEnabled(false);

    }

    private void initViews() {

        SharedPrefsUtil prefsUtil = SharedPrefsUtil.getInstance(this);

        apiService = ApiClient.createService(ApiService.class);
        token = prefsUtil.getUserAccessToken();

        FloatingActionButton createNewRequest = findViewById(R.id.fab_templateRecyler);
        createNewRequest.hide();

        initRecyclerCompleteViews();
    }

    private void initRecyclerCompleteViews() {
        View completeItemViews = findViewById(R.id.inlcude_templateRecycler_complete);

        View loading = completeItemViews.findViewById(R.id.include_template_loading);
        View noData = completeItemViews.findViewById(R.id.include_template_no_data);
        View error = completeItemViews.findViewById(R.id.include_template_error);
        View data = completeItemViews.findViewById(R.id.include_template_data);

        mainViews = new ListViewUtils(
                loading,
                noData,
                data,
                error,
                this
        );

        mainViews.setNoDataMessage("There are no sample collection requests ");
        recyclerView = data.findViewById(R.id.rv_template_data);

    }

    @Override
    public void onSampleRequestClicked(int position) {

        SampleRequest request = sampleRequests.get(position - 1);

        Gson gson = new Gson();
        String data = gson.toJson(request);

        Intent myIntent = new Intent(this, SampleCollectionActivity.class);
        myIntent.putExtra(AppConstantsUtils.SAMPLE_COLLECTION_ACTIVITY_CODE_EXTRA, AppConstantsUtils.CODE_SAMPLE_LIST_ACTIVITY);
        myIntent.putExtra(AppConstantsUtils.SAMPLE_COLLECTION_ACTIVITY_EXTRA, data);

        startActivity(myIntent);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem mSearchMenuItem = menu.findItem(R.id.menu_item_search);
        searchView = (SearchView) mSearchMenuItem.getActionView();
        searchView.setQueryHint("Search Patient");
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    protected void onResume() {
        getSampleRequests(1);
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {

            pageNumber = 1;
            isUserSearching = false;
            searchQuery = null;

            searchView.setIconified(true);
            mainViews.showLoading();
            getSampleRequests(pageNumber);

        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_template_retry) {
            getSampleRequests(pageNumber);
            mainViews.showLoading();
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchQuery = query;

        if (!query.isEmpty()) {

            isUserSearching = true;
            pageNumber = 1;

            mainViews.showLoading();
            getSearchedSampleRequests(pageNumber);
        }

        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (sampleRequests != null && !sampleRequests.isEmpty()) {
            adapter.getFilter().filter(newText);
        }
        return true;
    }

    private void getSampleRequests(int page) {

        Call<SampleRequestsResponse> call = apiService.getSampleRequests(token, page);
        call.enqueue(new Callback<SampleRequestsResponse>() {
            @Override
            public void onResponse(Call<SampleRequestsResponse> call, Response<SampleRequestsResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    totalPageNumber = response.body().getMeta().getLastPage();

                    if (pageNumber == 1) {

                        if (!response.body().getSampleRequests().isEmpty()) {
                            initRecycler(response.body());
                            mainViews.showContent();
                            recyclerView.refreshComplete();
                        } else {
                            mainViews.showNoData();
                        }

                    } else {

                        updateRecycler(response.body());

                    }
                    return;
                }
                mainViews.showError();
                Toast.makeText(SampleCollectionListActivity.this, "error getting results", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<SampleRequestsResponse> call, Throwable t) {
                mainViews.showError();
                Toast.makeText(SampleCollectionListActivity.this, "error getting results", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getSearchedSampleRequests(int page) {

        Call<SampleRequestsResponse> call = apiService.getSearchedSampleRequests(token, searchQuery, page);
        call.enqueue(new Callback<SampleRequestsResponse>() {
            @Override
            public void onResponse(Call<SampleRequestsResponse> call, Response<SampleRequestsResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    totalPageNumber = response.body().getMeta().getLastPage();

                    if (pageNumber == 1) {

                        if (!response.body().getSampleRequests().isEmpty()) {
                            initRecycler(response.body());
                            mainViews.showContent();
                            recyclerView.refreshComplete();
                        } else {
                            mainViews.showNoData();
                        }

                    } else {

                        updateRecycler(response.body());

                    }
                    return;
                }
                mainViews.showError();
                Toast.makeText(SampleCollectionListActivity.this, "error getting results", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<SampleRequestsResponse> call, Throwable t) {
                mainViews.showError();
                Toast.makeText(SampleCollectionListActivity.this, "error getting results", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
