package com.collabmedhealthcare.vitals.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.SampleRequest;
import com.collabmedhealthcare.vitals.data.models.ProfilePhotoUtils;

import java.util.ArrayList;
import java.util.List;

public class SampleRequestRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private ArrayList<SampleRequest> allSampleRequest;
    private ArrayList<SampleRequest> filteredSampleRequest;
    private OnSampleRequestListener onScreenedPatientListener;

    public SampleRequestRecyclerAdapter(ArrayList<SampleRequest> sampleRequests, OnSampleRequestListener onScreenedPatientListener) {
        this.allSampleRequest = sampleRequests;
        filteredSampleRequest = sampleRequests;
        this.onScreenedPatientListener = onScreenedPatientListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View dataView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.layout_screened_patient, parent, false);
        return new SampleRequestViewHolder(dataView, onScreenedPatientListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        SampleRequest sampleRequest = filteredSampleRequest.get(position);
        SampleRequestViewHolder viewHolder = (SampleRequestViewHolder) holder;

        viewHolder.patientName.setText(sampleRequest.getPatient());
        viewHolder.patientRequestStatus.setText(sampleRequest.getStatus());
        viewHolder.patientRequestDate.setText(sampleRequest.getDate());

        Glide.with(viewHolder.patientImage.getContext())
                .load(ProfilePhotoUtils.getProfileResourceByName(sampleRequest.getPatient()))
                .centerCrop()
                .into(viewHolder.patientImage);

        if (sampleRequest.getStatus().equals("Collected")) {
            Glide.with(viewHolder.patientStatusImage.getContext())
                    .load(R.drawable.ic_check_24dp)
                    .centerCrop()
                    .into(viewHolder.patientStatusImage);
        } else {
            Glide.with(viewHolder.patientStatusImage.getContext())
                    .load(R.drawable.ic_hourglass_empty_24dp)
                    .centerCrop()
                    .into(viewHolder.patientStatusImage);
        }

    }

    @Override
    public int getItemCount() {
        return filteredSampleRequest.size();
    }

    @Override
    public Filter getFilter() {
        return sampleRequestFilter;
    }

    private Filter sampleRequestFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String queryString = constraint.toString().toLowerCase();

            if (queryString.isEmpty()) {
                filteredSampleRequest = allSampleRequest;
            } else {

                List<SampleRequest> filteredList = new ArrayList<>();

                for (SampleRequest topic : allSampleRequest) {
                    if (topic.getPatient().toLowerCase().contains(queryString.trim())) {
                        filteredList.add(topic);
                    }
                }

                filteredSampleRequest = new ArrayList<>(filteredList);
            }

            FilterResults results = new FilterResults();
            results.values = filteredSampleRequest;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredSampleRequest = ((ArrayList<SampleRequest>) results.values);
            notifyDataSetChanged();
        }
    };

    public class SampleRequestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView patientImage;
        ImageView patientStatusImage;
        TextView patientName;
        TextView patientRequestDate;
        TextView patientRequestStatus;

        OnSampleRequestListener screenedPatientListener;

        public SampleRequestViewHolder(@NonNull View itemView, OnSampleRequestListener onScreenedPatientListener) {
            super(itemView);

            patientImage = itemView.findViewById(R.id.iv_screenedPatient_image);
            patientStatusImage = itemView.findViewById(R.id.iv_screenedPatient_status);
            patientName = itemView.findViewById(R.id.tv_ScreenedPatient_name);
            patientRequestDate = itemView.findViewById(R.id.tv_screenedPatient_date);
            patientRequestStatus = itemView.findViewById(R.id.tv_screenedPatient_status);

            screenedPatientListener = onScreenedPatientListener;

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            screenedPatientListener.onSampleRequestClicked(getAdapterPosition());
        }
    }

    public interface OnSampleRequestListener {
        void onSampleRequestClicked(int position);
    }
}
