package com.collabmedhealthcare.vitals.ui.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.LoginRequest;
import com.collabmedhealthcare.vitals.data.models.PatientRegion;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.UserRegion;
import com.collabmedhealthcare.vitals.data.models.response.LoginResponse;
import com.collabmedhealthcare.vitals.ui.views.LoadingDialog;
import com.collabmedhealthcare.vitals.ui.views.RegionBottomSheet;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements RegionBottomSheet.AddRegionSelectionListener {

    TextInputEditText usernameEditText;
    TextInputEditText passwordEditText;

    TextView tvLoginDetails;

    LoadingDialog loadingDialog;
    private SharedPrefsUtil prefsUtil;

    private LoginResponse loginResponse;
    private ApiService apiService;
    private CardView loginView;
    private TextView tvLoginStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loadingDialog = new LoadingDialog(this);

        initViews();
    }

    private void initViews() {
        prefsUtil = SharedPrefsUtil.getInstance(this);
        apiService = ApiClient.createService(ApiService.class);

        tvLoginDetails = findViewById(R.id.tv_login_details);
        tvLoginStatus = findViewById(R.id.tv_login_status);

        usernameEditText = findViewById(R.id.edt_login_username);
        passwordEditText = findViewById(R.id.edt_login_password);

        loginView = findViewById(R.id.cardView_loginActivity_login);
        loginView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidInput()) {
                    loadingDialog.showDialog();

                    LoginRequest loginRequest =
                            new LoginRequest(usernameEditText.getText().toString(),
                                    passwordEditText.getText().toString());

                    signInResponder(loginRequest);

                } else {
                    Toast.makeText(LoginActivity.this, "enter credentials", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void signInResponder(LoginRequest request) {

        Call<LoginResponse> loginResponseCall = apiService.loginUser(request);

        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    loginResponse = response.body();
                    showSuccessLoginIn();

                    ArrayList<PatientRegion> regions = new ArrayList<>(response.body().getRegionDetails().getRegions());
                    ArrayList<UserRegion> userRegions = new ArrayList<>();

                    if (!regions.isEmpty()) {
                        for (PatientRegion currentRegion : regions) {
                            UserRegion region = new UserRegion();

                            region.setRegionId(currentRegion.getId());
                            region.setRegionName(currentRegion.getName());

                            userRegions.add(region);
                        }

                        RegionBottomSheet regionBottomSheet = new RegionBottomSheet(userRegions);
                        regionBottomSheet.show(getSupportFragmentManager(), "Add Region");

                        return;
                    }

                    storeUserPreferences();
                    openMainActivity();

                    return;
                }
                showErrorLoginIn();
                loadingDialog.hideDialog();
                Toast.makeText(LoginActivity.this, "Invalid credentials", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                showErrorLoginIn();
                loadingDialog.hideDialog();
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    public boolean isValidInput() {

        boolean isValidDetails = true;

        if (usernameEditText.getText().toString().trim().isEmpty()) {
            usernameEditText.setError("Invalid Username");
            isValidDetails = false;
        }

        if (passwordEditText.getText().toString().trim().isEmpty()) {
            passwordEditText.setError("Invalid password");
            isValidDetails = false;
        }

        return isValidDetails;
    }

    private void openMainActivity() {
        Intent myIntent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(myIntent);
        finish();
    }

    private void storeUserPreferences() {
        prefsUtil.setUserHasLoggedIn();
        prefsUtil.setUserHasSetUp();
        prefsUtil.storeAccessToken(loginResponse.getAccessToken());
        prefsUtil.storeUsername(loginResponse.getUserNames());
    }

    private void showErrorLoginIn() {
        tvLoginStatus.setText("Retry");
        loginView.setCardBackgroundColor(getResources().getColor(R.color.quantum_googred));
    }

    private void showSuccessLoginIn() {
        tvLoginStatus.setText("success");
        loginView.setCardBackgroundColor(getResources().getColor(R.color.quantum_googgreen));
    }

    @Override
    public void onRegionSelected(boolean isSelected) {

        if (isSelected) {
            storeUserPreferences();
            openMainActivity();
        } else {
            loadingDialog.hideDialog();
            showErrorLoginIn();
            Toast.makeText(this, "please select a region to continue", Toast.LENGTH_SHORT).show();
        }

    }
}
