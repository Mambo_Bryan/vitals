package com.collabmedhealthcare.vitals.ui.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.PatientLocationDetails;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.collabmedhealthcare.vitals.ui.viewmodels.SpecificContactTraceViewModel;
import com.collabmedhealthcare.vitals.ui.views.activities.AddPatientActivity;
import com.collabmedhealthcare.vitals.ui.views.RequestSampleBottomSheet;
import com.google.gson.Gson;

/**
 * A simple {@link Fragment} subclass.
 */
public class SpecificContactTracingFragment extends Fragment implements View.OnClickListener, RequestSampleBottomSheet.OnSampleRequestListener {

    private View rootView;
    private ScreenedPatient currentPatient;

    private TextView contactName;
    private TextView contactMobile;
    private TextView contactEmail;
    private TextView contactLocation;
    private TextView contactNextOfKinName;
    private TextView contactNextOfKinMobile;
    private TextView contactTracker;
    private TextView contactTrackingStatus;

    public SpecificContactTracingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_specific_contact_tracing, container, false);

        initViews();

        return rootView;
    }

    private void initViews() {

        contactName = rootView.findViewById(R.id.tv_specificTrace_name);
        contactMobile = rootView.findViewById(R.id.tv_specificTrace_mobile);
        contactEmail = rootView.findViewById(R.id.tv_specificTrace_email);
        contactLocation = rootView.findViewById(R.id.tv_specificTrace_location);

        contactNextOfKinName = rootView.findViewById(R.id.tv_specificTrace_nextOfKin_name);
        contactNextOfKinMobile = rootView.findViewById(R.id.tv_specificTrace_nextOfKin_mobile);

        contactTracker = rootView.findViewById(R.id.tv_specificTrace_tracking_trackee);
        contactTrackingStatus = rootView.findViewById(R.id.tv_specificTrace_tracking_status);

        rootView.findViewById(R.id.tv_specificTrace_edit_contact).setOnClickListener(this);
        rootView.findViewById(R.id.tv_specificTrace_change_status).setOnClickListener(this);
        rootView.findViewById(R.id.tv_specificTrace_request_sample).setOnClickListener(this);
        rootView.findViewById(R.id.tv_specificTrace_add_note).setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SpecificContactTraceViewModel viewModel = new ViewModelProvider(requireActivity()).get(SpecificContactTraceViewModel.class);
        viewModel.getScreenedPatient().observe(getViewLifecycleOwner(), new Observer<ScreenedPatient>() {
            @Override
            public void onChanged(ScreenedPatient patient) {
                if (patient != null) {

                    currentPatient = patient;

                    updateContactViews();
                }
            }
        });
    }

    private void updateContactViews() {

        setContactDetailsText(contactName, currentPatient.getFullName());
        setContactDetailsText(contactMobile, currentPatient.getMobile());
        setContactDetailsText(contactEmail, currentPatient.getEmail());

        PatientLocationDetails homeLocation = currentPatient.getHomeLocation();
        if (homeLocation != null) {
            setContactDetailsText(contactLocation, homeLocation.getPlaceName());
        } else {
            setContactDetailsText(contactLocation, "No Location Set");
        }

        if (currentPatient.getNok() != null && !currentPatient.getNok().isEmpty()) {
            contactNextOfKinName.setText(currentPatient.getNok().get(0).getFirstName());
            contactNextOfKinMobile.setText(currentPatient.getNok().get(0).getMobile());
        } else {
            contactNextOfKinName.setText("No Next of Kin");
            contactNextOfKinMobile.setText("No Next of Kin");
        }

        if (currentPatient.getContacts() != null && !currentPatient.getContacts().isEmpty()) {
            contactTracker.setText(currentPatient.getContacts().get(0).getTrackee());
            contactTrackingStatus.setText(currentPatient.getContacts().get(0).getTrackingStatus());
        } else {
            contactTracker.setText("No Tracker");
            contactTrackingStatus.setText("Pending");
        }

    }

    public void setContactDetailsText(TextView textView, String text) {

        if (text == null) {
            textView.setText("Empty");
        } else {
            textView.setText(text);
        }

    }

    @Override
    public void onClick(View v) {
        Gson gson = new Gson();
        String clickedPatient = gson.toJson(currentPatient);


        switch (v.getId()) {

            case R.id.tv_specificTrace_edit_contact:

                Intent myIntent = new Intent(getContext(), AddPatientActivity.class);
                myIntent.putExtra(AppConstantsUtils.SCREENED_PATIENT_ACTIVITY_EXTRA, clickedPatient);
                startActivity(myIntent);

                break;

            case R.id.tv_specificTrace_change_status:
            case R.id.tv_specificTrace_add_note:
                Toast.makeText(getContext(), "feature currently being added", Toast.LENGTH_SHORT).show();
                break;

            case R.id.tv_specificTrace_request_sample:

                Intent screenIntent = new Intent(getContext(), AddPatientActivity.class);
                screenIntent.putExtra(AppConstantsUtils.SCREENED_PATIENT_ACTIVITY_CODE_EXTRA,
                        AppConstantsUtils.PATIENT_ADD_FROM_CONTACT);
                screenIntent.putExtra(AppConstantsUtils.SCREENED_PATIENT_ACTIVITY_EXTRA, clickedPatient);
                startActivity(screenIntent);
                break;

            default:
                break;
        }
    }

    private void requestContactSample() {

        RequestSampleBottomSheet bottomSheet = new RequestSampleBottomSheet(currentPatient, this);
        bottomSheet.show(getFragmentManager(), "Request Sample");
    }

    @Override
    public void onRequestSentSuccess(boolean isSuccessful) {

        if (isSuccessful) {

        } else {

        }
    }
}
