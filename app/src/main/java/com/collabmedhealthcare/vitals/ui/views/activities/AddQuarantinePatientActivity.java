package com.collabmedhealthcare.vitals.ui.views.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.ui.adapter.ScreenedPatientAdapter;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.PatientLocationDetails;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.request.QuarantinePatientRequest;
import com.collabmedhealthcare.vitals.data.models.response.DefaultMessageResponse;
import com.collabmedhealthcare.vitals.data.models.response.ScreenedPatientsResponse;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddQuarantinePatientActivity extends AppCompatActivity implements View.OnClickListener {


    private AutoCompleteTextView patient;
    private AutoCompleteTextView startingDate;
    private TextInputEditText numberOfDays;
    private AutoCompleteTextView location;

    private PatientLocationDetails quarantineLocationDetails;
    private List<ScreenedPatient> patients;
    private ScreenedPatient selectedPatient;
    private ScreenedPatientAdapter listAdapter;
    private SharedPrefsUtil prefsUtil;
    private ApiService apiService;
    private String authToken;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_quarantine_patient);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Patient");

        initViews();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void initViews() {

        patients = new ArrayList<>();

        listAdapter = new ScreenedPatientAdapter(this,
                R.layout.layout_item_screened_patient_row, patients);

        prefsUtil = SharedPrefsUtil.getInstance(this);
        authToken = prefsUtil.getUserAccessToken();
        apiService = ApiClient.createService(ApiService.class);

        findViewById(R.id.btn_addQuarantine_date).setOnClickListener(this);
        findViewById(R.id.btn_addQuarantine_location).setOnClickListener(this);
        findViewById(R.id.btn_addQuarantine_patient).setOnClickListener(this);

        patient = findViewById(R.id.edt_addQuarantine_patientName);
        startingDate = findViewById(R.id.edt_addQuarantine_date);
        startingDate.setEnabled(false);
        numberOfDays = findViewById(R.id.edt_addQuarantine_nummberOfDays);
        progressBar = findViewById(R.id.progressBar_quarantine);
        progressBar.setVisibility(View.GONE);

        location = findViewById(R.id.edt_addQuarantine_location);
        location.setEnabled(false);


        patient.setThreshold(1);
        patient.setAdapter(listAdapter);
        patient.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedPatient = patients.get(position);
                patient.setText(selectedPatient.getFullName());

            }
        });
        patient.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    if (!patient.getText().toString().trim().isEmpty()) {
                        String query = patient.getText().toString().toLowerCase();
                        searchForPatient(query);
                    } else {
                        patient.setError("Invalid Patient Name");
                    }

                }
                return true;
            }
        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_addQuarantine_date:
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                startingDate.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                break;

            case R.id.btn_addQuarantine_location:

                /**
                 * Initialize Places. For simplicity, the API key is hard-coded. In a production
                 * environment we recommend using a secure mechanism to manage API keys.
                 */
                if (!Places.isInitialized()) {
                    Places.initialize(getApplicationContext(), AppConstantsUtils.LOCATION_API_KEY);
                }


                // Set the fields to specify which types of place data to return.
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, fields)
                        .build(this);
                startActivityForResult(intent, AppConstantsUtils.REQUEST_CODE_MAPS_LOCATION_HOME);
                break;

            case R.id.btn_addQuarantine_patient:

                if (isValidDetails()) {
                    addPatientToQuarantine();
                }

                break;

            default:
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == AppConstantsUtils.REQUEST_CODE_MAPS_LOCATION_HOME) {
                getLocationData(data);
            }
        }

    }

    private void getLocationData(Intent data) {
        if (data != null) {

            Place place = Autocomplete.getPlaceFromIntent(data);
            LatLng activityLocation = place.getLatLng();

            if (activityLocation != null) {

                quarantineLocationDetails = new PatientLocationDetails();

                quarantineLocationDetails.setLatitude(String.valueOf(activityLocation.latitude));
                quarantineLocationDetails.setLatitude(String.valueOf(activityLocation.longitude));
                quarantineLocationDetails.setPlaceName(place.getName());

                location.setText(place.getName());
            }
        }
    }

    private void initAutocompleteAdapter() {
        listAdapter = new ScreenedPatientAdapter(AddQuarantinePatientActivity.this,
                R.layout.layout_item_screened_patient_row, patients);
        patient.setThreshold(1);
        patient.setAdapter(listAdapter);
        patient.showDropDown();
    }

    private boolean isValidDetails() {
        boolean isValid = true;

        if (patient.getText().toString().trim().isEmpty()) {
            patient.setError("Name cannot be empty");
            isValid = false;
        }

        if (selectedPatient == null) {
            patient.setError("Invalid location");
            isValid = false;
        }

        if (startingDate.getText().toString().trim().isEmpty()) {
            startingDate.setError("Invalid Date");
            isValid = false;
        }

        if (numberOfDays.getText().toString().trim().isEmpty()) {
            numberOfDays.setError("Invalid Days");
            isValid = false;
        }

        if (location.getText().toString().trim().isEmpty()) {
            location.setError("Invalid Location");
            isValid = false;
        }

        if (quarantineLocationDetails == null) {
            location.setError("Invalid location");
            isValid = false;
        }

        return isValid;
    }

    private void searchForPatient(String query) {
        Call<ScreenedPatientsResponse> call = apiService.getSearchedPatient(authToken,
                query, true);

        call.enqueue(new Callback<ScreenedPatientsResponse>() {
            @Override
            public void onResponse(Call<ScreenedPatientsResponse> call, Response<ScreenedPatientsResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    patients = response.body().getScreenedPatients();

                    initAutocompleteAdapter();

                    return;
                }
                Toast.makeText(AddQuarantinePatientActivity.this,
                        "error getting results", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ScreenedPatientsResponse> call, Throwable t) {
                Toast.makeText(AddQuarantinePatientActivity.this,
                        "error getting patient", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addPatientToQuarantine() {

        progressBar.setVisibility(View.VISIBLE);

        QuarantinePatientRequest body = new QuarantinePatientRequest();

        body.setPatientId(selectedPatient.getId());
        body.setStartingOn(startingDate.getText().toString());
        body.setDays(numberOfDays.getText().toString());
        body.setPlace(quarantineLocationDetails);

        Call<DefaultMessageResponse> call = apiService.addQuarantinePatient(authToken, body);
        call.enqueue(new Callback<DefaultMessageResponse>() {
            @Override
            public void onResponse(Call<DefaultMessageResponse> call, Response<DefaultMessageResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    Toast.makeText(AddQuarantinePatientActivity.this, "requested", Toast.LENGTH_SHORT).show();
                    finish();
                    progressBar.setVisibility(View.INVISIBLE);

                    return;
                }

                progressBar.setVisibility(View.GONE);
                Toast.makeText(AddQuarantinePatientActivity.this,
                        "unable add patient", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<DefaultMessageResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(AddQuarantinePatientActivity.this,
                        "unable add patient", Toast.LENGTH_SHORT).show();
            }
        });

    }

}