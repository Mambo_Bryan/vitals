package com.collabmedhealthcare.vitals.ui.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.ContactData;
import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.TraceProfile;
import com.collabmedhealthcare.vitals.data.models.objects.SystemUser;
import com.collabmedhealthcare.vitals.data.models.request.ContactTraceRequest;
import com.collabmedhealthcare.vitals.data.models.request.CreateTraceContactRequest;
import com.collabmedhealthcare.vitals.data.models.request.TrackerRequestBody;
import com.collabmedhealthcare.vitals.data.models.response.DefaultMessageResponse;
import com.collabmedhealthcare.vitals.data.models.response.ScreenedPatientsResponse;
import com.collabmedhealthcare.vitals.data.models.response.UsersResponse;
import com.collabmedhealthcare.vitals.ui.adapter.ScreenedPatientAdapter;
import com.collabmedhealthcare.vitals.ui.views.SystemUsersBottomSheet;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddContactTraceActivity extends AppCompatActivity implements SystemUsersBottomSheet.AddTrackersListener {

    private ContactData contactData;

    private TextInputEditText patientFullname;
    private CreateTraceContactRequest patientRequest;
    private TextInputEditText patientAge;
    private TextInputEditText patientMobile;
    private TextInputEditText patientIDNumber;
    private TextInputEditText patientEmail;
    private TextInputEditText patientAlternateEmail;
    private TextInputEditText patientAlternateMobile;
    private ProgressBar patientProgressBar;
    private Button addPatientContact;
    private TextInputEditText patientTown;

    private ApiService apiService;
    private SharedPrefsUtil prefsUtil;

    private Button addContact;
    private ProgressBar contactProgress;
    private TextInputEditText contactName;
    private TextInputEditText contactMobile;
    private TextInputEditText contactAlternateMobile;
    private TextInputEditText contactMail;
    private TextInputEditText contactAlternateMail;
    private AutoCompleteTextView contactPatient;
    private AutoCompleteTextView contactLocation;
    private AutoCompleteTextView contactTrackers;
    private TextInputEditText contactComments;
    private ContactTraceRequest contactRequest;
    private String contactLatitude;
    private String contactLongitude;
    private UsersResponse usersResponse;
    private ArrayList<SystemUser> trackersList;

    private String authToken;
    private List<ScreenedPatient> patients;
    private ScreenedPatient patient;
    private ScreenedPatientAdapter listAdapter;

    private boolean isLevelOneUpdating = false;
    private boolean isLevelTwoUpdating = false;
    private ArrayList<TrackerRequestBody> trackers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        apiService = ApiClient.createService(ApiService.class);
        prefsUtil = SharedPrefsUtil.getInstance(this);
        authToken = prefsUtil.getUserAccessToken();

        Intent myIntent = getIntent();

        int code = myIntent.getIntExtra(AppConstantsUtils.CONTACT_TRACE_STRING_EXTRA, AppConstantsUtils.ERROR);

        if (code == AppConstantsUtils.LEVEL_1_CONTACT_TRACE_INT) {

            setContentView(R.layout.activity_leve_one_contact);
            initLevelOneViews();
            showContentContact();
            getSupportActionBar().setTitle("Add Tracing Contact");

        } else if (code == AppConstantsUtils.LEVEL_1_CONTACT_TRACE_UPDATE_INT) {
            setContentView(R.layout.activity_leve_one_contact);
            isLevelOneUpdating = true;

            Gson gson = new Gson();
            String contactDataString = myIntent.getStringExtra(AppConstantsUtils.GENERAL_TRACE_CONTACT_EXTRA);
            contactData = gson.fromJson(contactDataString, ContactData.class);
            getSupportActionBar().setTitle("Update Contact");
            initLevelOneViews();
            showContentContact();
            showUpdateContact();

        } else if (code == AppConstantsUtils.LEVEL_2_CONTACT_TRACE_INT) {

            setContentView(R.layout.activity_level_two_contact);
            contactData = myIntent.getParcelableExtra(AppConstantsUtils.GENERAL_TRACE_CONTACT_EXTRA);
            initLevelTwoViews();
            showContentPatient();
            getSupportActionBar().setTitle("Add Tracing Contact");

        } else if (code == AppConstantsUtils.LEVEL_2_CONTACT_TRACE_UPDATE_INT) {

            isLevelTwoUpdating = true;
            setContentView(R.layout.activity_level_two_contact);
            getSupportActionBar().setTitle("Update Contact");
            initLevelTwoViews();
            showContentPatient();
        }

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void initLevelTwoViews() {

        patientFullname = findViewById(R.id.edt_addPatientContact_fullname);
        patientAge = findViewById(R.id.edt_addPatientContact_age);
        patientMobile = findViewById(R.id.edt_addPatientContact_mobile);
        patientIDNumber = findViewById(R.id.edt_addPatientContact_idNumber);
        patientEmail = findViewById(R.id.edt_addPatientContact_email);
        patientAlternateEmail = findViewById(R.id.edt_addPatientContact_alternateEmail);
        patientAlternateMobile = findViewById(R.id.edt_addPatientContact_alternateMobile);
        patientTown = findViewById(R.id.edt_addPatientContact_town);

        patientProgressBar = findViewById(R.id.progressBar_addPatientContact);
        addPatientContact = findViewById(R.id.btn_addPatientContact);
        addPatientContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidPatientCredentials()) {
                    showLoadingPatient();

                    createPatientContact();
                    sendPatientDetailsToServer();

                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();

        return super.onSupportNavigateUp();
    }


    private void sendPatientDetailsToServer() {

        Call<DefaultMessageResponse> call = apiService.createNewContactTracePerson(
                authToken,
                patientRequest
        );

        call.enqueue(new Callback<DefaultMessageResponse>() {
            @Override
            public void onResponse(Call<DefaultMessageResponse> call,
                                   Response<DefaultMessageResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();

                    return;
                }

                showErrorPatient();
                Toast.makeText(AddContactTraceActivity.this, "unable create contact", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<DefaultMessageResponse> call, Throwable t) {
                showErrorPatient();
                Toast.makeText(AddContactTraceActivity.this, "unable to connect", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void createPatientContact() {

        TraceProfile profile = new TraceProfile();

        String[] fullname = patientFullname.getText().toString().split(" ");

        profile.setFirstName(fullname[0]);
        profile.setMiddleName(fullname[1]);

        if (fullname.length > 2) {
            profile.setLastName(fullname[2]);
        } else {
            profile.setLastName(" ");
        }

        profile.setAge(patientAge.getText().toString());
        profile.setMobile(patientMobile.getText().toString());
        profile.setIdNo(patientIDNumber.getText().toString());
        profile.setEmail(patientEmail.getText().toString());

        if (patientAlternateEmail.getText().toString().isEmpty()) {
            profile.setSecondaryEmail(" ");
        } else {
            profile.setSecondaryEmail(patientAlternateEmail.getText().toString());
        }

        profile.setTown(patientTown.getText().toString());

        if (patientAlternateMobile.getText().toString().isEmpty()) {
            profile.setTelephone(null);
        } else {
            profile.setTelephone(patientAlternateMobile.getText().toString());

        }

        patientRequest = new CreateTraceContactRequest();

        patientRequest.setContactId(String.valueOf(contactData.getId()));
        patientRequest.setProfile(profile);

    }

    private void showLoadingPatient() {

        addPatientContact.setVisibility(View.INVISIBLE);
        patientProgressBar.setVisibility(View.VISIBLE);

    }

    private void showContentPatient() {

        patientProgressBar.setVisibility(View.INVISIBLE);
        addPatientContact.setVisibility(View.VISIBLE);

    }

    private void showErrorPatient() {

        patientProgressBar.setVisibility(View.INVISIBLE);
        addPatientContact.setBackgroundColor(getResources().getColor(R.color.quantum_googred));
        addPatientContact.setText("retry");
        addPatientContact.setVisibility(View.VISIBLE);

    }

    private boolean isValidPatientCredentials() {
        boolean isValid = true;

        String[] pateintName = patientFullname.getText().toString().split(" ");

        if (patientFullname.getText().toString().trim().isEmpty()) {
            patientFullname.setError("Name cannot be empty");
            isValid = false;
        }

        if (pateintName.length < 1) {
            patientFullname.setError("Enter full names");
            isValid = false;
        }

        if (patientAge.getText().toString().trim().isEmpty()) {
            patientAge.setError("Invalid Age");
            isValid = false;
        }

        if (patientMobile.getText().toString().trim().isEmpty()) {
            patientMobile.setError("Invalid Mobile");
            isValid = false;
        }

        if (patientIDNumber.getText().toString().trim().isEmpty()) {
            patientIDNumber.setError("Invalid ID Number");
            isValid = false;
        }

        if (patientTown.getText().toString().trim().isEmpty()) {
            patientTown.setError("Invalid Town");
            isValid = false;
        }


        return isValid;
    }

    private void initLevelOneViews() {

        patients = new ArrayList<>();
        usersResponse = null;
        trackersList = new ArrayList<>();

        getTrackers();

        listAdapter = new ScreenedPatientAdapter(this,
                R.layout.layout_item_screened_patient_row, patients);

        contactName = findViewById(R.id.edt_addContact_name);
        contactMobile = findViewById(R.id.edt_addContact_mobile);
        contactAlternateMobile = findViewById(R.id.edt_addContact_alternateMobile);
        contactMail = findViewById(R.id.edt_addContact_email);
        contactAlternateMail = findViewById(R.id.edt_addContact_alternateEmail);

        contactPatient = findViewById(R.id.edt_addContact_patientName);

        contactPatient.setThreshold(1);
        contactPatient.setAdapter(listAdapter);
        contactPatient.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                patient = patients.get(position);
                contactPatient.setText(patient.getFullName());

            }
        });
        contactPatient.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    if (!contactPatient.getText().toString().trim().isEmpty()) {
                        String query = contactPatient.getText().toString().toLowerCase();
                        searchForPatient(query, 1);
                    } else {
                        contactPatient.setError("Invalid Patient Name");
                    }

                }
                return true;
            }
        });

        contactLocation = findViewById(R.id.edt_addContact_location);
        contactTrackers = findViewById(R.id.edt_addContact_trackers);
        contactComments = findViewById(R.id.edt_addContact_comments);

        contactProgress = findViewById(R.id.progressBar_addContact);
        addContact = findViewById(R.id.btn_addContact);
        addContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidContactCredentials()) {

                    if (isLevelOneUpdating) {
                        Toast.makeText(AddContactTraceActivity.this, "" +
                                "Upcoming feature in next release", Toast.LENGTH_SHORT).show();
                    } else {
                        createContactRequest();
                        sendContactDetailsToServer();
                    }

                }
            }
        });

        Button addContactLocation = findViewById(R.id.btn_addContact_location);
        addContactLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Initialize Places. For simplicity, the API key is hard-coded. In a production
                 * environment we recommend using a secure mechanism to manage API keys.
                 */
                if (!Places.isInitialized()) {
                    Places.initialize(getApplicationContext(), AppConstantsUtils.LOCATION_API_KEY);
                }

                // Set the fields to specify which types of place data to return.
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, fields)
                        .build(AddContactTraceActivity.this);

                startActivityForResult(intent, AppConstantsUtils.REQUEST_CODE_MAPS_LOCATION_WORk);
            }
        });

        Button addContactTrackers = findViewById(R.id.btn_addContact_trackers);
        addContactTrackers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SystemUsersBottomSheet bottomSheet = new SystemUsersBottomSheet(usersResponse, AddContactTraceActivity.this);
                bottomSheet.show(getSupportFragmentManager(), "System Users");
            }
        });

    }

    private void showUpdateContact() {

        contactName.setText(contactData.getContact());
        contactMobile.setText(contactData.getMobile());
        contactAlternateMobile.setText(contactData.getTelephone());
        contactMail.setText(contactData.getEmail());
        contactAlternateMail.setText(contactData.getSecondaryEmail());

        contactPatient.setText(contactData.getPatient());

        searchForPatient(contactData.getPatient(), 1);

        contactLocation.setText(contactData.getLocation());
//        contactTrackers.setText(contactData.getTrackers());

        contactComments.setText(contactData.getComments());

        addContact.setText("update");
        addContact.setBackgroundColor(getResources().getColor(R.color.quantum_googgreen));

    }

    private void createContactRequest() {

        contactRequest = new ContactTraceRequest();

        contactRequest.setContact(contactName.getText().toString());
        contactRequest.setMobile(contactMobile.getText().toString());
        contactRequest.setTelephone(contactAlternateMobile.getText().toString());
        contactRequest.setEmail(contactMail.getText().toString());
        contactRequest.setSecondaryEmail(contactAlternateMail.getText().toString());

        contactRequest.setPatientId(patient.getId());

        contactRequest.setLocationName(contactLocation.getText().toString());
        contactRequest.setLat(String.valueOf(contactLatitude));
        contactRequest.set_long(String.valueOf(contactLongitude));

        contactRequest.setTrackees(trackers);
        contactRequest.setComments(contactComments.getText().toString());

    }

    private void sendContactDetailsToServer() {

        showLoadingContact();

        Call<DefaultMessageResponse> call = apiService.createNewContactTrace(authToken,
                contactRequest);

        call.enqueue(new Callback<DefaultMessageResponse>() {
            @Override
            public void onResponse(Call<DefaultMessageResponse> call, Response<DefaultMessageResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();

                    return;
                }

                showErrorContact();
                Toast.makeText(AddContactTraceActivity.this, "unable create contact", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<DefaultMessageResponse> call, Throwable t) {
                showErrorContact();
                Toast.makeText(AddContactTraceActivity.this, "unable to connect", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void searchForPatient(String query, int page) {
        Call<ScreenedPatientsResponse> call = apiService.getSearchedScreenedPatient(authToken,
                query, true, page);

        call.enqueue(new Callback<ScreenedPatientsResponse>() {
            @Override
            public void onResponse(Call<ScreenedPatientsResponse> call, Response<ScreenedPatientsResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    patients = response.body().getScreenedPatients();


                    initAutocompleteAdapter();


                    return;
                }
                Toast.makeText(AddContactTraceActivity.this, "error getting results", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ScreenedPatientsResponse> call, Throwable t) {
                Toast.makeText(AddContactTraceActivity.this, "error getting patient", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getTrackers() {

        Call<UsersResponse> call = apiService.getUsers(authToken, true, 1);
        call.enqueue(new Callback<UsersResponse>() {
            @Override
            public void onResponse(Call<UsersResponse> call, Response<UsersResponse> response) {

                if (response.isSuccessful() && response.body() != null) {
                    usersResponse = response.body();
                    return;
                }

                usersResponse = null;

            }

            @Override
            public void onFailure(Call<UsersResponse> call, Throwable t) {
                usersResponse = null;
            }
        });

    }

    private void initAutocompleteAdapter() {
        listAdapter = new ScreenedPatientAdapter(AddContactTraceActivity.this,
                R.layout.layout_item_screened_patient_row, patients);
        contactPatient.setThreshold(1);
        contactPatient.setAdapter(listAdapter);
        contactPatient.showDropDown();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == AppConstantsUtils.REQUEST_CODE_MAPS_LOCATION_WORk) {
                getLocationData(data, contactLocation);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getLocationData(Intent data, EditText editText) {
        if (data != null) {

            Place place = Autocomplete.getPlaceFromIntent(data);
            LatLng contactLocationLatLng = place.getLatLng();

            if (contactLocationLatLng != null) {
                contactLatitude = String.valueOf(contactLocationLatLng.latitude);
                contactLongitude = String.valueOf(contactLocationLatLng.longitude);
                editText.setText(place.getName());
            }
        }
    }

    private boolean isValidContactCredentials() {
        boolean isValid = true;

        if (contactName.getText().toString().trim().isEmpty()) {
            contactName.setError("Invalid Name");
            isValid = false;
        }

        if (contactMobile.getText().toString().trim().isEmpty()) {
            contactMobile.setError("Invalid Mobile");
            isValid = false;
        }

        if (contactPatient.getText().toString().trim().isEmpty()) {
            contactPatient.setError("Invalid Patient");
            isValid = false;
        }

        if (patient == null) {
            contactPatient.setError("Please Select a Patient");
            isValid = false;
        }

        if (contactLocation.getText().toString().trim().isEmpty()) {
            contactLocation.setError("Invalid Location");
            isValid = false;
        }

        return isValid;
    }

    private void showLoadingContact() {

        addContact.setVisibility(View.INVISIBLE);
        contactProgress.setVisibility(View.VISIBLE);

    }

    private void showContentContact() {

        contactProgress.setVisibility(View.INVISIBLE);
        addContact.setVisibility(View.VISIBLE);

    }

    private void showErrorContact() {

        contactProgress.setVisibility(View.INVISIBLE);
        addContact.setBackgroundColor(getResources().getColor(R.color.quantum_googred));
        addContact.setText("retry");
        addContact.setVisibility(View.VISIBLE);

    }

    @Override
    public void onTrackersSelected(ArrayList<SystemUser> selectedTrackers) {

        trackers = new ArrayList<>();

        if (selectedTrackers != null) {

            StringBuilder users = new StringBuilder();

            for (SystemUser user : selectedTrackers) {

                trackers.add(new TrackerRequestBody(user.getId(), user.getFullName()));

                users.append(user.getFullName()).append(", ");
            }

            contactTrackers.setText(users.toString());
            trackersList = selectedTrackers;

        }

    }
}
