package com.collabmedhealthcare.vitals.ui.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.collabmedhealthcare.vitals.data.models.ScreenedPatient;

public class ScreenedPatientViewModel extends ViewModel {

    private MutableLiveData<ScreenedPatient> screenedPatient;

    public void init(ScreenedPatient patient) {

        screenedPatient = new MutableLiveData<>();

        screenedPatient.setValue(patient);

    }

    public LiveData<ScreenedPatient> getScreenedPatient() {
        return screenedPatient;
    }
}
