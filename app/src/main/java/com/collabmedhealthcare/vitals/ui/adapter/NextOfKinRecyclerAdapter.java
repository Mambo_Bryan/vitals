package com.collabmedhealthcare.vitals.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.data.interfaces.RecyclerItemListener;
import com.collabmedhealthcare.vitals.data.models.objects.NextOfKin;
import com.collabmedhealthcare.vitals.utils.RelationshipsUtils;

import java.util.ArrayList;

public class NextOfKinRecyclerAdapter extends RecyclerView.Adapter<NextOfKinRecyclerAdapter.NextOfKinViewHolder> {

    private ArrayList<NextOfKin> nextOfKins;
    private RecyclerItemListener mListener;

    public NextOfKinRecyclerAdapter(ArrayList<NextOfKin> nextOfKins, RecyclerItemListener mListener) {
        this.nextOfKins = nextOfKins;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public NextOfKinViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_patient_next_of_kin, parent, false);
        return new NextOfKinViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull NextOfKinViewHolder holder, int position) {

        NextOfKin kins = nextOfKins.get(position);
        RelationshipsUtils relationshipsUtils = RelationshipsUtils.getInstance();

        String kinName = kins.getFirstName() + " " + kins.getMiddleName() + " " + kins.getLastName();
        int relationship = kins.getRelationshipId();

        holder.nokName.setText(kinName);

        holder.nokLocation.setText(relationshipsUtils.getRelationshipName(relationship));

        holder.nokMobile.setText(kins.getMobile());

    }

    @Override
    public int getItemCount() {
        return nextOfKins.size();
    }


    public void removeItem(int position) {
        nextOfKins.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(NextOfKin item, int position) {
        nextOfKins.add(position, item);
        notifyItemInserted(position);
    }

    public ArrayList<NextOfKin> getData() {
        return nextOfKins;
    }

    public class NextOfKinViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView nokName;
        private TextView nokLocation;
        private TextView nokMobile;

        private RecyclerItemListener mListener;

        public NextOfKinViewHolder(@NonNull View itemView, RecyclerItemListener mListener) {
            super(itemView);

            nokName = itemView.findViewById(R.id.tv_nokItem_name);
            nokLocation = itemView.findViewById(R.id.tv_nokItem_location);
            nokMobile = itemView.findViewById(R.id.tv_nokItem_mobile);

            this.mListener = mListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onItemSelected(getAdapterPosition());
        }
    }

}
