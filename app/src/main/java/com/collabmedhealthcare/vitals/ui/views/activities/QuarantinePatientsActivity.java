package com.collabmedhealthcare.vitals.ui.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.collabmedhealthcare.vitals.R;
import com.collabmedhealthcare.vitals.ui.adapter.QuarantinePatientsRecyclerAdapter;
import com.collabmedhealthcare.vitals.data.api.ApiClient;
import com.collabmedhealthcare.vitals.data.api.ApiService;
import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;
import com.collabmedhealthcare.vitals.data.models.QuarantinedPatient;
import com.collabmedhealthcare.vitals.data.models.SharedPrefsUtil;
import com.collabmedhealthcare.vitals.data.models.response.QuarantinedPatientsResponse;
import com.collabmedhealthcare.vitals.utils.ListViewUtils;
import com.collabmedhealthcare.vitals.ui.views.LoadingDialog;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuarantinePatientsActivity extends AppCompatActivity implements View.OnClickListener, QuarantinePatientsRecyclerAdapter.QuarantinePatientListener, SearchView.OnQueryTextListener {

    //
    ArrayList<QuarantinedPatient> screenedPatients;
    QuarantinePatientsRecyclerAdapter adapter;

    //
    private ApiService apiService;
    private String token;

    private SearchView searchView;
    private ListViewUtils screenedPatientsView;
    private XRecyclerView recyclerView;

    private int pageNumber;
    private int totalPageNumber;

    private boolean isUserSearching = false;
    private String searchQuery;
    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template_recycler);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Quarantine Patients");

        initViews();
        initRecyclerCompleteViews();

        screenedPatientsView.showLoading();

        pageNumber = 1;

        getQuarantinedPatientsList(pageNumber);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {

            pageNumber = 1;
            isUserSearching = false;
            searchQuery = null;

            searchView.setIconified(true);
            screenedPatientsView.showLoading();
            getQuarantinedPatientsList(pageNumber);

        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem mSearchMenuItem = menu.findItem(R.id.menu_item_search);
        searchView = (SearchView) mSearchMenuItem.getActionView();
        searchView.setQueryHint("Search Patient");
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchQuery = query;

        if (!query.isEmpty()) {

            isUserSearching = true;
            pageNumber = 1;

            getSearchedQuarantinePatient(pageNumber);
            screenedPatientsView.showLoading();

        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (screenedPatients != null && !screenedPatients.isEmpty()) {
            adapter.getFilter().filter(newText);
        }
        return true;
    }

    private void initViews() {

        loadingDialog = new LoadingDialog(this);

        findViewById(R.id.fab_templateRecyler).setOnClickListener(this);

        SharedPrefsUtil prefsUtil = SharedPrefsUtil.getInstance(this);

        apiService = ApiClient.createService(ApiService.class);
        token = prefsUtil.getUserAccessToken();
    }

    private void initRecyclerCompleteViews() {
        View completeItemViews = findViewById(R.id.inlcude_templateRecycler_complete);

        View loading = completeItemViews.findViewById(R.id.include_template_loading);
        View noData = completeItemViews.findViewById(R.id.include_template_no_data);
        View error = completeItemViews.findViewById(R.id.include_template_error);
        View data = completeItemViews.findViewById(R.id.include_template_data);

        screenedPatientsView = new ListViewUtils(
                loading,
                noData,
                data,
                error,
                this
        );

        screenedPatientsView.setNoDataMessage("There aren't any patients quarantined, " +
                "please click below to add one.");
        recyclerView = data.findViewById(R.id.rv_template_data);

    }

    private void initRecycler(QuarantinedPatientsResponse response) {

        screenedPatients = new ArrayList<>(response.getQuarantinedPatientList());

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new QuarantinePatientsRecyclerAdapter(screenedPatients, this);

        recyclerView.setAdapter(adapter);

        recyclerView.setPullRefreshEnabled(true);
        recyclerView.setLoadingMoreEnabled(true);

        recyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                pageNumber = 1;
                getQuarantinedPatientsList(pageNumber);
            }

            @Override
            public void onLoadMore() {
                pageNumber++;

                if (isUserSearching) {
                    getSearchedQuarantinePatient(pageNumber);
                } else {
                    getQuarantinedPatientsList(pageNumber);
                }
            }
        });

    }

    private void updateRecycler(QuarantinedPatientsResponse response) {

        if (response != null && response.getQuarantinedPatientList() != null) {

            screenedPatients.addAll(response.getQuarantinedPatientList());
            adapter.notifyDataSetChanged();

        }

        if (pageNumber > totalPageNumber)
            recyclerView.setLoadingMoreEnabled(false);
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_template_retry) {
            getQuarantinedPatientsList(pageNumber);
            screenedPatientsView.showLoading();
        } else if (v.getId() == R.id.fab_templateRecyler) {

            Intent intent = new Intent(this, AddQuarantinePatientActivity.class);
            startActivityForResult(intent, AppConstantsUtils.REQUEST_CODE_ADD_QUARANTINE_PATIENT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == AppConstantsUtils.REQUEST_CODE_MAPS_LOCATION_HOME) {
                pageNumber = 1;
                getQuarantinedPatientsList(pageNumber);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onQuarantinePatientClicked(int position) {

    }

    public void getQuarantinedPatientsList(int page) {
        Call<QuarantinedPatientsResponse> call = apiService.getQuarantinedPatients(token, page);
        call.enqueue(new Callback<QuarantinedPatientsResponse>() {
            @Override
            public void onResponse(Call<QuarantinedPatientsResponse> call, Response<QuarantinedPatientsResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    totalPageNumber = response.body().getMeta().getLastPage();

                    if (pageNumber == 1) {

                        if (!response.body().getQuarantinedPatientList().isEmpty()) {

                            initRecycler(response.body());
                            screenedPatientsView.showContent();
                            recyclerView.refreshComplete();

                        } else {
                            screenedPatientsView.showNoData();
                        }

                    } else {
                        updateRecycler(response.body());

                    }

                    return;
                }

                screenedPatientsView.showError();
                Toast.makeText(QuarantinePatientsActivity.this, "error getting results", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<QuarantinedPatientsResponse> call, Throwable t) {
                screenedPatientsView.showError();
                Toast.makeText(QuarantinePatientsActivity.this, "error connecting", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getSearchedQuarantinePatient(int page) {
        Call<QuarantinedPatientsResponse> call = apiService.getSearchedQuarantinedPatients(token, searchQuery, page);
        call.enqueue(new Callback<QuarantinedPatientsResponse>() {
            @Override
            public void onResponse(Call<QuarantinedPatientsResponse> call, Response<QuarantinedPatientsResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    totalPageNumber = response.body().getMeta().getLastPage();

                    if (!response.body().getQuarantinedPatientList().isEmpty()) {
                        if (pageNumber == 1) {
                            initRecycler(response.body());
                            screenedPatientsView.showContent();
                            recyclerView.refreshComplete();

                        } else {
                            updateRecycler(response.body());
                        }
                    } else {

                        screenedPatientsView.showNoData();

                    }

                    return;
                }

                screenedPatientsView.showContent();
                Toast.makeText(QuarantinePatientsActivity.this, "error getting results", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<QuarantinedPatientsResponse> call, Throwable t) {
                screenedPatientsView.showContent();
                Toast.makeText(QuarantinePatientsActivity.this, "failed to connect", Toast.LENGTH_SHORT).show();
            }
        });
    }

}