package com.collabmedhealthcare.vitals.ui.adapter;

import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.collabmedhealthcare.vitals.R;

public class LoadingViewHolder extends RecyclerView.ViewHolder {

    public ProgressBar loadingProgressBar;

    public LoadingViewHolder(@NonNull View itemView) {

        super(itemView);

        loadingProgressBar = itemView.findViewById(R.id.progressBar_itemLoading);
    }


}
