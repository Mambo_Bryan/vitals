package com.collabmedhealthcare.vitals;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

import com.collabmedhealthcare.vitals.data.models.AppConstantsUtils;

public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        setUpNotificationChannels();
    }

    private void setUpNotificationChannels() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel1 = new NotificationChannel(
                    AppConstantsUtils.NOTIFICATION_CHANNEL_1_NAME,
                    "Channel 1",
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel1.setDescription("This is channel one (1)");
            NotificationChannel channel2 = new NotificationChannel(
                    AppConstantsUtils.NOTIFICATION_CHANNEL_2_NAME,
                    "Channel 2",
                    NotificationManager.IMPORTANCE_LOW
            );
            channel1.setDescription("This is channel two (2)");

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);
            manager.createNotificationChannel(channel2);

        } else {

        }

    }
}
